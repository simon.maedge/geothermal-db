# Geothemal-DB Graph Webapp: Documentation of the frontend

**Note:** The content is already further revised, but the features remain the same. This usage and setup can also be found [here](https://pad.gwdg.de/xLIgiAJLTdeQREtcho0P6EN)
## Structure 
Calling the start page of the web application displays a representation of the entire network of method-parameter links of the ontology.
            
The web interface offers a four-part structure. At the top, the generated graph is displayed. Below that, on the left side, is a listing of additional factual data related to the graph being displayed, which aids in understanding the graph as well as providing a complete representation of ontology data not represented in the graph. In the middle are options for displaying and changing the display configuration and on the right is a selection of method categories.
                
An exemplary representation (web interface with focus on the object horizon orientation):
![](https://pad.gwdg.de/uploads/upload_94b3eb5b6382f28cc37fe872534ed67d.png)


## Display of further information
### Further object information
The further factual data, in the case of selecting a focused object, is a table of statements about the object that include the ontology beyond what is shown in the graph (overview of the additional information when the _heat production_ node is focused):
<!--furtherInfo_table-->
![](https://pad.gwdg.de/uploads/upload_f7ee96e97e64a3795dad2d304f8e6e5f.png)

It is presented together with the constrained graph view required in the analysis in the form of a constrained context component, as shown above (web interface when focusing on the horizon orientation object). 

### K shortest paths
In the case where the K shortest paths between two nodes have been calculated, they are listed in a table of these paths with the respective rank, cost and expired nodes per path in one row. 
Since it is possible to display individual ones of these paths as a graph above, they are shown here with a dark background; all others can be displayed as a graph by selecting the appropriate row. By default, all possible paths are combined in one graph at the beginning. 

:::warning
**Note:** Some links have been added randomly to present functionality, and have nothing to do with the real data -> keyword _test ontology_.
:::

Tabular overview:
![](https://pad.gwdg.de/uploads/upload_1d0df2546cc2f00a3d2d74ae3e3e3d28.png)

The corresponding results when selecting the paths (the first 5 rows of the table):
![](https://pad.gwdg.de/uploads/upload_c8e528eef815a978a74cbcbe84436c35.png)
All paths combined (the last row of the table):
![](https://pad.gwdg.de/uploads/upload_49afadf36b479f468886b4091cdf5377.png)

                
### Alternative names
In each view of the web interface you can find the following button:
![](https://pad.gwdg.de/uploads/upload_9b1519b0af988ff827d0d7f59fdf69fc.png)

This allows, if there are nodes with labels entered in the VocPrez system under the currently displayed graph, to display the returned synonyms in tabular form after a query of the SPARQL endpoint:
![](https://pad.gwdg.de/uploads/upload_f11d8255798f0cf8a37e1a3ee6357f0b.png)

*[VocPrez]: Nicolas Car and others: VocPrez: A tool (API and web front-end) for the read-only delivery system of SKOS vocabularies. https://github.com/RDFLib/VocPrez
*[SPARQL Endpoints]: Geoscience Australia: Geoscience Vocabularies for Linked Data http://cgi.vocabs.ga.gov.au/
                
## Configuration of the display 
The configuration buttons are divided into _Features_ and design decisions (_Style_), as well as a button to return to the previous view (_go back_). 
![](https://pad.gwdg.de/uploads/upload_4c9a5efaedcbc576e5faf801ef069a12.png)


The filled button in a button group shows the actual state and is disabled, the open buttons, if an action is possible, are enabled and show it. 

- _clustering_ and _cluster's edges_ describe whether similar nodes are collected in a cluster (marked by orange rectangle around affected nodes) or additionally edges, which are incident to all members of the cluster, are combined (orange arrows). 

#### Clustering and cluster edges
An overview of the three cases: Graphs with clustering enabled and cluster edges (top), with clustering but without cluster edges (middle), and without clustering (bottom):
![](https://pad.gwdg.de/uploads/upload_ce874108214fc78fb954a49ceaebcf63.png)

![](https://pad.gwdg.de/uploads/upload_ac45f3f4d42036b84839128ebddc0715.png)

![](https://pad.gwdg.de/uploads/upload_df6b8a0aa1ca84a4343e93d59f744096.png)

#### Theme
_Theme_ lets you choose between a _Dark_ and a _Light_ display:
![](https://pad.gwdg.de/uploads/upload_dc1de624e46ff920f69804d88996298e.png)
![](https://pad.gwdg.de/uploads/upload_7cb46e010c6e0fc28ec5f26a860e7846.png)
(especially interesting for exporting the graphic for print or presentation)


#### Direction of Graph
                
Under _Direction of Graph_ it is possible to decide between running the edges from top to bottom (_Top-Down_) and from left to right (_Left-Right_):
![](https://pad.gwdg.de/uploads/upload_4f192f1c5f76113d1a4fff64bbcf5f74.png)

![](https://pad.gwdg.de/uploads/upload_b2d5f056d6fe8dff4300e2245f48648b.png)

#### Method and parameter shape

Using dropdowns it is possible to decide between 26 of the possible shapes of nodes for methods and parameters (_Shape of Method/Parameter nodes_), such as oval, rectangular, rhombic, square, ...:
![](https://pad.gwdg.de/uploads/upload_4e84b1f4038c7d0ec85e52f03f6129dd.png)
![](https://pad.gwdg.de/uploads/upload_ac0017594e26a93fd16a811ed0f4fb50.png)

#### edge width

as well as the size of the edges (_Edge Width_):
![](https://pad.gwdg.de/uploads/upload_c4ed8fc97361de27d864f1ff4ddea602.png)
![](https://pad.gwdg.de/uploads/upload_1a54a4c38a0c209b85c44c85f48eed3a.png)

#### Font Size

and of the captions (_Font Size_):
![](https://pad.gwdg.de/uploads/upload_8d4e7ad02ae3f885bd0ab47069979715.png)
![](https://pad.gwdg.de/uploads/upload_16e57ce672d86958c81df69ff7989537.png)

#### Links on nodes

The graph provides links on nodes to select them. After selecting a node for the first time, it turns dark green. 
![](https://pad.gwdg.de/uploads/upload_2bc4cfa74a5ad88b8cb4d6a3126700b4.png)
![](https://pad.gwdg.de/uploads/upload_51bdc306dc0831c41224c9af1f8e69fe.png)

If the same node is selected again, it turns light green and it switches to the object focus view of that object:
![](https://pad.gwdg.de/uploads/upload_592fdfb8c0b59e3c408a66ebb96f34d1.png)

If another node is selected as the second node, it switches to the calculation of the K shortest paths from the first to the second (here the second node is _thermal gradient hole_):
![](https://pad.gwdg.de/uploads/upload_a0c8fe6631354d229d2c6d2431b3f680.png)

<!--In autoref{node links} the function is introduced in more detail.-->
        
### Selection of method categories to restrict the display.
Some methods are equipped with the object property _hasMethodCat_ (Domain: _Method_, Range: _MethodCategory_). Right of the configuration buttons is now a column with a selection of these method categories:

![](https://pad.gwdg.de/uploads/upload_61ec82af33627d35d4d6ae32c9350afb.png)
<!--![](https://pad.gwdg.de/uploads/upload_474f1de9223aac183c569f5e5f8d4882.png)
previously ![](https://pad.gwdg.de/uploads/upload_5bc897e0e185a744150e338fbe1412d7.png)-->

If only certain categories are selected in a view, only statements with involved methods included in one/all (see _union/intersection_) are visualized.

The headings _Accuracy_ and _Scientific_ result from the _rdf:type_ of the MethodCategory (except the _Category_ in the name). If there are further superclasses, they are also displayed.
Below follow the MethodCategories (also without _Method_ in the original name, since everywhere occurring). As badge behind it: The number of existing nodes of this category in the current view.

_Toggle all_ selects all checkboxes. This is also the initial situation when starting the app or when no parameters are selected.

_unite_ and _intersect_ select whether methods are displayed for multiple selections, where it is sufficient that they belong to one of the categories (_unite_), or must belong to all categories.

:::info
**Example:** The above selection shows all _geometric_ methods as well as all _field_ methods and results in the following graph:

![](https://pad.gwdg.de/uploads/upload_5250ad7fc90e62871b37428c9c25f15c.png)

Choosing _intersect_ instead results in the following graph:

![](https://pad.gwdg.de/uploads/upload_471fcf84acf2efe82a0d82bcc5870bd8.png)
:::
_unclassified_ includes all methods that are not (yet) attached to a category (= do not have the _hasMethodCat_ property).

_Apply_ submits and implements the selection -> all selected categories are appended to the URL as the value of the _onlyMethodCat_ parameter. On the command line, the argument corresponds to _-onlyMethodCat=[value]_. _Reset_ resets the display.

By default, all method categories are selected and merged (_unite_ is enabled). 

### K shortest Paths regarding a medium
Parameters now have additional labels outside the node that describe the associated medium. Medium belonging to a node connects to the left edge.

![](https://pad.gwdg.de/uploads/upload_8affe769efe3a0574bb69363c94bf15a.png)
:::info
**Example:** _fracture set spacing_ has the medium _drill core_, _fracture spacing_ refers to _outcrop model_point_cloud_ and _fault spacing_ refers to _outcrop wall_.

![](https://pad.gwdg.de/uploads/upload_fae6b715eaa3c6a49ae8f192c7e69481.png) 
:::

When calculating the K shortest paths, if instead of one of the nodes (or both) a medium is selected by clicking on the medium label, all paths coming to/from a parameter that refers to that medium will be calculated.
:::info
**Example:** The K shortest paths from _compass measurement_ to the medium _outcrop wall_:

![](https://pad.gwdg.de/uploads/upload_b0fbdac39546a1962c7385cfd94c1c76.png)

![](https://pad.gwdg.de/uploads/upload_c98a6d527df57a3378b2e08772578ad4.png)
:::
 
#### Implementation
During the path calculation, an additional node with the medium as well as edges from it to the corresponding parameter or back (depending on the calculation direction) is inserted, 

## Command line arguments and web server parameters
For transparent configuration and to accommodate personal user preferences, in addition to using the web interface to request a specific display, parameters can also be entered directly into the URL. For this purpose, for a request of the web interface _[tomcat]/geothermalGraph/map?_ and for a request of the single graph image _[tomcat]/geothermalGraph/pic?_ are added with the parameters separated by _&_ (with _[tomcat]_ as placeholder for the URL of the used Tomcat web server). An overview of the available command line arguments and web service parameters with corresponding function descriptions:

feature | command line argument | parameter in the web server URL 
 ---|----|---
use test ontology | -\-testowl | owl=test (otherwise: owl=normal) 
no clusters | -\-noclustering | clustering=disable (otherwise: clustering=enable) 
with cluster but without cluster edges | -\-noclusteredges | clustering=noedges 
pre-selection of a node | -selectNode=_Name_ | selectNode=_Name_
Focus of an object with representation of the context component | -compOfCon=_Name_ | -compOfCon=_Name_ 
Calculation of the K shortest paths (KSP) | -kspSource=_source name_, -kspTarget=_target name_ | kspSource=_source name_, kspTarget=_target name_
For KSP: number of paths $K$ to be calculated and path $K^{th}$ to be displayed | -kspK=_integer_, -kspKth=_integer/_all__ | kspK=_integer_, kspKth=_integer/_all__
Request VocPrez SPARQL endpoint for alternative names | - (_enabled by default_) | withVocprez=enable
building the graph from left to right | -direction=lr (otherwise: nothing or -direction=tb) | direction=lr (otherwise: nothing or direction=tb) 
shape of methods / parameters nodes | -shapeMethod=_Form_ / -shapeParameter=_Form_ | shapeMethod=_Form_ / shapeParameter=_Form_ 
edge size | -edgeWidth=_integer_ | edgeWidth=_integer_ 
font size | -fontSize=_integer_ | fontSize=_integer_ 
Selection of only certain method categories | -onlyMethodCat=_classname_ | onlyMethodCat=_classname_
Intersect or unite category methods | -methodCatSelBy=intersect / unite (_default_) | methodCatSelBy=intersect / unite (_default_)
Overview of possible parameters | -\-help | -
