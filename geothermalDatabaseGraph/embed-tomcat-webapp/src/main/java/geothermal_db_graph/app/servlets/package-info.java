/**
 * These Servlets are used for building the JAR that
 * is used by the embedded Tomcat webserver.
 * <p>
 * The main difference to {@link owl2graph.servlets}:
 * {@link jakarta.servlet}-Classes are used instead of {@link javax.servlet}
 */
package geothermal_db_graph.app.servlets;