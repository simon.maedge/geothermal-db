package geothermal_db_graph.app.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;

/**
 * @todo
 */
@MultipartConfig
public class MergeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException{
        PrintWriter out = resp.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<body>");
        out.println("<t1>" + req.getParameter("user") +  "</t1>");
        out.println("<t1>" + req.getParameter("password") +  "</t1>");
        out.println("</body>");
        out.println("</html>");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException{

        // 1. APPROACH
        ;
        String description = req.getParameter("description"); // Retrieves <input type="text" name="description">
        Part filePart = req.getPart("file"); // Retrieves <input type="file" name="file">
        String fileNamee = Paths.get(filePart.getHeader("filename")/*in template: .getSubmittedFilename()*/).getFileName().toString(); // MSIE fix.
        InputStream fileContent = filePart.getInputStream();

        response(resp, "quackquackquaaaaack....:(");

        // END 1. APPROACH

        String uploadPath = getServletContext().getRealPath("") + File.separator + "fileupload";
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) uploadDir.mkdir();

        for (Part part : req.getParts()) {
            String fileName = getFileName(part);
            part.write(uploadPath + File.separator + fileName);
        }

        // this only works because HTTPServlet already handles the parsing of
        // content-type "application/x-www-form-urlencoded"!
        // response(resp,"alles ok :)");
    }

    private String getFileName(Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename"))
                return content.substring(content.indexOf("=") + 2, content.length() - 1);
        }
        return "somethingsomething";
    }

    private void response(HttpServletResponse resp, String msg)
            throws IOException {
        PrintWriter out = resp.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<t1>" + msg + "</t1>");
        out.println("</body>");
        out.println("</html>");
    }
}
