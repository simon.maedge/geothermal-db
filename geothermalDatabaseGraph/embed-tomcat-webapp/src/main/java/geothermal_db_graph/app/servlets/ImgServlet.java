package geothermal_db_graph.app.servlets;

import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.parse.Parser;
import geothermal_db_graph.app.main.GeothermalOwlGraph;

import javax.imageio.ImageIO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Arrays;

/**
 * Show the picture of the graph. Normally this Servlet is requested
 * by {@link ImgServlet}
 *
 * The Servlet is started by Tomcat calling _[tomcat]/geothermalGraph/img?[parameter]_.
 * It calls the main class like {@link MapServlet} with approximately the same
 * parameters, except that it expects an image instead of an HTML file
 * and returns that as a response
 */
public class ImgServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        BufferedImage image;
        String dot = req.getParameter("dot");

        // case: only dot code given
        if (dot != null) {
            image = generateGraphVizImage(dot);
        }
        // case: only parameter given
        else {
            // * parameters
            // owl=test&compOfCon=disable&clustering=disable

            String[] geothermalArgs = new String[128]; // todo adjus
            Arrays.fill(geothermalArgs, "");
            int i = 0;

            // Owl
            // : normal, test
            String parOwl = req.getParameter("owl");
            if (parOwl != null && parOwl.equals("test")) {
                geothermalArgs[i] = "--testowl";
                i++;
            }


            // compOfCon // a.k.a. Pathfinding
            String parCompOfCon = req.getParameter("compOfCon");
            if (parCompOfCon == null || parCompOfCon.equals("disable")) {
                geothermalArgs[i] = "--nopathfinding";
                i++;
            } else {
                geothermalArgs[i] = "-compOfCon=" + parCompOfCon;
                i++;
            }


            // Clustering
            // : enable, noedges, disable
            String parClustering = req.getParameter("clustering");
            if (parClustering != null) {
                switch (parClustering) {
                    case "enable":
                        break;
                    case "noedges":
                        geothermalArgs[i] = "--noclusteredges";
                        i++;
                        break;
                    case "disable":
                        geothermalArgs[i] = "--noclustering";
                        i++;
                        break;
                    default:
                        break;
                }
            }

            // ksp
            // kspSource: <String:localname>
            // kspTarget: <String:localname>
            String parKspSource = req.getParameter("kspSource");
            String parKspTarget = req.getParameter("kspTarget");
            if (parKspSource != null && parKspTarget != null) {
                geothermalArgs[i] = "-kspSource=" + parKspSource;
                i++;
                geothermalArgs[i] = "-kspTarget=" + parKspTarget;
                i++;
            }

            // kspK: <int:k>
            // kspKth: <int:kth>
            String parK = req.getParameter("kspK");
            String parKth = req.getParameter("kspKth");
            if (parK != null) {
                geothermalArgs[i] = "-kspK=" + parK;
                i++;
            }
            if (parKth != null) {
                geothermalArgs[i] = "-kspKth=" + parKth;
                i++;
            }


            // selectNode
            // : <localname>
            String parSelectNode = req.getParameter("selectNode");
            if (parSelectNode != null) {
                geothermalArgs[i] = "-selectNode=" + parSelectNode;
                i++;
            }

            // theme
            // : dark, light
            String parTheme = req.getParameter("theme");
            if (parTheme != null) {
                geothermalArgs[i] = "-theme=" + parTheme;
                i++;
            }

            // direction
            // : lr (left-right) [tb (top-bottom, is default)]
            String parDirection = req.getParameter("direction");
            if (parDirection != null && parDirection.equals("lr")) {
                geothermalArgs[i] = "-direction=lr";
                i++;
            }

            // shape Method
            // : rect, oval ...
            String parShapeMethod = req.getParameter("shapeMethod");
            if (parShapeMethod != null) {
                geothermalArgs[i] = "-shapeMethod=" + parShapeMethod;
                i++;
            }
            // shape Parameter
            // : rect, oval ...
            String parShapeParameter = req.getParameter("shapeParameter");
            if (parShapeParameter != null) {
                geothermalArgs[i] = "-shapeParameter=" + parShapeParameter;
                i++;
            }
            // edge width
            // : [int]
            String parEdgeWidth = req.getParameter("edgeWidth");
            if (parEdgeWidth != null) {
                geothermalArgs[i] = "-edgeWidth=" + parEdgeWidth;
                i++;
            }
            // fontsize
            // : [int pt]
            String parFontSize = req.getParameter("fontSize");
            if (parFontSize != null) {
                geothermalArgs[i] = "-fontSize=" + parFontSize;
                i++;
            }

            String[] parMethodCats = req.getParameterValues("onlyMethodCat");
            if (parMethodCats != null && parMethodCats.length != 0) {
                for (String par : parMethodCats) {
                    geothermalArgs[i] = "-onlyMethodCat=" + par;
                    i++;
                }
            }

            String parMethodCatSelBy = req.getParameter("methodCatSelBy");
            if (parMethodCatSelBy != null) {
                geothermalArgs[i] = "-methodCatSelBy=" + parMethodCatSelBy;
                i++;
            }


            // * main

            // ServletContext sc = getServletContext();

            GeothermalOwlGraph geoOwlGr = new GeothermalOwlGraph(geothermalArgs);
            geoOwlGr.setCallerToWebserver();

            geoOwlGr.setThisQueryString("" + req.getQueryString());
            geoOwlGr.make(geothermalArgs);

            image = geoOwlGr.getGraphDisplay().getImage();
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        // it is the responsibility of the container to close output stream
        OutputStream os = resp.getOutputStream();
        ImageIO.write(image, "png", os);
        os.write(byteArrayOutputStream.toByteArray());

        InputStream is = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());


        resp.setContentType("image/png");

        byte[] buffer = new byte[1024];
        int bytesRead;


        while ((bytesRead = is.read(buffer)) != -1) {

            os.write(buffer, 0, bytesRead);
        }


    }

    /**
     * Use the GraphVIZ-Api to generate a SVG-Image
     * and creates HTML-CMAPX-MAP
     * (only place where it is used)
     *
     * @param graphString query-String
     * @return the SVG-Image
     */
    public BufferedImage generateGraphVizImage(String graphString) {
        MutableGraph g;
        BufferedImage image = null;
        try {

            g = new Parser().read(graphString);
            //image = Graphviz.fromGraph(g).rasterize(Rasterizer.SALAMANDER).toImage();
            image = Graphviz.fromGraph(g).render(Format.PNG).toImage();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // this only works because HTTPServlet already handles the parsing of
        // content-type "application/x-www-form-urlencoded"!

        doGet(req, resp);
    }

}
