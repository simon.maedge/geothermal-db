package geothermal_db_graph.app.servlets;

import geothermal_db_graph.app.main.GeothermalOwlGraph;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class TestServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // * parameters
        req.getParameterNames();


        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        String[] geothermalArgs = {"--testowl"};
        GeothermalOwlGraph geoOwlGr = new GeothermalOwlGraph();
        geoOwlGr.make(geothermalArgs);
        String htmlString = geoOwlGr.getMadeHTML();

        out.print(htmlString);
        out.flush();
        out.close();


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // this only works because HTTPServlet already handles the parsing of
        // content-type "application/x-www-form-urlencoded"!

        doGet(req, resp);
    }

}
