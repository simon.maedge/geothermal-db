package geothermal_db_graph.app.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import geothermal_db_graph.app.main.GeothermalOwlGraph;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 * Show the full html web interface with including an image requested by
 * {@link MapServlet}
 *
 * The Servlet is started by calling _[tomcat]/geothermalGraph/map?[parameter]_
 * through Tomcat (with _[tomcat]_ as a placeholder for the URL of the Tomcat web server
 * used and _[parameter]_ for possible parameters).
 * This mapping is found in Tomcat's _web.xml_.
 * It uses supplied parameters as arguments to call the main class and returns
 * an HTML page in response.
 */
public class MapServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // req.getParameterMap();

        // * parameters
        // owl=test&compOfCon=disable&clustering=disable

        String[] geothermalArgs = new String[128]; // todo adjus
        Arrays.fill(geothermalArgs, "");
        int i = 0;

        // Owl
        // : normal, test
        String parOwl = req.getParameter("owl");
        if (parOwl != null && parOwl.equals("test")) {
            geothermalArgs[i] = "--testowl";
            i++;
        }


        // compOfCon // a.k.a. Pathfinding
        String parCompOfCon = req.getParameter("compOfCon");
        if (parCompOfCon != null && !parCompOfCon.equals("disable")) {
            geothermalArgs[i] = "-compOfCon=" + parCompOfCon;
            i++;
        }


        // Clustering
        // : enable, noedges, disable
        String parClustering = req.getParameter("clustering");
        if (parClustering != null) {
            switch (parClustering) {
                case "enable":
                    break;
                case "noedges":
                    geothermalArgs[i] = "--noclusteredges";
                    i++;
                    break;
                case "disable":
                    geothermalArgs[i] = "--noclustering";
                    i++;
                    break;
                default:
                    break;
            }
        }

        // ksp
        // kspSource: <localname>
        // kspTarget: <localname>
        String parKspSource = req.getParameter("kspSource");
        String parKspTarget = req.getParameter("kspTarget");
        if (parKspSource != null && parKspTarget != null) {
            geothermalArgs[i] = "-kspSource=" + parKspSource;
            i++;
            geothermalArgs[i] = "-kspTarget=" + parKspTarget;
            i++;
        }
        // kspK: <int:k>
        // kspKth: <int:kth>
        String parKth = req.getParameter("kspKth"); // 1st if this and following line switched: ArrayIndexOutOfBounds for index 6 and length 6
        String parK = req.getParameter("kspK");     // 2nd
        if (parKth != null) {
            geothermalArgs[i] = "-kspKth=" + parKth;
            i++;
        }
        if (parK != null) {
            geothermalArgs[i] = "-kspK=" + parK;
            i++;
        }

        // selectNode
        // : <localname>
        String parSelectNode = req.getParameter("selectNode");
        if (parSelectNode != null) {
            geothermalArgs[i] = "-selectNode=" + parSelectNode;
            i++;
        }

        // direction
        // : lr (left-right) [tb (top-bottom, is default)]
        String parDirection = req.getParameter("direction");
        if (parDirection != null && parDirection.equals("lr")) {
            geothermalArgs[i] = "-direction=lr";
            i++;
        }

        // theme
        // : dark, light
        String parTheme = req.getParameter("theme");
        if (parTheme != null) {
            geothermalArgs[i] = "-theme=" + parTheme;
            i++;
        }


        // shape Method
        // : rect, oval ...
        String parShapeMethod = req.getParameter("shapeMethod");
        if (parShapeMethod != null) {
            geothermalArgs[i] = "-shapeMethod=" + parShapeMethod;
            i++;
        }
        // shape Parameter
        // : rect, oval ...
        String parShapeParameter = req.getParameter("shapeParameter");
        if (parShapeParameter != null) {
            geothermalArgs[i] = "-shapeParameter=" + parShapeParameter;
            i++;
        }
        // edge width
        // : [int]
        String parEdgeWidth = req.getParameter("edgeWidth");
        if (parEdgeWidth != null) {
            geothermalArgs[i] = "-edgeWidth=" + parEdgeWidth;
            i++;
        }
        // fontsize
        // : [int pt]
        String parFontSize = req.getParameter("fontSize");
        if (parFontSize != null) {
            geothermalArgs[i] = "-fontSize=" + parFontSize;
            i++;
        }

        String[] parMethodCats = req.getParameterValues("onlyMethodCat");
        if (parMethodCats != null && parMethodCats.length != 0) {
            for (String par : parMethodCats) {
                geothermalArgs[i++] = "-onlyMethodCat=" + par;
            }
        }

        String parMethodCatSelBy = req.getParameter("methodCatSelBy");
        if (parMethodCatSelBy != null) {
            geothermalArgs[i] = "-methodCatSelBy=" + parMethodCatSelBy;
            i++;
        }


        // ! only for map
        // request VocPrez Endpoint
        // : [int pt]
        String parVocprez = req.getParameter("withVocprez");
        if (parVocprez != null) {
            geothermalArgs[i] = "--withVocprez";
            i++;
        }


        // * main

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        GeothermalOwlGraph geoOwlGr = new GeothermalOwlGraph(geothermalArgs);
        geoOwlGr.setCallerToWebserver();
        geoOwlGr.setThisQueryString("" + req.getQueryString().replace("&withVocprez=enable", ""));
        geoOwlGr.make(geothermalArgs);
        String rawHtmlString = geoOwlGr.getMadeHTML(); // !!! gets null

        String paramString = req.getQueryString();

        // debug
        StringBuilder argsStr = new StringBuilder("[");
        for (String arg : geothermalArgs) {
            argsStr.append(arg);
            argsStr.append(" ");
        }
        argsStr.append("]");
        if (rawHtmlString == null) {
            out.print("calculated map-html is empty (rawHtmlString==null!); parameter " + argsStr);
        } else {
            /*String htmlString = rawHtmlString.replace("\"pic\"", "\"pic?dot=" + test + "\"");
            */
            String htmlString = rawHtmlString.replace("\"pic\"", "\"pic?"
                    + paramString.replace("&withVocprez=enable", "")
                    + "\"");

            out.print(htmlString);
        }
        out.flush();
        out.close();


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // this only works because HTTPServlet already handles the parsing of
        // content-type "application/x-www-form-urlencoded"!

        doGet(req, resp);
    }

}
