package geothermal_db_graph.app.main;

import geothermal_db_graph.app.config.Preferences;
import org.apache.jena.ontology.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.XSD;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Enables an advanced setup of the model. Over the normal reading of the ontology,
 * a restricted context component of a specific individual is detected by certain
 * patterns and the affected individuals are marked in the model.
 * <p>
 * It is optionally called by the main class GeothermalOwlGraph. It thus enables
 * the required representation of a subgraph with respect to a specific object.
 */
public class CompOfCon {

    /**
     * log4j-logger of this class. Preferences defined in ./log4j2.xml
     */
    private static final Logger logger = LogManager.getLogger(CompOfCon.class);

    // VARS

    /**
     * model to rely and work on
     */
    private final OntModel model;

    private OntProperty depthProperty;
    private OntProperty inPathProperty;
    private OntProperty markedProperty;

    // CONSTRUCTOR

    /**
     * constructs the CompOfCon
     *
     * @param owlpath path of owl to rely on
     */
    CompOfCon(String owlpath) {
        this.model = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
        model.read(owlpath);
    }


    // FIND PATHS


    /**
     * Sets all Path-Depth properties on 1; all other don't have this component
     * (checkable by hasDepthProperty(Individual)
     *
     * @param individual individual to start checking
     */
    public void findComponentOfConnectivityOf(Individual individual) {
        dfs(individual, Level.ROOT);
    }

    /**
     * Sets all Path-Depth properties on 1; all other don't have this component
     * (checkable by hasDepthProperty(Individual)
     *
     * @param individualStr individual string to start checking
     */
    public void findComponentOfConnectivityOf(String individualStr) {
        initProperties();
        ResIterator list = model.listResourcesWithProperty(model.getProperty(Preferences.PREFIX + Preferences.NAME_IRI));
        StringBuilder stringBuilder = new StringBuilder();
        /*for (; list.hasNext(); ) {
            Resource res = list.next();

            stringBuilder.append(res.getURI());
            stringBuilder.append("\n");
        }
        System.out.println(stringBuilder.toString());*/
        dfs(model.getResource(individualStr), Level.ROOT);
        setImportancePropertyBlue(model.getIndividual(individualStr));
    }

    /**
     * DFS for finding connections
     *
     * @param resource root element
     */
    private void dfs(Resource resource, Level level) {
        setDepthProperty1(resource);
        logger.trace("DFS: " + resource.getLocalName());


        // 1
        if (level != Level.DESCENDANT || !Preferences.REMOVE_SIBLINGS) {
            logger.trace("// 1");
            //dfsRecursionByIri(resource, true, Preferences.PREFIX + Preferences.FROM_METHOD_IRI, (level == Level.ROOT) ? Level.ANCESTOR : level); // 1
            for (String iri : Preferences.FROM_METHOD_IRI_ABCO) {
                dfsRecursionByIri(resource, true, Preferences.PREFIX + iri, (level == Level.ROOT) ? Level.ANCESTOR : level); // 1
            }
        }
        // 2
        logger.trace("// 2");
        //dfsRecursionByIri(resource, false, Preferences.PREFIX + Preferences.TO_METHOD_IRI, (level == Level.ROOT) ? Level.ANCESTOR : level); // 2
        for (String iri : Preferences.TO_METHOD_IRI_MO) {
            dfsRecursionByIri(resource, false, Preferences.PREFIX + iri, (level == Level.ROOT) ? Level.ANCESTOR : level); // 2
        }
        // 3
        logger.trace("// 3");
        //dfsRecursionByIri(resource, false, Preferences.PREFIX + Preferences.FROM_METHOD_IRI, (level == Level.ROOT || level == Level.ANCESTOR) ? Level.DESCENDANT : level); // 3
        for (String iri : Preferences.FROM_METHOD_IRI_ABCO) {
            dfsRecursionByIri(resource, false, Preferences.PREFIX + iri, (level == Level.ROOT || level == Level.ANCESTOR) ? Level.DESCENDANT : level); // 3
        }
        // 4
        if (level != Level.ANCESTOR || !Preferences.REMOVE_SIBLINGS) {
            logger.trace("// 4");
            //dfsRecursionByIri(resource, true, Preferences.PREFIX + Preferences.TO_METHOD_IRI, (level == Level.ROOT) ? Level.DESCENDANT : level); // 4
            for (String iri : Preferences.TO_METHOD_IRI_MO) {
                dfsRecursionByIri(resource, true, Preferences.PREFIX + iri, (level == Level.ROOT) ? Level.DESCENDANT : level); // 4
            }
        }

        // INVERSE
        // 1
        if (level != Level.DESCENDANT || !Preferences.REMOVE_SIBLINGS) {
            logger.trace("// 1");
            //dfsRecursionByIri(resource, true, Preferences.PREFIX + Preferences.FROM_METHOD_IRI, (level == Level.ROOT) ? Level.ANCESTOR : level); // 1
            for (String iri : Preferences.FROM_METHOD_IRI_ABCO_INVERSE) {
                dfsRecursionByIri(resource, false, Preferences.PREFIX + iri, (level == Level.ROOT) ? Level.ANCESTOR : level); // 1
            }
        }
        // 2
        logger.trace("// 2");
        //dfsRecursionByIri(resource, false, Preferences.PREFIX + Preferences.TO_METHOD_IRI, (level == Level.ROOT) ? Level.ANCESTOR : level); // 2
        for (String iri : Preferences.TO_METHOD_IRI_MO_INVERSE) {
            dfsRecursionByIri(resource, true, Preferences.PREFIX + iri, (level == Level.ROOT) ? Level.ANCESTOR : level); // 2
        }
        // 3
        logger.trace("// 3");
        //dfsRecursionByIri(resource, false, Preferences.PREFIX + Preferences.FROM_METHOD_IRI, (level == Level.ROOT || level == Level.ANCESTOR) ? Level.DESCENDANT : level); // 3
        for (String iri : Preferences.FROM_METHOD_IRI_ABCO_INVERSE) {
            dfsRecursionByIri(resource, true, Preferences.PREFIX + iri, (level == Level.ROOT || level == Level.ANCESTOR) ? Level.DESCENDANT : level); // 3
        }
        // 4
        if (level != Level.ANCESTOR || !Preferences.REMOVE_SIBLINGS) {
            logger.trace("// 4");
            //dfsRecursionByIri(resource, true, Preferences.PREFIX + Preferences.TO_METHOD_IRI, (level == Level.ROOT) ? Level.DESCENDANT : level); // 4
            for (String iri : Preferences.TO_METHOD_IRI_MO_INVERSE) {
                dfsRecursionByIri(resource, false, Preferences.PREFIX + iri, (level == Level.ROOT) ? Level.DESCENDANT : level); // 4
            }
        }

    }

    // DFS

    /*  Algorithms for programming contests: 05.graph_theory.pdf slide 17

        vector <int > edges [ MAXN ];
        int used [ MAXN ];

        void dfs ( int v) {
            used [v] = 1;

            for ( auto u: edges [v])
                if (! used [u])
                    dfs (u);
        }
     */

    /**
     * DFS for a specific PropertyIri, by which the resources should be connected
     *
     * @param resource       root elt
     * @param connectedByIri PropertyIri, by which the resources should be connected
     */
    private void dfsRecursionByIri(Resource resource, Boolean x_to_given, String connectedByIri, Level level) {

        StmtIterator it1;
        if (!x_to_given) {
            it1 = model.listStatements(
                    resource,
                    model.getProperty(connectedByIri),
                    (RDFNode) null
            );
        } else {
            it1 = model.listStatements(
                    null,
                    model.getProperty(connectedByIri),
                    resource
            );

        }

        for (StmtIterator it = it1; it.hasNext(); ) {
            Statement currentStmt = it.next();
            logger.debug(("DFS-it1-currentStmt: " + level + " " + currentStmt).replace(Preferences.PREFIX, ""));
            Resource currentResource =
                    (x_to_given)
                            ? currentStmt.getSubject().asResource()
                            : currentStmt.getObject().asResource();

            if (!hasDepthProperty(currentResource)) {
                logger.debug("DFS-it1-currentResource: " + currentResource.getLocalName());
                dfs(currentResource, level);
            }
        }
    }

    /**
     * new Datatype Props for GeoThings
     * <p>
     * compOfConProperties
     * |- inPath (short): number of the path the GeoThing is contained
     * |- pathdepth (short): whether uses | depth by dfs
     */
    private void initProperties() {
        DatatypeProperty compOfConProperty = model.createDatatypeProperty(Preferences.PREFIX + Preferences.PATHFINDER_IRI);

        // (<GeoObj>, <in Path>, <Short>)
        DatatypeProperty pathProperty = model.createDatatypeProperty(Preferences.PREFIX + Preferences.PATHS_IRI);
        pathProperty.addSuperProperty(compOfConProperty);
        pathProperty.addDomain(model.getOntClass(Preferences.PREFIX + Preferences.PATHS_DOMAIN_IRI));
        pathProperty.addRange(XSD.xshort);

        // (<GeoThing>, <pathsDepth>, <Short>)
        DatatypeProperty thisDepthProperty = model.createDatatypeProperty(Preferences.PREFIX + Preferences.PATHS_DEPTH_IRI);
        thisDepthProperty.addSuperProperty(compOfConProperty);
        thisDepthProperty.addDomain(model.getOntClass(Preferences.PREFIX + Preferences.PATHS_DOMAIN_IRI));
        thisDepthProperty.addRange(XSD.xshort);

        // (<GeoThing>, <markedAs>, <String>)
        DatatypeProperty thisMarkedProperty = model.createDatatypeProperty(Preferences.PREFIX + Preferences.IMPORTANCE_IRI);
        thisMarkedProperty.addSuperProperty(compOfConProperty);
        thisMarkedProperty.addDomain(model.getOntClass(Preferences.PREFIX + Preferences.PATHS_DOMAIN_IRI));
        thisMarkedProperty.addRange(XSD.xstring);

        // add to this file's variables
        depthProperty = model.getDatatypeProperty(Preferences.PREFIX + Preferences.PATHS_DEPTH_IRI);
        inPathProperty = model.getDatatypeProperty(Preferences.PREFIX + Preferences.PATHS_IRI);
        markedProperty = model.getDatatypeProperty(Preferences.PREFIX + Preferences.PATHS_IRI);

    }


    // PROPERTY HELPING METHODS

    /**
     * add a inPath property to individual
     *
     * @param individual individual
     * @param value      value
     */
    private void addInPathProperty(Individual individual, String value) {
        individual.addProperty(inPathProperty, value);
    }

    /**
     * add a pathDepth property to individual
     *
     * @param resource individual
     */
    private void setDepthProperty1(Resource resource) {
        if (!resource.hasProperty(depthProperty))
            resource.addProperty(
                    model.getProperty(Preferences.PREFIX + Preferences.PATHS_DEPTH_IRI),
                    "1"
            );
    }

    /**
     * add a pathDepth property to individual
     *
     * @param resource individual
     */
    private void setDepthPropertyMinus1(Resource resource) {
        if (!resource.hasProperty(depthProperty))
            resource.addProperty(
                    model.getProperty(Preferences.PREFIX + Preferences.PATHS_DEPTH_IRI),
                    "-1"
            );
    }

    /**
     * add an importance property to individual
     *
     * @param resource individual
     */
    private void setImportancePropertyBlue(Resource resource) {
        resource.addProperty(
                model.getProperty(Preferences.PREFIX + Preferences.IMPORTANCE_IRI),
                Preferences.Importance.HIGH.toString()
        );
    }

    /**
     * add a pathDepth property to individual
     *
     * @param resource individual
     */
    private void setDepthProperty(Resource resource, int depth) {
        if (!resource.hasProperty(depthProperty)) {
            resource.addProperty(
                    model.getProperty(Preferences.PREFIX + Preferences.PATHS_DEPTH_IRI),
                    Integer.toString(depth)
            );
        }
    }

    /**
     * does the individual have a Depth-Property?
     *
     * @param resource individual
     */
    private boolean hasDepthProperty(Resource resource) {
        return resource.hasProperty(depthProperty);
    }

    /**
     * especially for test cases
     */
    public void writeModelToDisk(boolean isTestOwl) throws IOException {
        String filename = (isTestOwl)
                ? Preferences.PATHFINDERED_TEST_SOURCE
                : Preferences.PATHFINDERED_SOURCE;

        try (OutputStream os = new BufferedOutputStream(new FileOutputStream(filename))) {
            model.write(os);
        }
    }

    /**
     * get the Model
     *
     * @return model in current state
     */
    public OntModel getModel() {
        return model;
    }


    // GETTER&SETTER

    /**
     * describes
     * the relation of a node
     * to the (compOfCon) root
     */
    private enum Level {
        ANCESTOR,
        ROOT,
        DESCENDANT
    }

}
