package geothermal_db_graph.app.main;

import geothermal_db_graph.app.things.GeothermalEdge;
import geothermal_db_graph.app.things.GeothermalObject;
import geothermal_db_graph.app.things.GeothermalSuperObject;
import org.apache.jena.graph.Triple;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import geothermal_db_graph.app.io.GraphDisplay;
import geothermal_db_graph.app.pathfinding.graph.Edge;
import geothermal_db_graph.app.pathfinding.graph.Graph;
import geothermal_db_graph.app.pathfinding.graph.ksp.Yen;
import geothermal_db_graph.app.pathfinding.graph.util.Path;

import java.util.*;

/**
 * Manages paths. An important function of the program is to calculate
 * the K shortest paths. For this purpose GeoKSP receives the nodes of
 * the graph, the names of the start and end nodes and information about
 * the number of paths to be calculated. If the {@link #runKsp} method of the class
 * is executed, the objects needed for Yen's algorithm are prepared and
 * the algorithm is executed with the Yen class. The results are then
 * recorded in this GeoKSP object.
 * <p>
 * It is also a function that is executed only optionally.
 * <p>
 * It uses the {@link Yen} class which, with the help of some auxiliary classes (both in the
 * {@link pathfinding} package), runs Yen's algorithm to compute
 * the K shortest paths. It is instantiated and executed by GeoKSP.
 * Result is a collection of the K shortest paths and is returned to GeoKSP.
 */
public class GeoKSP {

    /**
     * log4j-logger of this class. Preferences defined in ./log4j2.xml
     */
    private static final Logger logger = LogManager.getLogger(GeoKSP.class);
    private final Graph graph;
    /**
     * the resulting paths
     */
    List<Path> ksp;
    private String sourceNode, targetNode;
    private int k;


    /**
     * constructor
     */
    public GeoKSP(Graph graph) {
        this.graph = graph;
    }

    /**
     * constructor
     */
    public GeoKSP(List<GeothermalObject> geothermalObjects) {
        graph = new Graph(geothermalObjects);
    }

    /**
     * constructor;
     * here addNode needed
     */
    public GeoKSP() {
        this.graph = new Graph();
    }

    /**
     * add parameters medium and all edges
     * <p>
     * weight=0 because there is nothing between a parameter and its medium
     *
     * @param medium
     * @param mediumsParameters
     */
    public void addMedium(String medium, List<Triple> mediumsParameters, boolean sourceIsMedium) {
        graph.addNode(medium);
        for (Triple mediumsParameter : mediumsParameters) {
            if (sourceIsMedium) { // subject is medium
                graph.addEdge(medium, mediumsParameter.getObject().getLocalName(), "medium2" + mediumsParameter.getObject().getLocalName(), 0, mediumsParameter);
            } else { // target is medium, object is medium
                graph.addEdge(mediumsParameter.getSubject().getLocalName(), medium, mediumsParameter.getSubject().getLocalName() + "2medium", 0, mediumsParameter);
            }
        }
    }

    /**
     * adds geoObj to the graph
     *
     * @param geothermalObject node to add
     */
    public void addNode(GeothermalObject geothermalObject) {
        graph.addNode(geothermalObject);
    }


    /**
     * runs Yen's
     *
     * @param sourceNode from this node
     * @param targetNode to this node
     * @param k          number of shortest paths (till k-th shortest path)
     */
    public void runKsp(String sourceNode, String targetNode, int k) {
        // logger configuration
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.INFO);

        this.sourceNode = sourceNode;
        this.targetNode = targetNode;
        this.k = k;

        Yen yenAlgorithm = new Yen();
        logger.debug("Start Yen: Computing "
                + k + "-shortest paths from "
                + sourceNode + " to "
                + targetNode + ".");
        ksp = yenAlgorithm.kShortestPaths(graph, sourceNode, targetNode, k);
        logger.debug("Finished Yen");

    }

    /**
     * @return all nodes (as String) that are in at least one of the Paths of {@link #ksp}
     */
    public LinkedHashSet<String> getInvolvedNodes(int kth) {
        LinkedHashSet<String> nodes = new LinkedHashSet<>();
        if (kth == Integer.MAX_VALUE) {
            for (Path path : ksp) {
                nodes.addAll(path.getNodes());
            }
        } else {
            nodes.addAll(ksp.get(kth).getNodes());
        }
        return nodes;
    }

    /**
     * @return all edges (as Edge) that are in at least one of the Paths of {@link #ksp}
     */
    public LinkedHashSet<Edge> getInvolvedEdges(int kth) {
        LinkedHashSet<Edge> edges = new LinkedHashSet<>();
        if (kth == Integer.MAX_VALUE) {
            for (Path path : ksp) {
                edges.addAll(path.getEdges());
            }
        } else {
            edges.addAll(ksp.get(kth).getEdges());
        }
        return edges;
    }

    /**
     * remove Geo-Object not contained in the paths
     *
     * @param graphDisplay the ontology that should be changed to fit ksp results
     */
    public void applyOnOwlGraph(GraphDisplay graphDisplay, int kth) {

        if (kth < Integer.MAX_VALUE && kth >= ksp.size()) {
            kth = ksp.size() - 1;
            logger.error("kth greater than the number of paths. kth set to paths-size");
        }
        final int finalKth = kth;

        // remove Geo-Objects
        LinkedList<String> geoObjList = new LinkedList<>(graphDisplay.getGeoObjects().keySet());
        LinkedHashSet<String> involvedNodes = getInvolvedNodes(kth);
        LinkedHashSet<Edge> involvedEdges = getInvolvedEdges(kth);


        for (String node : geoObjList) {
            if (!isInvolvedNode(involvedNodes, node)) {
                graphDisplay.getGeoObjects().remove(node);
                logger.trace("REMOVED " + node);
            } else {

                // remove Geo-Edge fixme
                graphDisplay.getGeoObject(node).getIncoming().removeIf(edge -> !isInvolvedEdge(involvedEdges, edge));
                graphDisplay.getGeoObject(node).getOutgoing().removeIf(edge -> !isInvolvedEdge(involvedEdges, edge));
            }
        }

        // remove from super geo objs
        LinkedList<GeothermalSuperObject> superGeoObjList = new LinkedList<>(graphDisplay.getSuperGeoObjects());
        for (GeothermalSuperObject geothermalSuperObj : superGeoObjList) {
            LinkedList<GeothermalObject> geoObjList_s = new LinkedList<>(geothermalSuperObj.getGeoObjects());

            for (GeothermalObject node : geoObjList_s) {
                String nodeStr = node.getLocalName();
                if (!isInvolvedNode(involvedNodes, nodeStr)) {
                    geothermalSuperObj.getGeoObjects().remove(node);
                    logger.debug("REMOVED " + nodeStr);
                } else {

                    // remove Geo-Edge
                    geothermalSuperObj.getIncoming().removeIf(edge -> !isInvolvedEdge(involvedEdges, edge));
                    geothermalSuperObj.getOutgoing().removeIf(edge -> !isInvolvedEdge(involvedEdges, edge));
                }
            }
        }

    }

    /**
     * @return whether node is involved in involvedNodes
     */
    private boolean isInvolvedNode(LinkedHashSet<String> involvedNodes, String node) {
        // return involvedNodes.stream().anyMatch(node::equals);

        // long coded
        boolean involved = false;
        for (String involvedNode : involvedNodes) {
            if (node.equals(involvedNode)) {
                involved = true;
                break;
            }
        }
        return involved;

    }

    /**
     * @return whether node is involved in involvedNodes
     */
    private boolean isInvolvedEdge(LinkedHashSet<Edge> involvedEdges, GeothermalEdge edge) {
        boolean involved = false;

        for (Edge involvedEdge : involvedEdges) {

            if (edge.getFromNode().getLocalName().equals(involvedEdge.getFromNode())
                    && edge.getToNode().getLocalName().equals(involvedEdge.getToNode())) {
                involved = true;
                break;
            }
        }
        return involved;
    }

    /**
     * @return GeoKSP string representation with important information
     */
    public List<Path> getKspPaths() {
        return ksp;
    }


    /*
    @Override
    public String toString() {
        String str = "sourceNode='" + sourceNode + "'" +
                ", targetNode='" + targetNode + "'" +
                ", k=" + k + ",\n" +
                "ksp=\n";
        for (Path p : ksp) {
            str += p + "\n";
        }
        str += "involvedNodes=\n";
        for (String node : getInvolvedNodes(Integer.MAX_VALUE)) {
            str += node + "\n";
        }
        return str;
    }
    */
}
