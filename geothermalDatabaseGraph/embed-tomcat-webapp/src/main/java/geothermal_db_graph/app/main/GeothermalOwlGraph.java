package geothermal_db_graph.app.main;

import geothermal_db_graph.app.config.CallConfig;
import geothermal_db_graph.app.config.Preferences;
import geothermal_db_graph.app.things.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.graph.Triple;
import org.apache.jena.ontology.*;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdf.model.impl.StatementImpl;
import org.apache.jena.vocabulary.RDF;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;

import geothermal_db_graph.app.io.GraphDisplay;
import geothermal_db_graph.app.pathfinding.graph.util.*;

import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * Represent the main class. When the Webservice is used, this class is
 * called by a servlet and returns its results to the servlet.
 * <p>
 * To do this, depending on the configuration, starts by reading the ontology or asking
 * CompOfCon class to set up a model, assigns shortest path calculations if
 * necessary, queries the VocPrez endpoint, and finally passes all information
 * important for display to the GraphDisplay class. After GeothermalOwlGraph
 * has generated the output, it passes the response back to the servlet.
 */
public class GeothermalOwlGraph {

    /**
     * log4j-logger of this class. Preferences defined in ./log4j2.xml
     */
    private final Logger logger = LogManager.getLogger(GeothermalOwlGraph.class);
    CallConfig config;
    /**
     * if true, true for whole project.
     * assigned by args[1]
     */
    private boolean isJUnitTest = false;
    /**
     * An owl2graph.OwlGraph with all the GeothermalObjects we use
     */
    private GraphDisplay graphDisplay;
    private OntModel model;
    private String owlPrefix;
    private Property nameProperty, toMethodProperty, fromMethodProperty, importanceProperty, weightProperty;
    private Property[] fromMethodProperties_ABCO, toMethodProperty_MO, fromMethodProperties_ABCO_inverse, toMethodProperty_MO_inverse;
    private List<Path> kspPaths;
    private HashMap<String, LinkedList<String>> allMethodCats = new LinkedHashMap<>(); // <Category Class, Method Category>
    private HashMap<String, Integer> allMethodCatsOccurrences = new LinkedHashMap<>(); // <Method Category, Num Of Occurrences>

    /**
     * constructor with parameter map
     *
     * @param map mapping parameter with value(s). (e.g. [clustering, disable])
     */
    public GeothermalOwlGraph(Map<String, String[]> map) {
        config = new CallConfig(map);
    }

    /**
     * constructor with command line arguments
     *
     * @param args command line arguments (e.g. -clustering=disable)
     */
    public GeothermalOwlGraph(String[] args) {
        config = new CallConfig(args);
    }

    /**
     * default constructor
     */
    public GeothermalOwlGraph() {
    }

    ;

    /**
     * here happens the main-part
     * <p>
     * <h2>project overview</h2>
     * GeothermalOwlGraph : main
     * |- gets Model by CompOfCon
     * |- makes GeothermalObjects
     * .. |- extended by GeothermalSuperObject
     * |- makes GeothermalSuperObjects
     * .. |- extends GeothermalObject
     * |- makes GeothermalEdges
     * |- makes Literature
     * |- makes GraphDisplay and sets Attributes
     * .. |- makes GraphHtml
     * ..... |- uses GraphDisplay Attributes
     * .. |- uses Geothermal[Super]Object/Edges
     *
     * @param args <b>flags:</b><br>
     *             --testowl replaces extended_ontology by testOntology;
     *             --noclustering deactivates clustering of Methods by Superclasses;
     *             --noclusteredges deactivates turning all same edges of cluster-Objects
     *             into one edge for the whole cluster (not working now);
     *             -compOfCon=name requests the map with special obj in focus.
     *             The Graph shows the (restricted) components of connectivity to this object;
     *             -kspSource=name and -kspTarget=name calculates the K shortest paths by Yen-Algorithm;
     *             -k and -kth specifies the K res. the number of the path that should be shown;
     *             -selectNode=name highlights a node and allows to run compOfCon (same node) or ksp (different node) with next klick;
     *             -direction=lr changes flow of graph to Left to Right (instead of default Top to Bottom);
     *             -shapeMethod=shape, -shapeParameter=shape specify the shape of the nodes (like rectangle, oval, ...);
     *             -edgeWidth=int, -fontSize=int specify the width of an edge res. the fontsize in numbers, defaults aare 1 res. 14;
     *             --withVocprez can only be disabled by webserver. if enabled vocprez-endpoint will be queried for alternative obj names
     * @throws IOException ioexception
     */
    public static void main(String[] args) throws IOException {
        GeothermalOwlGraph geothermalOwlGraph = new GeothermalOwlGraph(args);
        geothermalOwlGraph.make(args);
    }

    /**
     * see {@link #make}
     */
    public String make(String[] args, boolean isJUnitTest) throws IOException {
        this.isJUnitTest = isJUnitTest;
        make(args);
        return getMadeHTML();
    }

    /**
     * non-static main
     *
     * @param args see {@link #main(String[])}
     * @return HTML page
     * @throws IOException ioexception
     */
    public void make(String[] args) throws IOException {
        // logger configuration
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.INFO);

        if (config == null) {
            config = new CallConfig(args); // todo change
        }
        graphDisplay = new GraphDisplay(config);

        /*
        // init
        for (String arg : args) {
            if (arg.equals("--help")) {
                System.out.println("Owl2Graph - Geothermal Ontology Graph\n" +
                        "\n" +
                        "displays the ontology.owl as a html graph in out/maps/... and as a image in out/owlgraphs/...\n" +
                        "\n" +
                        "following parameters are possible:\n" +
                        "\n" +
                        "about functionality:\n" +
                        "--testowl replaces extended_ontology by testOntology;\n" +
                        "--noclustering deactivates clustering of Methods by Superclasses;\n" +
                        "--noclusteredges deactivates turning all same edges of cluster-Objects into one edge for the whole cluster (not working now); \n" +
                        "-compOfCon=name requests the map with special obj in focus. The Graph shows the (restricted) components of connectivity to this object;\n" +
                        "--withVocprez can only be disabled by webserver. if enabled vocprez-endpoint will be queried for alternative obj names\n" +
                        "\n" +
                        "about design:\n" +
                        "-kspSource=name and -kspTarget=name calculates the K shortest paths by Yen-Algorithm;\n" +
                        "-k and -kth specifies the K res. the number of the path that should be shown;\n" +
                        "-selectNode=name highlights a node and allows to run compOfCon (same node) or ksp (different node) with next klick;\n" +
                        "-direction=lr changes flow of graph to Left to Right (instead of default Top to Bottom);\n" +
                        "-shapeMethod=shape, -shapeParameter=shape specify the shape of the nodes (like rectangle, oval, ...);\n" +
                        "-edgeWidth=int, -fontSize=int specify the width of an edge res. the fontsize in numbers, defaults aare 1 res. 14;");
                System.exit(0);
            }
            if (arg.equals("--testowl")) {
                isJUnitTest = true; // ??
            } else if (arg.equals("--noclustering")) {
                disableClustering = true;
                disableClusteredges = true;
                graphDisplay.setDisableClusteringTrue();
                graphDisplay.setDisableClusterEdgesTrue();
            } else if (arg.equals("--noclusteredges")) {
                disableClusteredges = true;
                graphDisplay.setDisableClusterEdgesTrue();
            } else if (arg.contains("-compOfCon=")) {
                graphDisplay.setDisableCompOfConFalse();
                config.isCompOfConEnabled() = false; // ??
                config.getCompOfCon() = arg.replace("-compOfCon=", "");
            } else if (arg.contains("-onlyMethodCat=")) {
                selectedMethodCats.add(arg.replace("-onlyMethodCat=", ""));
            } else if (arg.contains("-methodCatSelBy=intersect")) {
                uniteMethodCatSelection = false;
            } else if (arg.contains("-kspSource=")) {
                kspSource = arg.replace("-kspSource=", "");
            } else if (arg.contains("-kspTarget=")) {
                kspTarget = arg.replace("-kspTarget=", "");
            } else if (arg.contains("-kspK=")) {
                kspK = Integer.parseInt(arg.replace("-kspK=", ""));
            } else if (arg.contains("-kspKth=")) {
                if (arg.contains("all")) {
                    kspKth = Integer.MAX_VALUE;
                } else {
                    kspKth = Integer.parseInt(arg.replace("-kspKth=", ""));
                }
            } else if (arg.contains("-selectNode=")) {
                selectedNode = arg.replace("-selectNode=", "");
            } else if (arg.contains("-direction=lr")) {
                graphDisplay.setDirectionLeftRightTrue();
            } else if (arg.contains("-shapeMethod=")) {
                shapeMethod = arg.replace("-shapeMethod=", "");
            } else if (arg.contains("-shapeParameter=")) {
                shapeParameter = arg.replace("-shapeParameter=", "");
            } else if (arg.contains("-edgeWidth=")) {
                graphDisplay.setEdgeWidth(arg.replace("-edgeWidth=", ""));
            } else if (arg.contains("-fontSize=")) {
                graphDisplay.setFontsize(arg.replace("-fontSize=", ""));
            } else if (arg.contains("--withVocprez")) {
                enableVocprez = true;
            }
        }
        */

        if (config.isCompOfConEnabled() && config.getCompOfCon() == null) {
            if (isJUnitTest) {
                config.setCompOfCon(Preferences.COMPOFCON_START_JUNITTEST);
            } else {
                config.setCompOfCon(Preferences.COMPOFCON_START);
            }
        }
        if (config.isCompOfConEnabled())
            initCompOfConModel(config.getCompOfCon());
        else
            initModel();

        // add Parameters and Methods
        // with their corresponding connections
        fillGraph();

        // for some time in future (#96) #99 #100
        if (config.isVocprezEnabled()) {
            LinkedList<AbstractMap.SimpleEntry<String, String>> endpointResults = getEndpointInformationList();
            graphDisplay.setEndpointInformation(true, endpointResults);
        }

        // add focused information
        if (config.getCompOfCon() != null) {
            graphDisplay.setFocusedGeoObj(graphDisplay.getGeoObject(config.getCompOfCon()));
            graphDisplay.setLiteratureOfFousedGeoObj(getLiteratureOfFocusedGeoObj(config.getCompOfCon()));
            LinkedList<String> furtherInfoExclude = new LinkedList<>();
            /* @TODO let user choof whether it should be shown / maybe only not in debug mode
            furtherInfoExclude.add("results");
            furtherInfoExclude.add("resultsFrom");
            furtherInfoExclude.add("requires");
            furtherInfoExclude.add("generates");
            furtherInfoExclude.add("isGeneratedFrom");
            furtherInfoExclude.add("hasMandInput");
            furtherInfoExclude.add("isMandInputOf");
            furtherInfoExclude.add("requiredBy");
            */
            furtherInfoExclude.add("pathsDepth");
            furtherInfoExclude.add("fromLiterature");
            furtherInfoExclude.add("hasImportance");
            LinkedList<AbstractMap.SimpleEntry<String, String>> furtherInformationOfFocusedObj = getFurtherInformationOfFocusedObj(
                    config.getCompOfCon(),
                    null,
                    furtherInfoExclude);
            graphDisplay.setFurtherInformationOfFocusedObj(furtherInformationOfFocusedObj);
        }

        // ksp / Yen
        if ((config.getKspSource() != null && config.getKspTarget() != null) || Preferences.ENABLE_KSP) {
            //if (graphDisplay.containsGeoObjOrIsInSuperGeoObject(kspSource)
            //        && graphDisplay.containsGeoObjOrIsInSuperGeoObject(kspTarget)) {
            ksp(args);
            //} else {
            //    String message = "k-shortest-paths can not be executed with this parameters. Maybe the object(s) do(es) not exist in this ontology.";
            //    logger.error(message);
            //    return graphDisplay.createDisplayMessage(message);
            //}
        }
        graphDisplay.setKspPaths(kspPaths);

        // method categories
        logger.debug("ADD METHOD CATS");

        addMethodCats();
        graphDisplay.setMethodCategories(config.getSelectedMethodCats(), allMethodCats, allMethodCatsOccurrences);
        logger.debug(allMethodCats.keySet() + "\n" + allMethodCats.values() + "\n");

        // prepare to and display the filled graph
        graphDisplay.displayGraph();
    }

    public String getMadeHTML() {
        return graphDisplay.getFullHTMLString();
    }

    public String getGraphvizString() {
        return graphDisplay.getFullGraphVizString();
    }

    /**
     * Initialize test ontology if {@link #config#isOwlTestowl()}
     */
    private void initCompOfConModel(String compOfConName) throws IOException {

        CompOfCon compOfCon = new CompOfCon(
                (config.isOwlTestowl())
                        ? Preferences.TEST_OWL_SOURCE
                        : Preferences.OWL_SOURCE
        );

        for (String prefix : Preferences.ALL_PREFIXES) {
            if (compOfCon.getModel().getResource(prefix + compOfConName) != null) {
                compOfCon.findComponentOfConnectivityOf( prefix + compOfConName);
            }
        }
        //compOfCon.findComponentOfConnectivityOf(getUriOfLocalname(compOfCon.getModel(), compOfConName));

        model = compOfCon.getModel();
        if (!config.isCallerWebserver()) {
            compOfCon.writeModelToDisk(config.isOwlTestowl());
        }

        owlPrefix = Preferences.PREFIX;

        logger.info(model.isEmpty() ? "read empty owl2graph compOfCon model" : "read not empty owl2graph.test model");

    }


    /**
     * Initialize by "extended-ontology.owl"
     */
    private void initModel() {
        model = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);//OWL_DL_MEM_RDFS_INF); // OWL_DL_MEM_RULE_INF does not react
        //model.getDocumentManager().;
        model.read(
                (config.isOwlTestowl())
                        ? Preferences.TEST_OWL_SOURCE
                        : Preferences.OWL_SOURCE
        );
        owlPrefix = Preferences.PREFIX;

        logger.info(model.isEmpty() ? "read empty model" : "read not empty model");
    }


    /**
     * executes Yen's k-shortest-paths algorithm
     */
    private void ksp(String[] args) {
        GeoKSP geoKSP = new GeoKSP(new LinkedList<>(graphDisplay.getGeoObjects().values()));

        String sourceNode;
        String targetNode;
        if (config.getKspSource() != null && config.getKspTarget() != null) {
            sourceNode = config.getKspSource();
            targetNode = config.getKspTarget();
        } else {
            sourceNode = config.isOwlTestowl() ? Preferences.KSP_SOURCENODE_TEST : Preferences.KSP_SOURCENODE;
            targetNode = config.isOwlTestowl() ? Preferences.KSP_TARGETNODE_TEST : Preferences.KSP_TARGETNODE;
        }
        logger.debug("Trying KSP with args: " + makeArgsToString(args));

        // mediums
        // decides whether source or target is a medium
        // by checking, if geoObjects contain this
        boolean sourceIsMedium = true;
        boolean targetIsMedium = true;
        for (GeothermalObject geoObj : graphDisplay.getGeoObjects().values()) {
            if (sourceIsMedium && geoObj.getLocalName().equals(sourceNode)) {
                sourceIsMedium = false;
            }
            if (targetIsMedium && geoObj.getLocalName().equals(targetNode)) {
                targetIsMedium = false;
            }
        }
        // add medium and edges in correct direction
        if (sourceIsMedium) {
            geoKSP.addMedium(sourceNode, getMediumsTriples(sourceNode, true), true);
        }
        if (targetIsMedium) {
            geoKSP.addMedium(targetNode, getMediumsTriples(targetNode, false), false);
        }

        // ksp
        int k, kth;
        k = config.getKspK() > 0 ? config.getKspK() : Preferences.KSP_k;
        kth = config.getKspKth() >= 0 ? config.getKspKth() : Preferences.KSP_kth;
        logger.trace("(k,kth) : " + k + " " + kth);

        if (config.isCompOfConEnabled()) { // ??
            logger.error("KSP will not be executed");

        } else {

            geoKSP.runKsp(
                    sourceNode,
                    targetNode,
                    k);
            logger.debug("K SHORTEST PATHS:\n");
            for (Path path : geoKSP.getKspPaths()) {
                logger.debug(path);
            }

            // ! changes the graph
            geoKSP.applyOnOwlGraph(graphDisplay, kth);
        }
        kspPaths = geoKSP.getKspPaths();
    }

    /**
     * get all Parameter-hasMedium-Medium Triples that belong to a specific medium
     *
     * @param medium         list this medium's Parateter-hasMedium-Medium Triples
     * @param sourceIsMedium if the medium is ksp-source. if false, it is ksp-target
     * @return
     */
    private List<Triple> getMediumsTriples(String medium, boolean sourceIsMedium) {
        LinkedList<Triple> list = new LinkedList<>();

        // list all Medium-Statements
        StmtIterator propertyStmts;
        /*
        // aktuell noch keine IS_MEDIUM_OF
        if (sourceIsMedium) {
            propertyStmts = model.listStatements( model.getIndividual(owlPrefix + medium), model.getProperty(owlPrefix + Properties.IS_MEDIUM_OF_IRI), (RDFNode) null);
        } else {
            propertyStmts = model.listStatements(null, model.getProperty(owlPrefix + Properties.HAS_MEDIUM_IRI), model.getIndividual(owlPrefix + medium));
        }*/
        propertyStmts = model.listStatements(null, model.getProperty(owlPrefix + Preferences.HAS_MEDIUM_IRI), model.getIndividual(owlPrefix + medium));


        // add all statements as Triples
        while (propertyStmts.hasNext()) {
            Statement stmt = propertyStmts.next();
            if (sourceIsMedium) {
                list.add(new Triple(stmt.getObject().asNode(), stmt.getPredicate().asNode(), stmt.getSubject().asNode()));
            } else {
                list.add(stmt.asTriple());
            }
        }

        return list;
    }

    /**
     * offers a string representation of an String-Array.
     * Here for presentation of the program args
     *
     * @param args string array to stringrepresent
     * @return an concatination of the args space-seperated in [, ]
     */
    private String makeArgsToString(String[] args) {
        String str = "[";
        for (String arg : args) {
            str += arg + " ";
        }
        str += "]";

        return str;
    }

    private void addInverses(Property origin, Property inverse) {

    }

    /**
     * fills the Graph {@link #graphDisplay} with each Method as (circled) Node
     * and each Parameter as Edge-diamondShapedNode-Edge
     * with corresponding connections to the (Method)
     * <p>
     * (Method) ----→ diamondParam ----→ (Method) ---→ diamondParam ...
     * ___ --resultsFrom--   -requiredBy--→
     * _ 1stM --→ 1stP --→ 2ndP --→ 2ndM
     * <p>
     * Param---→ Method ---Param---→
     * _____ requires results
     */
    private void fillGraph() {
        nameProperty = model.getProperty(owlPrefix + Preferences.NAME_IRI);

        // in the Graph: Parameter -------> Method -------> Parameter
        //
        // syntactical:  Parameter <------- Method -------> Parameter
        //                    toMethodProperty  fromMethodProperty
        //  at 20-oct-9:        "requires"          "results"
        //  as property
        //  in the rdf-db
        toMethodProperty = model.getProperty(owlPrefix + Preferences.TO_METHOD_IRI); //method
        toMethodProperty_MO = new Property[]{
                model.getProperty(owlPrefix + Preferences.TO_METHOD_IRI_MO[0]),
                model.getProperty(owlPrefix + Preferences.TO_METHOD_IRI_MO[1])
        };
        toMethodProperty_MO_inverse = new Property[]{
                model.getProperty(owlPrefix + Preferences.TO_METHOD_IRI_MO_INVERSE[0]),
                model.getProperty(owlPrefix + Preferences.TO_METHOD_IRI_MO_INVERSE[1])
        };
        fromMethodProperty = model.getProperty(owlPrefix + Preferences.FROM_METHOD_IRI); // method
        fromMethodProperties_ABCO = new Property[]{
                model.getProperty(owlPrefix + Preferences.FROM_METHOD_IRI_ABCO[0]),
                model.getProperty(owlPrefix + Preferences.FROM_METHOD_IRI_ABCO[1]),
                model.getProperty(owlPrefix + Preferences.FROM_METHOD_IRI_ABCO[2]),
                model.getProperty(owlPrefix + Preferences.FROM_METHOD_IRI_ABCO[3])
        };
        fromMethodProperties_ABCO_inverse = new Property[]{
                model.getProperty(owlPrefix + Preferences.FROM_METHOD_IRI_ABCO_INVERSE[0]),
                model.getProperty(owlPrefix + Preferences.FROM_METHOD_IRI_ABCO_INVERSE[1]),
                model.getProperty(owlPrefix + Preferences.FROM_METHOD_IRI_ABCO_INVERSE[2]),
                model.getProperty(owlPrefix + Preferences.FROM_METHOD_IRI_ABCO_INVERSE[3])
        };
        importanceProperty = model.getProperty(owlPrefix + Preferences.IMPORTANCE_IRI);

        // todo for issue #55
        // weightProperty = model.getProperty(owlPrefix + Properties.WEIGHT_PROPERTY);

        addStatementBunch(toMethodProperty_MO[0], this::useStatement);
        addInverseStatementBunch(toMethodProperty_MO_inverse[0], toMethodProperty_MO[0], this::useStatement);
        addStatementBunch(toMethodProperty_MO[1], this::useStatement);
        addInverseStatementBunch(toMethodProperty_MO_inverse[1], toMethodProperty_MO[0], this::useStatement);
        //addStatements(toMethodProperty, this::useStatement);
        //addStatements(fromMethodProperty, this::useStatement);
        addStatementBunch(fromMethodProperties_ABCO[0], this::useStatement);
        addInverseStatementBunch(fromMethodProperties_ABCO_inverse[0], fromMethodProperties_ABCO[0], this::useStatement);
        addStatementBunch(fromMethodProperties_ABCO[1], this::useStatement);
        addInverseStatementBunch(fromMethodProperties_ABCO_inverse[1], fromMethodProperties_ABCO[1], this::useStatement);
        addStatementBunch(fromMethodProperties_ABCO[2], this::useStatement);
        addInverseStatementBunch(fromMethodProperties_ABCO_inverse[2], fromMethodProperties_ABCO[2], this::useStatement);
        addStatementBunch(fromMethodProperties_ABCO[3], this::useStatement);
        addInverseStatementBunch(fromMethodProperties_ABCO_inverse[3], fromMethodProperties_ABCO[3], this::useStatement);
    }


    /**
     * earlier parted in addMethodToParameter and addParameterToMethod
     * <p/>
     * if property is toMethodProperty:
     * <p/>
     * a.k.a.
     * Method "results" in Parameter
     * (Method) ---------→ Parameter
     * <p/>
     * and
     * a.k.a.
     * Parameter "requiredBy"(?) Node
     * Parameter ------------→ (Node)
     * <p/>
     *
     * @param property toMethodProperty or fromMethodProperty
     */
    private void addStatementBunch(Property property, Predicate<Statement> statementPredicate) {
        // add first node and edges
        // a.k.a. (Method) --> <Parameter>
        Statement statement;

        StmtIterator stmtIterator = selectPropertyTuples(property);

        while (stmtIterator.hasNext()) {
            statement = stmtIterator.next();
            Resource subjectResource = statement.getSubject();
            Resource objectResource = statement.getObject().asResource();

            addSingleStatement(property, statementPredicate, statement, subjectResource, objectResource);
        }
    }


    /**
     * like {@link #addStatementBunch(Property, Predicate)}
     * but inverse
     *
     * @param inverseProperty
     * @param statementPredicate
     */
    private void addInverseStatementBunch(Property inverseProperty, Property rightWayRoundProperty, Predicate<Statement> statementPredicate) {
        // add first node and edges
        // a.k.a. (Method) --> <Parameter>
        Statement statement;

        StmtIterator stmtIterator = selectPropertyTuples(inverseProperty);

        while (stmtIterator.hasNext()) {
            statement = stmtIterator.next();
            Resource objectResource = statement.getSubject();
            Resource subjectResource = statement.getObject().asResource();

            Statement rightWayRoundStatement = new StatementImpl(subjectResource, rightWayRoundProperty, objectResource);
            addSingleStatement(rightWayRoundProperty, statementPredicate, rightWayRoundStatement, subjectResource, objectResource);
        }
    }

    /**
     * Add a statement to model
     *
     * @param property
     * @param statementPredicate
     * @param statement
     * @param subjectResource
     * @param objectResource
     */
    private void addSingleStatement(Property property, Predicate<Statement> statementPredicate, Statement statement, Resource subjectResource, Resource objectResource) {
        // add first node
        if (statementPredicate.test(statement)
                && (!config.isCompOfConEnabled() // ??
                || (toGetIncluded(subjectResource) // requires CompOfCon
                && toGetIncluded(objectResource)))
        ) {
            GeothermalObject methodObject;
            if ((methodObject = graphDisplay.getGeoObject(getLocalName(subjectResource))) == null) {
                methodObject = new GeothermalObject(
                        subjectResource.getURI().replace(owlPrefix, ""),
                        getAvailableResourceName(subjectResource, nameProperty),
                        GeothermalThing.METHOD // todo right?
                );
                methodObject.setImportance(getImportance(subjectResource));
                methodObject.setWeight(getWeight(subjectResource));
                methodObject.setThisQueryString(config.getThisQueryString());
                methodObject.setShapeMethodAndParameter(config.getShapeMethod(), config.getShapeParameter());

                if (config.getClustering() != CallConfig.Clustering.DISABLE)
                    addToSuperGeoObject(subjectResource, methodObject);
                logger.trace("GeoObjekt neu erstellt    - addStatements - Method - " + methodObject.getName());
            } else logger.trace("GeoObjekt wiederverwendet - addStatements - Method - " + methodObject.getName());


            // add parameter
            GeothermalObject parameterObject;
            if ((parameterObject = graphDisplay.getGeoObject(getLocalName(objectResource))) == null) {
                parameterObject = new GeothermalObject(
                        getLocalName(objectResource),
                        getAvailableResourceName(objectResource, nameProperty),
                        (property == fromMethodProperties_ABCO[3])
                                ? GeothermalThing.OBJECT
                                : GeothermalThing.PARAMETER
                );
                // ? maybe not because it's parameter (res issue #55)
                // edgeObject.setWeight(getWeight(firstEdgeResource));

                parameterObject.setImportance(getImportance(objectResource));
                parameterObject.setThisQueryString(config.getThisQueryString());
                parameterObject.setShapeMethodAndParameter(config.getShapeMethod(), config.getShapeParameter());
                parameterObject.addMediums(getMediumsOfResource(objectResource));

                logger.trace("GeoObjekt neu erstellt    - addStatements - Param  - " + parameterObject.getName());
            } else {
                logger.trace("GeoObjekt wiederverwendet - addStatements - Param  - " + parameterObject.getName());
            }

            /*
             * here main differences
             * of execution with different properties
             */
            if (/*property == toMethodProperty*/Arrays.stream(toMethodProperty_MO).collect(Collectors.toList()).contains(property)) {
                GeothermalEdge edge = new GeothermalEdge(parameterObject, methodObject, statement);
                methodObject.addEdge(edge, null);
                parameterObject.addEdge(null, edge);
            } else if (/*property == fromMethodProperty || */Arrays.stream(fromMethodProperties_ABCO).collect(Collectors.toList()).contains(property)) {
                GeothermalEdge edge = new GeothermalEdge(methodObject, parameterObject, statement);
                methodObject.addEdge(null, edge); // opposite as above
                parameterObject.addEdge(edge, null);
            } else {
                System.err.println("Cannot match properties");
            }

            graphDisplay.putGeoObject(methodObject); //method
            graphDisplay.putGeoObject(parameterObject); // param
        }
    }

    private LinkedList<String> getMediumsOfResource(Resource resource) {
        LinkedList<String> list = new LinkedList<>();

        StmtIterator propertyStmts = model.listStatements(resource, model.getProperty(owlPrefix + Preferences.HAS_MEDIUM_IRI), (RDFNode) null);

        while (propertyStmts.hasNext()) {
            Statement stmt = propertyStmts.next();
            list.add(stmt.getObject().asResource().getLocalName());
        }

        return list;
    }

    /**
     * defines optional restrictions on statements
     * <p/>
     * used as predicate
     *
     * @param stmt the statement to test
     * @return if stmt should be used
     */
    private boolean useStatement(Statement stmt) {
        if (stmt == null) {
            return false;
        }

        Property methodCatProperty = model.getProperty(Preferences.PREFIX + Preferences.METHOD_CAT_IRI);
        StmtIterator propertyStmts = model.listStatements(stmt.getSubject(), methodCatProperty, (RDFNode) null);
        LinkedList<String> propertyObjStrs = new LinkedList<>();
        while (propertyStmts.hasNext()) {
            Statement propertyStmt = propertyStmts.next();
            String propertyObjStr = propertyStmt.getObject().asResource().getLocalName();
            propertyObjStrs.add(propertyObjStr);
        }
        boolean selectedMethodCatContainsAProp;
        if (config.getMultipleMethCatOption() == CallConfig.MultipleMethCatOptions.UNITE) {
            selectedMethodCatContainsAProp = false;
            if (config.getSelectedMethodCats() != null) {
                for (String str : propertyObjStrs) {
                    if (config.getSelectedMethodCats().contains(str)) {
                        selectedMethodCatContainsAProp = true;
                        break;
                    }
                }
            }
        } else {
            selectedMethodCatContainsAProp = true;
            if (config.getSelectedMethodCats() != null) {
                for (String str : config.getSelectedMethodCats()) {
                    if (!propertyObjStrs.contains(str)) {
                        selectedMethodCatContainsAProp = false;
                        break;
                    }
                }
            }

        }

        //
        if (stmt.getSubject().hasProperty(methodCatProperty) && config.getSelectedMethodCats() != null && !config.getSelectedMethodCats().isEmpty()
                // the method in statement has a category that was not selected but some were selected
                && (!selectedMethodCatContainsAProp)) {
            return false;
        }
        if (!stmt.getSubject().hasProperty(methodCatProperty) && config.getSelectedMethodCats() != null && !config.getSelectedMethodCats().isEmpty()
                && !config.getSelectedMethodCats().contains("unclassified")) {
            return false;
        }
        // Elements cannot be unclassified and have a class
        if (!(config.getMultipleMethCatOption() == CallConfig.MultipleMethCatOptions.UNITE) && config.getSelectedMethodCats() != null && config.getSelectedMethodCats().size() > 1 && config.getSelectedMethodCats().contains("unclassified")) {
            return false;
        }

        return true;
    }


    /**
     * Select Statements with the specific property-Property by SimpleSelector.
     * <p>
     * (earlier "queryPropertyTuples" by SPARQL Query. replaced and removed with Issue #13.)
     *
     * @param property Property the tuples are describing
     * @return an Iterator over the selected Statements
     */
    public StmtIterator selectPropertyTuples(Property property) {
        return model.listStatements(new SimpleSelector(null, property, (RDFNode) null));
    }


    /**
     * get nameProperty-Name of the resource, if available otherwise use local name
     *
     * @param resource     get name of this
     * @param nameProperty nameProperty
     * @return the name
     */
    public String getAvailableResourceName(Resource resource, Property nameProperty) {
        // prevent no-name-exceptions
        Statement resourceNameStatement = resource.getProperty(nameProperty);
        String firstNodeResourceName;
        if (resourceNameStatement != null)
            firstNodeResourceName = resourceNameStatement.getObject().toString();
        else {
            logger.trace("No Name Property");
            firstNodeResourceName = getLocalName(resource);
        }
        return firstNodeResourceName;
    }

    /**
     * get the best string representation of the node
     */
    public String getAvailableNodeName(RDFNode node, Property nameProperty) {
        if (node.isResource()) {
            String str = getAvailableResourceName(node.asResource(), nameProperty);
            return StringUtils.join(
                    StringUtils.splitByCharacterTypeCamelCase(str),
                    ' '
            );
        } else if (node.isLiteral()) {
            return node.asLiteral().toString();
        }
        return node.toString();
    }

    /**
     * does the individual have a Depth-Property?
     * till now 2020-08-30 same as hasDepthProperty in {@link CompOfCon}
     *
     * @param resource individual
     */
    private boolean toGetIncluded(Resource resource) {
        return resource.hasProperty(
                model.getDatatypeProperty(Preferences.PREFIX + Preferences.PATHS_DEPTH_IRI)
        );
    }

    /**
     * add Method to super method class
     */
    private void addToSuperGeoObject(Resource resource, GeothermalObject geoObj) {

        // iterate over Classes of the geo Object
        StmtIterator stmtIterator = model.listStatements(new SimpleSelector(resource, RDF.type, (RDFNode) null));
        String superClass = ""; // accumulates Name of Class
        while (stmtIterator.hasNext()) {

            // add the super class name
            Statement stmt = stmtIterator.next(); // just for this 2 lines
            RDFNode ontClassNode = stmt.getObject();
            String superClassPart = getLocalName(ontClassNode.asResource());
            logger.debug(resource + " ### " + superClassPart);
            if (!superClassPart.contains("NamedIndividual")) {
                superClass += superClassPart;
                logger.debug("ADD: " + resource + " ### " + superClassPart);

                // if not existing: create new class
                // otherwise add
                GeothermalSuperObject superGeoObject;
                if ((superGeoObject = graphDisplay.getSuperGeoObject(superClass)) == null) {
                    // create new class
                    GeothermalSuperObject geothermalSuperObject = new GeothermalSuperObject(
                            superClass,
                            getAvailableResourceName(ontClassNode.asResource(), nameProperty),
                            geoObj.getGeothermalThing(),
                            config.getClustering() == CallConfig.Clustering.WITHOUT_EDGES
                    );
                    geothermalSuperObject.addGeothermalObject(geoObj);
                    graphDisplay.getSuperGeoObjects().add(geothermalSuperObject);
                    graphDisplay.removeGeoObj(geoObj);
                } else {
                    // add to existing
                    superGeoObject.addGeothermalObject(geoObj);
                    graphDisplay.removeGeoObj(geoObj);
                }

                logger.trace("RDFNode: " + getLocalName(ontClassNode.asResource()));
            }
        }
        logger.trace("superClass: " + superClass);


    }

    private Preferences.Importance getImportance(Resource resource) {
        /*
        // start node in MEDIUM
        if (Properties.ENABLE_KSP) {
            if (config.isOwlTestowl() ? resource.getLocalName().equals(Properties.KSP_SOURCENODE_TEST)
                    : resource.getLocalName().equals(Properties.KSP_SOURCENODE)) {
                return Properties.Importance.MEDIUM;
            } else if (config.isOwlTestowl() ? resource.getLocalName().equals(Properties.KSP_TARGETNODE_TEST)
            :resource.getLocalName().equals(Properties.KSP_TARGETNODE)) {
                return Properties.Importance.HIGH;
            }
        }
        */

        // start node and target node in HIGH
        if (
            // desktop
                config.getSelectedNode() != null && resource.getLocalName().equals(config.getSelectedNode())) {
            return Preferences.Importance.MEDIUM;
        } else if ((Preferences.ENABLE_KSP
                && (config.isOwlTestowl()
                ? (getLocalName(resource).equals(Preferences.KSP_SOURCENODE_TEST)
                || getLocalName(resource).equals(Preferences.KSP_TARGETNODE_TEST))
                : (getLocalName(resource).equals(Preferences.KSP_SOURCENODE)
                || getLocalName(resource).equals(Preferences.KSP_TARGETNODE))))

                // especially for webserver (but also desktop)
                || (config.getKspSource() != null && config.getKspTarget() != null
                && (getLocalName(resource).equals(config.getKspSource())
                || getLocalName(resource).equals(config.getKspTarget())))) {
            return Preferences.Importance.HIGH;
        } else if (resource.hasProperty(importanceProperty)) {
            String str = resource.getProperty(importanceProperty).getObject().toString();
            for (Preferences.Importance importance : Preferences.Importance.values()) {
                if (importance.toString().equals(str)) return importance;
            }
        }
        return Preferences.Importance.NORMAL;
    }

    /**
     * queries the Literature of an Object
     *
     * @return List of Lit
     */
    public List<Literature> getLiteratureOfFocusedGeoObj(String localname) {
        OntResource resource = null;
        for (String prefix : Preferences.ALL_PREFIXES) {
            if (model.getResource(prefix + localname) != null) {
                resource = model.getOntResource(prefix + localname);
            }
        }


        List<Literature> literatureList = new LinkedList<>();

        StmtIterator stmtIterator = model.listStatements(resource, model.getProperty(owlPrefix + Preferences.FROM_LITERATURE_IRI), (RDFNode) null);

        /*
         * add each Literature with it's properties
         */
        while (stmtIterator.hasNext()) {
            Statement stmt = stmtIterator.next();
            Resource stmtResource = stmt.getObject().asResource();

            // Properties
            OntProperty fauthorProperty = model.getOntProperty(owlPrefix + Preferences.LIT_FAUTHOR_IRI);
            OntProperty authorsProperty = model.getOntProperty(owlPrefix + Preferences.LIT_AUTHORS_IRI);
            OntProperty yearProperty = model.getOntProperty(owlPrefix + Preferences.LIT_YEAR_IRI);
            OntProperty titleProperty = model.getOntProperty(owlPrefix + Preferences.LIT_TITLE_IRI);

            // add the values to this
            String litName = getStmtObjStr(stmtResource, nameProperty);
            String litTitle = getStmtObjStr(stmtResource, titleProperty);// stmtResource.getProperty(titleProperty).getObject().toString();
            String litFauthors = getStmtObjStr(stmtResource, fauthorProperty);//stmtResource.getProperty(fauthorProperty).getObject().toString();
            String litAuthors = getStmtObjStr(stmtResource, authorsProperty);//stmtResource.getProperty(authorsProperty).getObject().toString();
            Statement yearStmtProperty = stmtResource.getProperty(yearProperty);
            String litYear = null;
            if (yearStmtProperty != null) {
                litYear = yearStmtProperty.getObject().toString().split("^^")[0];
            }
            Literature literature = new Literature(litTitle != null ? litTitle : "unknownlit");

            // get BibTex

            StmtIterator litPropIterator = stmtResource.listProperties();
            StringBuilder bibTexCodeBuilder = new StringBuilder();
            RDFNode object = stmtResource.getProperty(
                    model.getObjectProperty(
                            owlPrefix + Preferences.LIT_BIBTEXTYPE_IRI
                    )
            ).getObject();
            if (object.isResource()) {
                bibTexCodeBuilder.append("@" + object.asResource().getLocalName());
            } else {
                bibTexCodeBuilder.append("@" + object.asLiteral());
            }

            bibTexCodeBuilder.append(stmtResource.getLocalName() + ", \n");

            if (litTitle != null) bibTexCodeBuilder.append("\ttitle={" + litTitle + "},\n");
            if (litAuthors != null) bibTexCodeBuilder.append("\tauthor={" + litAuthors + "},\n");
            if (litYear != null) bibTexCodeBuilder.append("\tyear={" + litYear + "},\n");
            for (StmtIterator litProp = litPropIterator; litPropIterator.hasNext(); ) {
                Statement litPropStmt = litPropIterator.next();
                Property litPropProp = litPropStmt.getPredicate();
                if (litPropProp != titleProperty && litPropProp != authorsProperty && litPropProp != yearProperty) {
                    bibTexCodeBuilder.append("\t" + litPropProp.getLocalName() + "={" + litPropStmt.getObject().toString() + "},\n");
                }
            }
            bibTexCodeBuilder.append("}\n");

            // hardcoded properties
            if (litFauthors != null) literature.setFauthor(litFauthors);
            if (litAuthors != null) literature.setAuthors(litAuthors);
            if (litYear != null) {
                literature.setYear(Integer.parseInt(litYear.replace("^^http://www.w3.org/2001/XMLSchema#integer", "").replace("^^http://www.w3.org/2001/XMLSchema#int", "")));
            }

            literature.setBibTex(bibTexCodeBuilder.toString());

            // add this literature
            literatureList.add(literature);
        }
        return literatureList;
    }

    /**
     * return stmtResource.getProperty(property).getObject().toString() safely
     */
    private String getStmtObjStr(Resource stmtResource, Property property) {
        Statement stmtProperty = stmtResource.getProperty(property);
        if (stmtProperty != null) {
            return stmtProperty.getObject().toString();
        } else {
            return null;
        }
    }

    /**
     * queries the method categories and adds to {@link #allMethodCats}
     */
    private void addMethodCats() {
        // HashSet to not have duplicate Resources
        StmtIterator stmtIterator = model.listStatements(null, model.getProperty(owlPrefix + Preferences.METHOD_CAT_IRI), (RDFNode) null);

        LinkedList<String> methodsWithCat = new LinkedList<>();

        // add each category to 1 pool
        while (stmtIterator.hasNext()) {
            Statement stmt = stmtIterator.next();

            // add real
            String catStr = stmt.getObject().asResource().getLocalName();

            String catGroupStr = getAvailableResourceName(stmt.getObject().asResource().getProperty(RDF.type).getObject().asResource(), nameProperty);

            if (allMethodCats.containsKey(catGroupStr)) {
                if (!allMethodCats.get(catGroupStr).contains(catStr))
                    allMethodCats.get(catGroupStr).add(catStr);
            } else {
                LinkedList<String> list = new LinkedList<>();
                list.add(catStr);
                allMethodCats.put(catGroupStr, list);
            }

            String methodStr = stmt.getSubject().asResource().getLocalName();
            if (graphDisplay.containsGeoObjOrIsInSuperGeoObject(methodStr)) {
                if (allMethodCatsOccurrences.containsKey(catStr)) {
                    allMethodCatsOccurrences.put(catStr, allMethodCatsOccurrences.get(catStr) + 1);
                } else {
                    allMethodCatsOccurrences.put(catStr, 1);
                }
            }

            methodsWithCat.add(methodStr);
        }
        int unclassifiedNumber = 0;
        for (GeothermalObject geothermalObject : graphDisplay.getGeoObjects().values()) {
            if (geothermalObject.getGeothermalThing() == GeothermalThing.METHOD && !methodsWithCat.contains(geothermalObject.getLocalName())) {
                unclassifiedNumber++;
            }
        }
        allMethodCatsOccurrences.put("unclassified", unclassifiedNumber);
    }

    /**
     * further information about an focused object
     * <p>
     * SimpleEntry because it is sorted by its Keys
     */
    private LinkedList<AbstractMap.SimpleEntry<String, String>> getFurtherInformationOfFocusedObj(String localname,
                                                                                                  LinkedList<String> onlyIncludeProps,
                                                                                                  LinkedList<String> excludeProps) {
        OntResource resource = null;
        for (String prefix : Preferences.ALL_PREFIXES) {
            if (model.getResource(prefix + localname) != null) {
                resource = model.getOntResource(prefix + localname);
            }
        }



        LinkedList<AbstractMap.SimpleEntry<String, String>> furtherInformation = new LinkedList<>();

        StmtIterator stmtIterator = model.listStatements(resource, null, (RDFNode) null);

        while (stmtIterator.hasNext()) {
            Statement stmt = stmtIterator.next();

            String stmtPredicate = stmt.getPredicate().getLocalName();
            RDFNode stmtObject = stmt.getObject();
            String objName = getAvailableNodeName(stmtObject, nameProperty);


            // make predicate from camelCase to human readable string
            // "hasProperty" -> "has Property"
            String predicateStr = StringUtils.join(
                    StringUtils.splitByCharacterTypeCamelCase(stmtPredicate),
                    ' '
            );

            // collect specific statements
            if ((onlyIncludeProps == null || onlyIncludeProps.contains(stmtPredicate))
                    && (excludeProps == null || !excludeProps.contains(stmtPredicate))
                    && !objName.contains("Named Individual")) {
                furtherInformation.add(new AbstractMap.SimpleEntry<>(predicateStr, objName));
            }

            // sort by key
            furtherInformation.sort(new Comparator<AbstractMap.SimpleEntry<String, String>>() {
                @Override
                public int compare(AbstractMap.SimpleEntry<String, String> stringStringSimpleEntry, AbstractMap.SimpleEntry<String, String> t1) {
                    return stringStringSimpleEntry.getKey().compareTo(t1.getKey());
                }
            });
        }

        return furtherInformation;
    }

    /**
     * {@link #getEndpointInformation()} als LinkedList
     */
    private LinkedList<AbstractMap.SimpleEntry<String, String>> getEndpointInformationList() {
        // init
        ResultSet results = getEndpointInformation();
        LinkedList<AbstractMap.SimpleEntry<String, String>> resultList = new LinkedList<>();

        // iterate
        if (results != null) {
            while (results.hasNext()) {
                QuerySolution querySolution = results.next();
                String foreignInstance = querySolution.get("ForeignInstance").toString();
                String foreignName = querySolution.get("ForeignInstance").toString();
                resultList.add(new AbstractMap.SimpleEntry<>(foreignInstance, foreignName));
            }
        }

        // finalize
        return resultList;
    }

    /**
     * "SPARQL Endpoints sind WebServices, die Anfagen im http: Protokol entgegen nehmen,
     * auswerten und RDFXML antworten geben. Das heißt im Klartext, man kann mit dem
     * SERVICE Keyword in einer SPARQL Anfrage zusätzlich weitere Ontologie hinter diesen
     * Servicen anfragen. Das würde die Möglichkeit geben, elegant auf andere Ontologie zu
     * zugreifen und deren Daten zu nutzen und alles was sie damit verbinden. Soweit ich das
     * sehe sind aber alle Ontologie in dem http://cgi.vocabs.ga.gov.au/endpoint Endpoint.
     * <p>
     * Dann könnten wir (wir im Sinne von Simon) den Service anfagen, ob er etwas kennt, was
     * den selben Namen hat und man bekommt dann alle informationen die er dazu kennt."
     * https://pad.gwdg.de/M-Xpr8cxTquTyirlUrVwdQ?both#SPARQL-SERVICE-amp-Endpoints
     */
    private ResultSet getEndpointInformation() {

        // Endpoint Query
        //
        // Quelle: Geothermal Database Dokumentation > Programmiertechnische Umsetzung
        // > Einbindung anderer Ontologien > SPARQL SERVICE & Endpoints
        // "Mit der Anfrage [..] sollte man also alles bekommen,
        // was sich mit seinen Kenntnissen zu den Namen matched."
        // https://pad.gwdg.de/M-Xpr8cxTquTyirlUrVwdQ?both#SPARQL-SERVICE-amp-Endpoints

        /*
                // original query
                String queryStr = "prefix : <http://www.semwebtech.org/geothermie-ontology#>\n" +
                "select ?ForeignName ?ForeignInstance ?OwnInstance\n" +
                "where { \n" +
                "?OwnInstance :name ?OwnName .\n" +
                "        service <http://cgi.vocabs.ga.gov.au/endpoint>\n" +
                "          { select distinct ?ForeignInstance ?ForeignName\n" +
                "            where\n" +
                "            { ?ForeignInstance <http://www.w3.org/2004/02/skos/core#prefLabel> ?fname .  \n" +
                "              Filter(Lang(?fname) = \"en\") .\n" +
                "              BIND(STR(?fname) as ?ForeignName) \n" +
                "          } }\n" +
                "Filter(lcase(?OwnName) = lcase(?ForeignName))\n" +
                "}";

         */

        // LOCAL SELECT
        String ownNamesGeoObjList = getGraphDisplay().getGeoObjectsSparqlFormattedList();
        logger.debug("OwnNames-Geoobj-List: " + ownNamesGeoObjList);
        String localQuery = "prefix : <http://www.semwebtech.org/geothermie-ontology#>\n" +
                "select \n" +
                "    distinct ?OwnName ?OwnInstance\n" +
                "where { \n" +
                "    ?OwnInstance :name ?OwnName .\n" +
                "}\n";


        logger.debug("initialize loocal query execution");
        QueryExecution localQueryExecution = QueryExecutionFactory.create(localQuery, model);
        logger.debug("loocal query initialized");

        logger.debug("starting loocal selection execution");
        ResultSet localResults = localQueryExecution.execSelect();
        logger.debug("loocal selection executed");

        logger.debug("SPARQL-Result-Head: " + localResults.getResultVars());
        // logger.debug("SPARQL-Result-RowNo: " + results.getRowNumber());

        // make to sparql formatted list `elt_1, elt_2, elt_3, ... elt_n`
        StringBuilder str = new StringBuilder();
        while (localResults.hasNext()) {
            QuerySolution querySolution = localResults.next();
            str.append("\"" + querySolution.getLiteral("OwnName").toString()
                    .replace(",", "")
                    .replace(":", "") + "\"");
            if (localResults.hasNext()) {
                str.append(", ");
            }
        }
        // str.append(", \"russian code\", \"agate\", \"aggregate\"");
        String ownNamesList = str.toString().toLowerCase()
                .replace("(", "")
                .replace(")", "")
                .replace("\\\"", "")
                .replace(".", "")
                .replace("ä", "")//"%C3%A4")
                .replace("ö", "")//""%C3%B6")
                .replace("ü", "");//""%C3%BC");
        logger.debug("OwnNames-Ontology-List: " + ownNamesGeoObjList);


        // SERVER SELECT
        String serviceTestQuery = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" +
                "SELECT (COUNT(?c) AS ?count)\n" +
                "WHERE {\n" +
                "    ?c a skos:Concept .\n" +
                "}";

        logger.debug("OwnNamesList: " + ownNamesList);
        String serviceQuery = "select \n" +
                "    distinct ?ForeignInstance ?ForeignName\n" +
                "where { \n" +
                "    ?ForeignInstance <http://www.w3.org/2004/02/skos/core#prefLabel> ?fname .  \n" +
                "    FILTER(Lang(?fname) = \"en\") .\n" +
                "    FILTER(lcase(?ForeignName) IN(" + ownNamesList + ")) .\n" +
                "    BIND(STR(?fname) as ?ForeignName) .\n" +
                "}\n";
        //+ "LIMIT 10";
        logger.debug("service query: " + serviceQuery);

        /*
        serviceQuery throws error that says
        > Exception in thread "main" org.apache.jena.query.QueryException:
        >   Endpoint returned Content-Type: text/html which is not recognized for SELECT queries
        cause could be that the set is empty, so use the serviceTestQuery until works
         */
        logger.debug("initialize service query execution");
        QueryExecution serviceQueryExecution = QueryExecutionFactory.sparqlService(
                "http://cgi.vocabs.ga.gov.au/endpoint", serviceQuery); // idea at https://narkive.com/inklpyuc:4.380.221: add "?force=true"
        logger.debug("service query initialized");

        logger.debug("starting service selection execution");
        ResultSet serviceResults;
        try {
            serviceResults = serviceQueryExecution.execSelect();
        } catch (QueryException e) {
            serviceResults = null;
        }
        logger.debug("service selection executed");

        if (serviceResults != null) {
            logger.debug("SPARQL-Result-Head: " + serviceResults.getResultVars());

            if (!serviceResults.hasNext()) {                            //if rs.next() returns false
                //then there are no rows.
                System.out.println("No records found");
            }
            while (serviceResults.hasNext()) {
                logger.debug(serviceResults.next().toString());
            }
        } else {
            logger.debug("SPARQL-Results empty!");
        }
        // logger.debug("SPARQL-Result-RowNo: " + results.getRowNumber());


        return serviceResults;

        // CONSTRUCT APPROACH
        /*
        Query query = QueryFactory.create(testQueryAgainstEndpointAsConstruct);
        QueryExecution queryExecution = QueryExecutionFactory.create(query, model);

        Model constructedModel = queryExecution.execConstruct();

        return null;
        */

    }

    /**
     * the parameter of webserver query
     *
     * @param thisQueryString
     */
    public void setThisQueryString(String thisQueryString) {
        config.setThisQueryString(thisQueryString);
    }


    private double getWeight(Resource resource) {
        // todo for issue #55
        /*
        if(resource.hasProperty(weightProperty)) {
            return Integer.parseInt(resource.getProperty(weightProperty).getObject().toString());
        }
        */

        return 0;

    }

    private String getLocalName(Resource resource) {
        String localname = resource.getLocalName();
        if (localname == null) {
            String uri = resource.getURI();

            if (uri == null) {
                return "unnamed";
            }
            return uri.replace(owlPrefix, "");

        /*if (resource == null || resource.getURI() == null) {
            return "UnknownName";
        }
        if (uri.contains(owlPrefix)) {

         */
            /*
        }
        String[] iriparts = uri.split("#");
        if (iriparts.length > 1) {
            return iriparts[1];
        }
        return uri;

             */
        }
        return localname;
    }

    /**
     * look up the whole URI just by knowing the localname.
     * Used to not always have to write the prefix.
     * If multiple prefixes fit, only the first of {@link Preferences#ALL_PREFIXES} will take turn.
     *
     * @param modelToUse     model in which should be looked for resource
     * @param localname the localname that is known
     * @return the / the 'best' Uri of the localname
     */
    public String getUriOfLocalname(Model modelToUse, String localname) {
        for (String prefix : Preferences.ALL_PREFIXES) {
            if (modelToUse.getResource(prefix + localname) != null) {
                return prefix + localname;
            }
        }
        return null;
    }

    public GraphDisplay getGraphDisplay() {
        return graphDisplay;
    }

    public void setCallerToWebserver() {
        config.setCaller(CallConfig.Caller.WEBSERVER);
    }
}


