package geothermal_db_graph.app.main;

import geothermal_db_graph.app.config.Preferences;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;

import java.io.IOException;

/**
 * Run the program with different arguments to generate a bunch of
 * maps and graphs of different views. For testing reasons mainly.
 * <p>
 * But because no longer used, it is outdated.
 */
public class MakeAll {

    /**
     * log4j-logger of this class. Preferences defined in ./log4j2.xml
     */
    private static final Logger logger = LogManager.getLogger(MakeAll.class);


    public static void main(String[] args) throws IOException {
        // logger configuration
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.INFO);

        String[] testowl = {"", "--testowl"};
        String[] noclustering = {"", "--noclustering"};
        String[] pathfinding = {"", "--nopathfinding"};
        String[] parameterasedge = {"", "--parameterasedge"};
        String[] noclusteredges = {"", "--noclusteredges"};
        for (String a : testowl) {
            for (String b : noclustering) {
                for (String c : pathfinding) {
                    for (String d : parameterasedge) {
                        for (String e : noclusteredges) {
                            if ((b.equals("") || e.equals(""))
                                    && !(Preferences.ENABLE_KSP && c.equals(""))) {
                                String[] geothermalArgs = {a, b, c, d, e};
                                logger.debug(geothermalArgs);
                                GeothermalOwlGraph geoOwlGr = new GeothermalOwlGraph();
                                geoOwlGr.make(geothermalArgs);
                            }
                        }
                    }
                }
            }
        }
    }
}
