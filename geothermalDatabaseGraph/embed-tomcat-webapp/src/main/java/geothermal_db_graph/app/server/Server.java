package geothermal_db_graph.app.server;

public interface Server {
    public void run(String[] args);
}