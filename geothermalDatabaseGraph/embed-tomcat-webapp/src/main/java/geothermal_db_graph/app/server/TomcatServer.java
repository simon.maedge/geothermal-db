package geothermal_db_graph.app.server;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import geothermal_db_graph.app.servlets.ImgServlet;
import geothermal_db_graph.app.servlets.MapServlet;
import jakarta.servlet.http.HttpServlet;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * made based on https://julianjupiter.com/blog/java-web-application-with-embedded-tomcat
 */
public class TomcatServer implements Server {

    private static final Logger LOGGER = LoggerFactory.getLogger(TomcatServer.class);
    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 8880;
    private static final String DEFAULT_CONTEXT_PATH = "/geothermalGraph"; // adjusted
    private static final String DOC_BASE = ".";
    private static final String ADDITION_WEB_INF_CLASSES = ".";
    private static final String WEB_APP_MOUNT = "/WEB-INF/classes";
    private static final String INTERNAL_PATH = "/";

    @Override
    public void run(String[] args) {
        int port = port(args);
        Tomcat tomcat = tomcat(port);

        try {
            tomcat.start();
        } catch (LifecycleException exception) {
            System.err.println(exception.getMessage());
            System.out.println("Exit...");
            System.exit(1);
        }

        //String url =  DEFAULT_HOST + ":" + port + DEFAULT_CONTEXT_PATH;
        System.out.println("\nApplication started with URL " + DEFAULT_HOST + ":" + port + DEFAULT_CONTEXT_PATH);
        System.out.println("Hit Ctrl + D or C to stop it...");
        tomcat.getServer().await();
        //openURLinBrowser(DEFAULT_HOST + ":" + port + DEFAULT_CONTEXT_PATH);
    }

    private Tomcat servlets(Tomcat tomcat, Context context) {
        // configure web applications
        // Map
        HttpServlet mapServlet = new MapServlet();
        String mapServletName = "MapServlet";
        String mapUrlPattern = "/map";

        tomcat.addServlet(DEFAULT_CONTEXT_PATH, mapServletName, mapServlet);
        context.addServletMappingDecoded(mapUrlPattern,mapServletName);

        // Img
        HttpServlet imgServlet = new ImgServlet();
        String imgServletName = "ImgServlet";
        String imgUrlPattern = "/pic";

        tomcat.addServlet(DEFAULT_CONTEXT_PATH, imgServletName, imgServlet);
        context.addServletMappingDecoded(imgUrlPattern,imgServletName);

        return tomcat;
    }

    private int port(String[] args) {
        if (args.length > 0) {
            String port = args[0];
            try {
                return Integer.valueOf(port);
            } catch (NumberFormatException exception) {
                LOGGER.error("Invalid port number argument {}", port, exception);
            }
        }

        return DEFAULT_PORT;
    }

    private Tomcat tomcat(int port) {
        Tomcat tomcat = new Tomcat();
        tomcat.setHostname(DEFAULT_HOST);
        tomcat.getHost().setAppBase(DOC_BASE);
        tomcat.setPort(port);
        tomcat.getConnector();
        Context context = context(tomcat);

        servlets(tomcat, context);

        return tomcat;
    }

    private Context context(Tomcat tomcat) {
        Context context = tomcat.addWebapp(DEFAULT_CONTEXT_PATH, DOC_BASE);
        File classes = new File(ADDITION_WEB_INF_CLASSES);
        String base = classes.getAbsolutePath();
        WebResourceRoot resources = new StandardRoot(context);
        resources.addPreResources(new DirResourceSet(resources, WEB_APP_MOUNT, base, INTERNAL_PATH));
        context.setResources(resources);

        return context;
    }

    private void openURLinBrowser(String url) {
        Desktop desk= Desktop.getDesktop();
        // now we enter our URL that we want to open in our
        // default browser
        try {
            desk.browse(new URI(url));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }
}