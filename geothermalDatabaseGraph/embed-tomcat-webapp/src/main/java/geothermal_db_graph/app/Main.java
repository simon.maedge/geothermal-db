package geothermal_db_graph.app;

import geothermal_db_graph.app.server.Server;
import geothermal_db_graph.app.server.TomcatServer;

public class Main {
    public static void main(String[] args) {
        Server tomcat = new TomcatServer();
        tomcat.run(args);
    }

}
