package geothermal_db_graph.app.things;

import j2html.tags.DomContent;

import static j2html.TagCreator.*;
import static j2html.TagCreator.rawHtml;

/**
 * Capture literal values about one literature per instance used in collecting
 * information about {@link GeothermalObject}s.
 */
public class Literature {

    private String title;
    private String author;
    private String doi;
    private String fauthor;
    private int year;

    private String bibTex;

    public Literature(String title) {
        this.title = title;
    }

    public Literature(String title, String author, String fauthor, int year) {
        this.title = title;
        this.author = author;
        this.fauthor = fauthor;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthors(String author) {
        this.author = author;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getFauthor() {
        return fauthor;
    }

    public void setFauthor(String fauthor) {
        this.fauthor = fauthor;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    /**
     * creates a representation as HTML
     *
     * @return
     */
    public DomContent toHTML() {
        return join(
                // author
                (getAuthor() != null)
                        ? rawHtml( // to make in one line
                        (getFauthor() != null)
                                ? getAuthor().replace(getFauthor(), "<b>" + getFauthor() + "</b> ") + "."
                                : getAuthor()
                )
                        : rawHtml(""),

                // titel
                b(getTitle()),

                // year
                rawHtml(
                        getYear() > 0
                                ? ", " + getYear()
                                : ""
                )
        );
    }

    public String getBibTex() {
        return bibTex;
    }

    public void setBibTex(String bibTex) {
        this.bibTex = bibTex;
    }
}
