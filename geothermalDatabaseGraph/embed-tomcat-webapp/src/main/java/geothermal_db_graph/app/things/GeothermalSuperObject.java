package geothermal_db_graph.app.things;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.Collections;
import java.util.LinkedList;

/**
 * extends the {@link GeothermalObject}, so contains at least all the information
 * and methods of this class - with the difference that this describes per
 * instance not a specific method or parameter, but a whole cluster of methods
 * and parameters. These {@link GeothermalObject}s are collected in this class.
 * <p>
 * A collection of {@link GeothermalObject}s
 * with some more information
 * (for clustering the GeothermalObjects).
 * All member methods have some same Parameters
 * that are stored in .incoming and .outgoing
 */
public class GeothermalSuperObject extends GeothermalObject {

    /**
     * log4j-logger of this class. Preferences defined in ./log4j2.xml
     */
    private static final Logger logger = LogManager.getLogger(GeothermalSuperObject.class);

    private final LinkedList<GeothermalObject> geoObjects = new LinkedList<>();
    public boolean finalized = false, disableClusterEdges;
    private String representingGeoName = null;

    /**
     * Cluster-Constructor
     *
     * @param iri  the localName (uri-suffix)
     * @param name the shown Name
     */
    public GeothermalSuperObject(String iri, String name, GeothermalThing geothermalThing, boolean disableClusterEdges) {
        super(iri, name, geothermalThing);
        this.disableClusterEdges = disableClusterEdges;
    }

    /**
     * adds edges to cluster and
     * removes them from all {@link #geoObjects}
     */
    public void finalizeCluster() {
        if (!disableClusterEdges &&
                (!finalized || incoming.isEmpty() && outgoing.isEmpty())) {
            incoming = calculateInsOrOuts(true);
            outgoing = calculateInsOrOuts(false);

            removeSuperEdgesFromObjs(true);
            removeSuperEdgesFromObjs(false);
        }
        finalized = true;
    }


    /**
     * calculates the same (isIn ? In- : Out-)Edges of of all {@link #geoObjects}
     *
     * @param isIn if isIn In-Parameter, else out-Parameter
     * @return the same Edges as GeothermalEdge-LinkedList
     */
    private LinkedList<GeothermalEdge> calculateInsOrOuts(boolean isIn) {

        // prepare iteration
        // should cont edges of allisIn?ins:outs of the cluster
        LinkedList<GeothermalEdge> sameEdges = new LinkedList<>(); // should cont

        // init firstGeoObjs
        if (!geoObjects.isEmpty()) {
            // init iteration pt 2

            // # idea
            // ## first
            // assign with all edges of first containing GeoObj
            if (isIn)
                sameEdges.addAll(geoObjects.getFirst().getIncoming());
            else
                sameEdges.addAll(geoObjects.getFirst().getOutgoing());


            // ## then
            // remove with iteration over every geoObj
            // every not matching isIn?source:destination
            for (GeothermalObject currentGeoObj : geoObjects) {
                LinkedList<GeothermalEdge> currentGeoObjEdges = (isIn ? currentGeoObj.getIncoming() : currentGeoObj.getOutgoing());

                // remove every edge in sameEdge that
                // is not contained in that geo obj
                sameEdges.removeIf(sameEdge -> !containsEdgeWithEnd(isIn, currentGeoObjEdges, sameEdge));
            }

        }
        return sameEdges;
    }

    /**
     * # if isIn:
     * checks whether the list contains an edge
     * having the same source as the edge
     * <p>
     * # if not isIn
     * checks whether the list contains an edge
     * having the same destination as the edge
     */
    public boolean containsEdgeWithEnd(boolean isIn, LinkedList<GeothermalEdge> list, GeothermalEdge edge) {
        for (GeothermalEdge currentListEdge : list) {
            if (isIn ? currentListEdge.equalsSource(edge) : currentListEdge.equalsDestination(edge)) {
                return true;
            }
        }
        return false;
    }

    /**
     * remove all Edges of this SuperObj from
     * containing GeoObjects {@link #geoObjects}
     */
    public void removeSuperEdgesFromObjs(boolean isIn) {


        for (GeothermalObject geoObj : geoObjects) {
            if (representingGeoName == null)
                representingGeoName = geoObj.getName();

            // ! Changes are made on original !
            LinkedList<GeothermalEdge> geoObjEdges = (isIn ? geoObj.getIncoming() : geoObj.getOutgoing());

            geoObjEdges.removeIf(sameEdge -> containsEdgeWithEnd(isIn, isIn ? incoming : outgoing, sameEdge));
            // geoObjEdges.removeAll(isIn ? incoming : outgoing);

        }

    }

    public boolean containsGeoObject(String localname) {
        for (GeothermalObject geothermalObject : geoObjects) {
            if (geothermalObject.getLocalName().equals(localname)) {
                return true;
            }
        }
        return false;
    }


    public LinkedList<GeothermalObject> getGeoObjects() {
        return geoObjects;
    }

    public void addGeothermalObject(GeothermalObject geoObj) {
        geoObjects.add(geoObj);
    }

    @Override
    public String toEdgeNodeEdge() {
        String str = "";

        for (GeothermalEdge s : incoming) {
            str += "\"" + s.getFromNode().getName() + "\" -> \"" + representingGeoName + "\" " +
                    " [lhead=cluster" + getLocalName() + " , color=orange]; \n";
        }
        for (GeothermalEdge s : outgoing) {
            str += "\"" + representingGeoName + "\" -> \"" + s.getToNode().getName() + "\"" +
                    " [ltail=cluster" + getLocalName() + " , color=orange]; \n";
        }

        return str;

    }

    /**
     * GraphVIZ-Dot-String for creating a cluster
     * with all containing {@link #geoObjects}
     *
     * @return Cluster String
     */
    public String toClusterString() {
        String str = "\n";
        String declPrefix = "subgraph cluster";
        String declSuffix = " {\n";
        String suffix = "}\n";
        String colordef = "color=orange; \n";

        String declMain = "";
        String ranksamePrefix = "  { ";
        String ranksame = "";
        String ranksameSuffix = "}\n";
        declMain += this.getName();
        String initMain = "";
        // @TODO labels give errors

        String labelName = StringUtils.join(
                StringUtils.splitByCharacterTypeCamelCase(this.getName()),
                ' '
        );

        initMain += "  label=\"" + labelName + "\";\n";
        Collections.sort(geoObjects);
        for (GeothermalObject geoObj : geoObjects) {
            ranksame += "\"" + geoObj.getName() + "\"; ";

        }
        str += declPrefix + declMain + declSuffix
                + initMain
                + ranksamePrefix + ranksame + ranksameSuffix
                + colordef
                + suffix;
        logger.trace(str);
        return str;
    }

}

