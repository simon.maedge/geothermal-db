package geothermal_db_graph.app.things;

/**
 * Describer of a {@link GeothermalObject},
 * says whether it is of type Method, Parameter or Object.
 */
public enum GeothermalThing {
    METHOD,

    // both parameter-ish
    // == both are generated from a method or are used for methods
    PARAMETER,
    OBJECT
}
