package geothermal_db_graph.app.things;

import org.apache.jena.rdf.model.Statement;

/**
 * Describes a connection between two {@link GeothermalObject}s and/or
 * {@link GeothermalSuperObject}s. Semantically, it tells which parameters
 * and methods rely on each other and result.
 */
public class GeothermalEdge implements Comparable<GeothermalEdge> {
    private final GeothermalObject fromNode;
    private final GeothermalObject toNode;
    /**
     * RDF Statement about the edge connection from predicate to
     */
    private final Statement statement;


    /**
     * Constructor of the Edge.
     * <p>
     * from ---→ to
     * with the corresponding RDF Stateent
     */
    public GeothermalEdge(GeothermalObject fromNode, GeothermalObject toNode, Statement statement) {
        this.fromNode = fromNode;
        this.toNode = toNode;
        this.statement = statement;
    }


    /**
     * The weight of this node res yen's ksp algorithm
     *
     * @return the weight in double
     */
    public double getWeight() {

        // ? a possible implementation
        return fromNode.getWeight();

        // ! just default -> issue #55 todo
        // return 1;
    }


    public Statement getStatement() {
        return statement;
    }

    public GeothermalObject getToNode() {
        return toNode;
    }

    public GeothermalObject getFromNode() {
        return fromNode;
    }

    /**
     * checks equality for source
     *
     * @param g obj
     * @return true if this source and objs source equal
     */
    public boolean equalsSource(GeothermalEdge g) {
        return fromNode.equals(g.getFromNode());
    }

    /**
     * checks equality for source
     *
     * @param g obj
     * @return true if this source and objs source equal
     */
    public boolean equalsDestination(GeothermalEdge g) {
        return toNode.equals(g.getToNode());
    }

    /**
     * two edges equal if the source nodes equal and the destination nodes equal
     *
     * @param g Edge obj
     * @return whether this and g equal in src and dest
     */
    public boolean equals(GeothermalEdge g) {
        return toNode.equals(g.toNode)
                && fromNode.equals(g.fromNode);
    }

    /**
     * compared by {@link GeothermalEdge#toNode}
     *
     * @param o Edge obj
     * @return comparison result of both's {@link GeothermalEdge#toNode}
     */
    @Override
    public int compareTo(GeothermalEdge o) {
        return toNode.compareTo(o.getToNode());
    }
}
