package geothermal_db_graph.app.things;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import geothermal_db_graph.app.config.Preferences;
import geothermal_db_graph.app.pathfinding.graph.Edge;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Locale;


/**
 * Collects literal values and references about a specific
 * geothermal method or parameter in each instance,
 * and provides methods for managing direct neighbors and string
 * representation (especially for graphviz dot description).
 * <p>
 * Geothermal Objects are actually mainly
 * Methods as (circled Node) and
 * Parameters as --Edge-→ rectangleNode --Edge--→
 * <p>
 * the shapes can differ if parameter set manually
 */
public class GeothermalObject implements Comparable<GeothermalObject> {

    /**
     * log4j-logger of this class. Preferences defined in ./log4j2.xml
     */
    private static final Logger logger = LogManager.getLogger(GeothermalObject.class);

    private final GeothermalThing geothermalThing;
    /**
     * An IRI to identify object.
     * In RDF-Relations maybe just the IRI-Suffix
     */
    private final String localName;
    /**
     * name to display
     */
    private final String name;
    /**
     * list for edge-subjects
     */
    protected LinkedList<GeothermalEdge> incoming = new LinkedList<>();
    /**
     * list for edge-objects
     */
    protected LinkedList<GeothermalEdge> outgoing = new LinkedList<>();
    /**
     * for parameters: normally using medium
     */
    protected LinkedList<String> mediums = new LinkedList<>();
    private Preferences.Importance importance = Preferences.Importance.NORMAL;
    private String thisQueryString;
    private String shapeMethod = "oval", shapeParameter = "rectangle";
    private String objectobjectString = "color=red ";//beige ";
    /**
     * weight for edge todo for issue #55
     */
    private double weight = 1;

    /**
     * Node-Constructor
     *
     * @param iri  the localName (uri-suffix)
     * @param name the shown Name
     */
    public GeothermalObject(String iri, String name, GeothermalThing geothermalThing) {
        for (String[] strarr : Preferences.PREFIX_REPLACEMENTS) {
            if (iri.contains(strarr[0]))
                iri = iri.replace(strarr[0], "");
            if (name.contains(strarr[0])) {
                name = name.replace(strarr[0], "");
                //name += "_om2";
            }

        }
        this.localName = iri.replace(Preferences.PREFIX, "");
        this.name = name;
        this.geothermalThing = geothermalThing;
    }

    /**
     * make Node to EdgeNodeEdge by adding a (subject, object)-tuple.
     * one of both can be null if unknown
     *
     * @param in  Method--→Param
     * @param out Param--→
     */
    public void addEdge(GeothermalEdge in, GeothermalEdge out) {
        if (this.incoming == null) {
            this.incoming = new LinkedList<>();
        }
        if (this.outgoing == null) {
            this.outgoing = new LinkedList<>();
        }

        if (in != null) this.incoming.push(in);
        if (out != null) this.outgoing.push(out);
    }


    /**
     * Edges: get toEdge String Representation
     * Nodes: get toNode String Representation
     *
     * @return Edge: toEdge; Node: toNode
     */
    @Override
    public String toString() {
        return (geothermalThing == GeothermalThing.METHOD) ? toEdgeNodeEdge() : toNode();
    }


    /**
     * use this to get an extra Node between by edge connected nodes
     * WARNING: no corrections made that were made in toEdge
     *
     * @return GraphViz-Query-Part of this
     */
    public String toEdgeNodeEdge() {
        String str = "";

        str += "\"" + getName() + "\" ["
                // DEFAULT
                // + "shape=oval "
                + "shape=" + shapeMethod + " "
                + getImportanceString()
                + getMapLinkNodeString()
                + "]; \n";

        // sort the lists for deterministic string
        Collections.sort(incoming);
        Collections.sort(outgoing);

        for (GeothermalEdge s : incoming) {
            str += "\"" + s.getFromNode().getName() + "\" -> \"" + getName() + "\"; \n";
        }
        for (GeothermalEdge s : outgoing) {
            str += "\"" + getName() + "\" -> \"" + s.getToNode().getName() + "\"; \n";
        }

        return str;
    }

    /**
     * get the String-representation of this obj as an Edge.
     * <p>
     * (actually redundant because toEdgeNodeEdge ist used instead)
     *
     * @return GraphViz-Query-Part of this
     */
    public String toEdge() {
        String str = "";

        String nullFromNode = "null From Method";
        String nullToNode = "null To Method";

        // @todo remove occurances of i
        //int i = 0;
        int in_i = 0, out_i = 0;
        do {
            do {
                if (!(out_i >= outgoing.size()) && !(in_i >= incoming.size())) { // @TODO check! Abfrage bisher !(..&&..), nicht (!..)&&(!..)
                    String inStr, outStr;
                    inStr = (incoming.isEmpty()) ? nullFromNode : incoming.get(in_i).getToNode().toString();
                    outStr = (outgoing.isEmpty()) ? nullToNode : outgoing.get(out_i).getFromNode().toString();
                    logger.info("Edge: ");
                    str += "\"" + inStr + "\" -> \"" +
                            outStr +
                            "\" [label=\"" + getName() + " " /*+ i */ + "\"]";
                    //i++;
                    // logger
                    /*if(getName().equals("fracture spacing")) {
                        logger.debug("FRACTURE SPACING: " + str);
                    }*/
                }
                out_i++;

            } while (out_i < outgoing.size());
            in_i++;

        } while (in_i < incoming.size());

        logger.info("FULL STR: " + str);
        return str;
    }


    /**
     * get the String-representation of this obj as a Node
     *
     * @return GraphViz-Query-Part of this
     */
    public String toNode() {
        //+ "shape=diamond "
        StringBuilder str = new StringBuilder().append("\"" + getName() + "\"[");
        str.append("shape=" + shapeParameter + " ");
        if (geothermalThing == GeothermalThing.OBJECT) {
            str.append(objectobjectString);
        }
        str.append(getImportanceString())
                .append(getMapLinkNodeString())
                .append(makeXlabelString())
                .append("] ; \n");
        return str.toString();
    }

    public String makeXlabelString() {
        if (mediums.isEmpty()) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder("xlabel=<<TABLE cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");

        // mediums.add("medium"); // remove
        for (String medium : mediums) {
            String camelCaseInvertedMedium = StringUtils.join(
                    StringUtils.splitByCharacterTypeCamelCase(medium),
                    ' '
            ).toLowerCase(Locale.ROOT);
            String fontcolor = "#a8a8a8";
            if (thisQueryString != null) {
                if (thisQueryString.contains("selectNode=" + medium)) {
                    fontcolor = "#3cb371";
                } else if (thisQueryString.contains("kspSource=" + medium + "&") || thisQueryString.contains("kspTarget=" + medium + "&") || thisQueryString.endsWith("kspTarget=" + medium) || thisQueryString.contains("compOfCon=" + medium + "&") || thisQueryString.endsWith("compOfCon=" + medium)) {
                    fontcolor = "#00cd66";
                }
            }

            stringBuilder.append("<TR><TD HREF=\"" +
                    getMapLinkString(medium).replace("&", "&amp;") +
                    "\" " +
                    "title=\"medium of " + name + "\"" +
                    "><font color=\"" + fontcolor + "\">" + camelCaseInvertedMedium + "</font></TD></TR>");

        }
        // stringBuilder.append("<TR><TD HREF=\"bar.com\"><font color=\"green\">foooooo</font></TD></TR>");
        stringBuilder.append("</TABLE>>");
        return stringBuilder.toString();
    }

    /**
     * returns the Node Attribute this GeoObj should have
     * reg. the {@link Preferences.Importance}-Flag ({@link GeothermalObject#importance})
     *
     * @return GraphViz-Substring
     */
    private String getImportanceString() {
        switch (importance) {
            case HIGH:
                return "color=springgreen3 "; //mediumseagreen
            case MEDIUM:
                return "color=mediumseagreen "; //mediumseagreen
            //return "color=deepskyblue "; //deepskyblue
            default:
                return "";
        }
    }

    /**
     * only for incomings applicable
     *
     * @param g remove edge with that obj as source
     */
    public void removeInEdgeWithSource(GeothermalObject g) {
        incoming.removeIf(in -> in.getFromNode().equals(g));
    }

    /**
     * only for outgoing applicable
     *
     * @param g remove edge with that obj as destination
     */
    public void removeOutEdgeWithDestination(GeothermalObject g) {
        outgoing.removeIf(out -> out.getToNode().equals(g));
    }

    /**
     * the link to link to by klicking THIS object in the graph map
     *
     * @return a url
     */
    private String getMapLinkNodeString() {
        return "URL=\"" + getMapLinkString(null) + "\"";
    }

    private String getMapLinkString(String mediumLocalName) {
        String url = "";
        String[] thisQueryStrings;

        String linkIRI = (mediumLocalName == null) ? localName : mediumLocalName;

        if (thisQueryString != null) {
            thisQueryStrings = thisQueryString.split("&");
            url = "map?";

            String oldSelected, compOfConned;

            // iterate over all parameters
            for (String str : thisQueryStrings) {
                if (str != null && !str.contains("selectNode")) {
                    // not selected and no ksp -> just add
                    if (!(str.contains("kspSource") && thisQueryString.contains("selectNode"))
                            && !(str.contains("kspTarget") && thisQueryString.contains("selectNode"))
                            && !(str.contains("compOfCon") && thisQueryString.contains("selectNode"))
                            && !(Preferences.BEHAVIOUR_ON_1ST_NODE_CLICK == Preferences.On1stNodeClick.compOfCon
                            && str.contains("compOfCon"))) {
                        url += str + "&";
                    } // else do not add / remove ksp-Param
                } else { // case: there is already a selected (or compOfCon if option set) node -> run ksp
                    compOfConned = str.replace("compOfCon=", "");
                    oldSelected = str.replace("selectNode=", "");

                    // defaults for ksp
                    if (linkIRI.equals(oldSelected)) {
                        if (thisQueryString.contains("compOfCon")) {

                            url = url.replace("compOfCon", "prevCompOfCon");
                        }
                        url += "compOfCon=" + linkIRI;
                    } else {
                        if (!thisQueryString.contains("kspK=")) {
                            url += "kspK=10&";
                        }
                        if (!thisQueryString.contains("kspKth")) {
                            url += "kspKth=all&";
                        }

                        url += "kspSource=" + oldSelected + "&kspTarget=" + linkIRI + "&";
                    }
                    if (linkIRI.equals(compOfConned)) {
                        //if (Preferences.BEHAVIOUR_ON_1ST_NODE_CLICK == Preferences.On1stNodeClick.compOfCon) {
                        url = url.replace("compOfCon", "kspSource");
                        url += "&kspTarget=" + linkIRI + "&";
                        //} else {}
                    }
                }
            }
            url = url.replace("null&", "");
            if (thisQueryString.contains("selectNode")) {

                url = url.replace("selectNode", "prevSelectNode");
            } else {
                url +=
                        ((Preferences.BEHAVIOUR_ON_1ST_NODE_CLICK == Preferences.On1stNodeClick.compOfCon)
                                ? "compOfCon="
                                : "selectNode=")
                                + linkIRI;
            }

            /*
            for (String str : thisQueryStrings) {
                if (str != null && !str.contains("compOfCon")) {
                    url += str + "&";
                }
            }
            url = url.replace("null&","");
            if (thisQueryString.contains("compOfCon")) {

                url = url.replace("compOfCon","prevCompOfCon");
            }
            url += "compOfCon=" + localName;
            */


        }
        return ((!url.equals("")) ? url : Preferences.PREFIX + linkIRI);
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * Get the Iri to identify object.
     * In RDF-Relations maybe just the IRI-Suffix
     *
     * @return IRI / IRI-Suffix
     */
    public String getLocalName() {
        return localName;
    }

    /**
     * Get the name (that will be displayed)
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * get in-Parameter-Edge Names
     *
     * @return List of the Parameters
     */
    public LinkedList<GeothermalEdge> getIncoming() {
        return incoming;
    }

    /**
     * get out-Parameter-Edge Names
     *
     * @return List of the Parameters
     */
    public LinkedList<GeothermalEdge> getOutgoing() {
        return outgoing;
    }

    /**
     * set out-Parameter-Edge Names
     *
     * @param outgoing list of the Parameters
     */
    public void setOutgoing(LinkedList<GeothermalEdge> outgoing) {
        this.outgoing = outgoing;
    }

    /**
     * KSP-Edges by Nodes' Local Names
     *
     * @return
     */
    public LinkedList<Edge> getKspEdges() {
        LinkedList<Edge> kspEdges = new LinkedList<>();
        for (GeothermalEdge geoEdge : outgoing) {
            String from, to;
            kspEdges.add(
                    new Edge(
                            from = geoEdge.getFromNode().getLocalName(),
                            to = geoEdge.getToNode().getLocalName(),
                            from + "2" + to,
                            geoEdge.getWeight(),
                            geoEdge.getStatement().asTriple())
            );
        }
        return kspEdges;
    }

    /**
     * the parameter of webserver query
     *
     * @param thisQueryString the URL that queried the program.
     *                        "?parameter&parameter&..."
     */
    public void setThisQueryString(String thisQueryString) {
        this.thisQueryString = thisQueryString;
    }

    /**
     * for remarking nodes
     *
     * @param importance in default NORMAL,
     *                   higher if it is a compOfCon node or similar special
     */
    public void setImportance(Preferences.Importance importance) {
        this.importance = importance;
    }


    /**
     * set if not null
     *
     * @param shapeMethod    if not null set the method shape
     * @param shapeParameter if not null set the parameter shape
     */
    public void setShapeMethodAndParameter(String shapeMethod, String shapeParameter) {
        if (shapeMethod != null) {
            this.shapeMethod = shapeMethod;
        }
        if (shapeParameter != null) {
            this.shapeParameter = shapeParameter;
        }
    }

    /**
     * Sett the mediums this Parameter-geoObj describes
     *
     * @param mediums
     */
    public void addMediums(Collection<String> mediums) {
        this.mediums.addAll(mediums);
    }


    /**
     * Add a medium this Parameter-geoObj describes
     *
     * @param medium
     */
    public void addMedium(String medium) {
        mediums.add(medium);
    }


    /**
     * the type of thing (METHOD | PARAMETER currently)
     *
     * @return the type of geothermal thing
     */
    public GeothermalThing getGeothermalThing() {
        return geothermalThing;
    }


    /**
     * get costs that is defined for this Method / Parameter
     * for compOfCon (issue #25)
     */
    public double getCosts() {
        return 0;
    }

    //TODO this has to be utilized somehow or removed
    public double getCertainty() {
        return 1d;
    }

    /**
     * Compares this object with the specified object for order.
     * Returns a negative integer, zero, or a positive integer as this
     * object is less than, equal to, or greater than the specified object.
     *
     * @param geothermalObject - the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     */
    @Override
    public int compareTo(GeothermalObject geothermalObject) {
        return getLocalName().compareTo((geothermalObject.getLocalName()));
    }
}