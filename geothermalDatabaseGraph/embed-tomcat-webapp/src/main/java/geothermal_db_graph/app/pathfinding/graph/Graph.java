package geothermal_db_graph.app.pathfinding.graph;

/**
 * The Graph class implements a weighted, directed graph using an adjacency list representation.
 * <p>
 * Created by brandonsmock on 6/1/15.
 * see: https://github.com/bsmock/k-shortest-paths
 * <p/>
 * Adjusted to fit owl2graph Project. Main changes:
 * <ul>
 *   <li>Add constructor with GeothermalObject-List</li>
 *   <li>add getGraphVizString</li>
 *   <li>todo what is "triples"</li>
 *   <li>add containsEdgesWithoutTriples</li>
 *   <li>specify variable names</li>
 * </ul>
 *
 */

import org.apache.jena.graph.Triple;
import geothermal_db_graph.app.things.GeothermalObject;

import java.io.*;
import java.util.*;

public class Graph {
    private HashMap<String, Node> nodes;

    public Graph(List<GeothermalObject> geothermalObjects){
        this();
        for(GeothermalObject geothermalObject : geothermalObjects){
            addNode(geothermalObject);
            addEdges(geothermalObject.getKspEdges());
        }
    }

    public Graph() {
        nodes = new HashMap<String, Node>();
    }

    public Graph(String filename) {
        this();
        readFromFile(filename);
    }

    public String getGraphVizString() {
        String content = "Digraph YenGraph{ ";
        for (Node node : nodes.values()) {

            // add node
            if (node.geothermalObject != null) {
                // colorize the GeoObj-Node by Certainty
                double value = (node.geothermalObject.getCertainty() * 0.33d);
                content += "\n\t" + node.getLabel() + "[color= \"" + (float) value + ",1.0, 1.0\",style=filled]";
            } else {
                // colorize the Non-GeoObj-Node by 1,1,1
                content += "\n\t" + node.getLabel() + "[color= \"1.0 ,1.0, 1.0\",style=filled]";
            }

            // add edges of the node
            String fromNode = "\n\t" + node.getLabel() + " -> ";
            for (Iterator<List<Edge>> i = node.neighbors.values().iterator(); i.hasNext(); ) {
                List<Edge> neighbors = i.next();
                for (Iterator<Edge> j = neighbors.iterator(); j.hasNext(); ) {
                    Edge edge = j.next();
                    // label edge with and colorize it by weight
                    double value = 0.33d - (edge.getWeight() * 0.33d) / 100d;
                    content += fromNode + edge.getToNode() + "[label=\"" + edge.getLabel() + "(" + Math.round(edge.getWeight()) + ")\" color=\"" + (float) value + "," + "1.0, 1.0 " + "\"]";
                }
            }
        }
        return content + "\n}";
    }


    public boolean containsEdgesWithoutTriples() {
        boolean allRight = true;
        for (Node node : nodes.values()) {
            for (Edge edge : node.getEdges()) {
                if (edge.getTriple() == null) {
                    allRight = false;
                    System.out.println(getClass().getSimpleName() + ": " + node + "\n\t" + edge + "\n does not have a Triple");
                }
            }
        }
        return !allRight;
    }

    public Graph(HashMap<String, Node> nodes) {
        this.nodes = nodes;
    }

    public int numNodes() {
        return nodes.size();
    }

    public int numEdges() {
        int edgeCount = 0;
        for (Node node : nodes.values()) {
            edgeCount += node.getEdges().size();
        }
        return edgeCount;
    }

    public Node addNode(GeothermalObject geothermalObject) {
        return addNode(new Node(geothermalObject));
    }

    public Node addNode(String label, boolean isLiteral) {
        return addNode(new Node(label, isLiteral));
    }

    public Node addNode(String label) {
        return addNode(label, false);
    }

    public Node addNode(Node node) {
        String label = node.getLabel();
        if (!nodes.containsKey(label)) {
            nodes.put(label, node);
            return node;
        }
        return null;
    }

    public List<Edge> addEdge(GeothermalObject subject, String property, GeothermalObject object, Triple triple, double weight) {
        List<Edge> edges = new LinkedList<>();
        if (subject != null && object != null && property != null) {
            Edge edge = addEdge(subject.getLocalName(), object.getLocalName(), property, weight,triple);
            if (edge != null) {
                edges.add(edge);
            }
        }
        return edges;
    }

    public Edge addEdge(String label1, String label2, String edgeLabel, double weight, Triple triple) {
        if (!nodes.containsKey(label1))
            addNode(label1);
        if (!nodes.containsKey(label2))
            addNode(label2);
        return nodes.get(label1).addEdge(label2, edgeLabel, weight, triple);
    }

    public void addEdge(Edge edge) {
        Edge newEdge = addEdge(edge.getFromNode(), edge.getToNode(), edge.getLabel(), edge.getWeight(), edge.getTriple());
        newEdge.setTriple(edge.getTriple());
    }

    public void addEdges(List<Edge> edges) {
        for (Edge edge : edges) {
            addEdge(edge);
        }
    }

    public Edge removeEdge(String nodeLabel1, String nodeLabel2, String edgeLabel) {
        if (nodes.containsKey(nodeLabel1)) {
            Edge edge = nodes.get(nodeLabel1).getNeighborEdge(nodeLabel2, edgeLabel);
            double weight = nodes.get(nodeLabel1).removeEdge(nodeLabel2, edgeLabel);
            if (weight != Double.MAX_VALUE) {
                return edge.clone();
            }
        }

        return null;
    }

    public double getEdgeWeight(String label1, String label2, String edgeLabel) {
        if (nodes.containsKey(label1)) {
            Node node1 = nodes.get(label1);
            if (node1.getNeighbors().containsKey(label2)) {
                Edge edge = node1.getNeighborEdge(label2, edgeLabel);
                if (edge != null) {
                    return edge.getWeight();
                }
            }
        }

        return Double.MAX_VALUE;
    }

    public HashMap<String, Node> getNodes() {
        return nodes;
    }

    public List<Edge> getEdgeList() {
        List<Edge> edgeList = new LinkedList<Edge>();

        for (Node node : nodes.values()) {
            edgeList.addAll(node.getEdges());
        }
        return edgeList;
    }

    public Set<String> getNodeLabels() {
        return nodes.keySet();
    }

    public Node getNode(String label) {
        return nodes.get(label);
    }

    public List<Edge> removeNode(String label) {
        LinkedList<Edge> edges = new LinkedList<Edge>();
        if (nodes.containsKey(label)) {
            Node node = nodes.remove(label);
            edges.addAll(node.getEdges());
            edges.addAll(removeEdgesToNode(label));
        }

        return edges;
    }

    public void removeEdge(Edge edge) {
        Node fromNode = nodes.get(edge.getFromNode());
        Node toNode = nodes.get(edge.getToNode());
        removeEdge(edge, fromNode);
    }

    private void removeEdge(Edge edge, Node node) {
        List<Edge> properties = node.getNeighbor(edge.getToNode());
        properties.remove(edge);
        if (properties.isEmpty()) {
            node.getNeighbors().remove(edge.getToNode());
        }
    }

    public List<Edge> removeEdgesToNode(String label) {
        List<Edge> edges = new LinkedList<Edge>();
        for (Node node : nodes.values()) {
            if (node.getAdjacencyList().contains(label)) {
                for (Edge edge : node.getNeighbor(label)) {
                    double weight = node.removeEdge(label, edge.getLabel());
                    edges.add(new Edge(node.getLabel(), label, edge.getLabel(), weight, edge.getTriple()));
                }
            }
        }
        return edges;
    }

    public void clear() {
        nodes = new HashMap<String, Node>();
    }

    public void readFromFile(String fileName) {
        try {
            BufferedReader in = new BufferedReader(new FileReader(fileName));

            String line = in.readLine();

            while (line != null) {
                String[] edgeDescription = line.split("\\s");
                if (edgeDescription.length == 3) {
                    addEdge(edgeDescription[0], edgeDescription[1], "unnamed", Double.parseDouble(edgeDescription[2]), null);
                    //addEdge(edgeDescription[1],edgeDescription[0],Double.parseDouble(edgeDescription[2]));
                } else if (edgeDescription.length == 4) {
                    addEdge(edgeDescription[0], edgeDescription[1], edgeDescription[2], Double.parseDouble(edgeDescription[3]), null);
                }
                line = in.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String toString() {
        StringBuilder graphStringB = new StringBuilder();
        Iterator<String> it = nodes.keySet().iterator();
        while (it.hasNext()) {
            String nodeLabel = it.next();
            graphStringB.append(nodeLabel.toString());
            graphStringB.append(": {");
            Node node = nodes.get(nodeLabel);
            Set<String> adjacencyList = node.getAdjacencyList();
            Iterator<String> alIt = adjacencyList.iterator();
            HashMap<String, List<Edge>> neighbors = node.getNeighbors();
            while (alIt.hasNext()) {
                String neighborLabel = alIt.next();
                graphStringB.append(neighborLabel.toString());
                graphStringB.append(": ");
                graphStringB.append(neighbors.get(neighborLabel));
                if (alIt.hasNext())
                    graphStringB.append(", ");
            }
            graphStringB.append("}");
            graphStringB.append("\n");
        }

        return graphStringB.toString();
    }

    public void graphToFile(String filename) {
        BufferedWriter writer = null;
        try {
            File subgraphFile = new File(filename);

            // This will output the full path where the file will be written to...
            System.out.println(subgraphFile.getCanonicalPath());

            writer = new BufferedWriter(new FileWriter(subgraphFile));
            writer.write(Integer.toString(nodes.size()) + "\n\n");

            Iterator<Node> it = nodes.values().iterator();
            while (it.hasNext()) {
                Node node = it.next();
                String nodeLabel = node.getLabel();
                if (nodes.containsKey(nodeLabel)) {
                    HashMap<String, List<Edge>> neighbors = node.getNeighbors();
                    Iterator<String> it2 = neighbors.keySet().iterator();
                    while (it2.hasNext()) {
                        String nodeLabel2 = it2.next();
                        if (nodes.containsKey(nodeLabel2)) {
                            for (Edge edge : neighbors.get(nodeLabel2))
                                writer.write(nodeLabel + " " + nodeLabel2 + " " + edge.getLabel() + " " + edge.getWeight() + "\n");
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }
    }
}
