package geothermal_db_graph.app.pathfinding.graph;

import org.apache.jena.graph.Triple;

/**
 * The Edge class implements standard properties and methods for a weighted edge in a directed graph.
 * <p>
 * Created by Brandon Smock on 6/19/15.
 * <p/>
 * Adjusted to fit owl2graph Project. Main changes:
 * <ul>
 *     <li>...</li>
 * </ul>

 */
public class Edge implements Cloneable, Comparable<Edge> {
    private String fromNode;
    private String toNode;
    private String label;
    private double weight;
    private Triple triple;

    public Edge() {
        this.fromNode = null;
        this.toNode = null;
        this.label = "unnamed";
        this.weight = Double.MAX_VALUE;
    }

    public Edge(String fromNode, String toNode, String label, double weight,Triple triple) {
        this.fromNode = fromNode;
        this.toNode = toNode;
        this.label = label;
        this.weight = weight;
        setTriple(triple);
    }

    public String getFromNode() {
        return fromNode;
    }

    public void setFromNode(String fromNode) {
        this.fromNode = fromNode;
    }

    public String getToNode() {
        return toNode;
    }

    public void setToNode(String toNode) {
        this.toNode = toNode;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Edge clone() {
        return new Edge(fromNode, toNode, label, weight, getTriple());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        sb.append(fromNode);
        sb.append(",");
        sb.append(toNode);
        sb.append("){");
        sb.append(label + " (" + weight + ") ");
        sb.append("}");

        return sb.toString();
    }

    public boolean equals(Edge edge2) {
        if (hasSameEndpoints(edge2) && getWeight() == edge2.getWeight() && label.equals(edge2.label))
            return true;

        return false;
    }

    public boolean hasSameEndpoints(Edge edge2) {
        if (fromNode.equals(edge2.getFromNode()) && toNode.equals(edge2.getToNode()))
            return true;

        return false;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public int compareTo(Edge o) {
        int r = fromNode.compareTo(o.getFromNode());
        if(r == 0){
            r = toNode.compareTo((o.getToNode()));
            if(r == 0){
                r = getLabel().compareTo(o.getLabel());
            }
        }
        return r;
    }

    public Triple getTriple() {
        return triple;
    }

    public void setTriple(Triple triple) {
        this.triple = triple;
    }


}
