package geothermal_db_graph.app.pathfinding.graph.util;

import geothermal_db_graph.app.pathfinding.graph.Edge;
import geothermal_db_graph.app.pathfinding.graph.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by brandonsmock on 6/6/15.
 */
public class DijkstraNode extends Node implements Comparable<DijkstraNode> {
    private double dist = Double.MAX_VALUE;
    private int depth;

    public DijkstraNode(double dist) {
        super();
        this.dist = dist;
    }

    public DijkstraNode(String label) {
        super(label);
        this.dist = 0.0;
    }

    public DijkstraNode(String label, double dist) {
        super(label);
        this.dist = dist;
    }

    public DijkstraNode(String label, double dist, int depth, String parent,String edgeLabel) {
        super(label);
        this.dist = dist;
        this.depth = depth;
        super.addEdge(parent,edgeLabel,0.0,null);
    }

    public double getDist() {
        return dist;
    }

    public void setDist(double dist) {
        this.dist = dist;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public void setParent(String parent, Edge edge) {
        super.neighbors = new HashMap<String, List<Edge>>();
        List<Edge> edgeList = new ArrayList<>();
        Edge edgeClone = edge.clone();
        edgeClone.setWeight(0.0);
        edgeList.add(edgeClone);
        super.neighbors.put(parent,edgeList);
    }

    public String getParent() {
        Set<String> neighborLabels = super.neighbors.keySet();
        if (neighborLabels.size() > 1) {
            return null;
        }
        if (neighborLabels.size() < 1) {
            return null;
        }
        return super.neighbors.keySet().iterator().next();
    }

    public int compareTo(DijkstraNode comparedNode) {
        double distance1 = this.dist;
        double distance2 = comparedNode.getDist();
        if (distance1 == distance2)
            return 0;
        if (distance1 > distance2)
            return 1;
        return -1;
    }

    public boolean equals(DijkstraNode comparedNode) {
        return this.getLabel().equals(comparedNode.getLabel());
    }
}
