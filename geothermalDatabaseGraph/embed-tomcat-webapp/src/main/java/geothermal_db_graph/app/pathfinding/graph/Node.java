package geothermal_db_graph.app.pathfinding.graph;

/**
 * The Node class implements a node in a directed graph keyed on a label of type String, with adjacency lists for
 * representing edges.
 * <p/>
 * Created by brandonsmock on 5/31/15.
 * see: https://github.com/bsmock/k-shortest-paths
 * <p/>
 * Adjusted to fit owl2graph Project. Main changes:
 * <ul>
 *      <li>Replace double by Edge-List in Hashmap {@link #neighbors}
 *      (assume, to store all adjazent Edges)</li>
 *      <li>add corresponding GeothermalObject (and in Constructor)</li>
 * </ul>
 */

import org.apache.jena.graph.Triple;
import geothermal_db_graph.app.things.GeothermalObject;

import java.util.*;

public class Node {
    protected String label;
    protected HashMap<String, List<Edge>> neighbors = new HashMap<>(); // adjacency list, with HashMap for each edge weight
    protected GeothermalObject geothermalObject;
    protected boolean isLiteral;

    public Node() {
        neighbors = new HashMap<>();
    }

    /**
     * make Node by geoObj's LocalName
     * @param geothermalObject
     */
    public Node(GeothermalObject geothermalObject) {
        this(geothermalObject.getLocalName());
        setGeothermalObject(geothermalObject);
    }

    public Node(String label, boolean isLiteral) {
        setLabel(label);
        setLiteral(isLiteral);
    }

    public Node(String label) {
        setLabel(label);
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public HashMap<String, List<Edge>> getNeighbors() {
        return neighbors;
    }

    public List<Edge> getNeighbor(String nodeLabel) {
        return getNeighbors().get(nodeLabel);
    }

    public Edge getShortestEdge(String nodeLabel) {
        double weight = Double.MAX_VALUE;
        Edge shortestEdge = null;
        for (Edge edge : getNeighbor(nodeLabel)) {
            if (edge.getWeight() < weight) {
                weight = edge.getWeight();
                shortestEdge = edge;
            }
        }
        return shortestEdge;
    }

    public Edge getNeighborEdge(String nodeLabel, String edgeLabel) {
        List<Edge> edges = getNeighbor(nodeLabel);
        if (edges != null) {
            for (Edge edge : edges) {
                if (edge.getLabel().equals(edgeLabel)) {
                    return edge;
                }
            }
        }
        return null;
    }

    public void setNeighbors(HashMap<String, List<Edge>> neighbors) {
        this.neighbors = neighbors;
    }

    public Edge addEdge(String toNodeLabel, String edgeLabel, Double weight, Triple triple) {
        Edge newEdge = new Edge(this.label, toNodeLabel, edgeLabel, weight, triple);

        if (neighbors.containsKey(toNodeLabel)) {
            List<Edge> edges = neighbors.get(toNodeLabel);
            boolean contained = false;
            for (Edge edge : edges) {
                if (edge.hasSameEndpoints(newEdge) && edge.getLabel().equals(newEdge.getLabel())) {
                    edge.setWeight(newEdge.getWeight());
                    contained = true;
                }
            }
            if (!contained) {
                edges.add(newEdge);
            }
        } else {
            List<Edge> newEdgeList = new ArrayList<>();
            newEdgeList.add(newEdge);
            neighbors.put(toNodeLabel, newEdgeList);
        }
        return newEdge;
    }


    public double removeEdge(String toNodeLabel, String edgeLabel) {
        if (neighbors.containsKey(toNodeLabel)) {
            List<Edge> edgeList = neighbors.get(toNodeLabel);
            for (Edge edge : edgeList) {
                if (edge.getLabel().equals(edgeLabel)) {
                    double weight = edge.getWeight();
                    neighbors.remove(toNodeLabel);
                    return weight;
                }
            }
        }
        return Double.MAX_VALUE;
    }

    public Set<String> getAdjacencyList() {
        return neighbors.keySet();
    }

    public LinkedList<Edge> getEdges() {
        LinkedList<Edge> edges = new LinkedList<Edge>();
//        for (String toNodeLabel : neighbors.keySet()) {
//            edges.add(new Edge(label, toNodeLabel, neighbors.get(toNodeLabel)));
//        }
        for (List<Edge> edgeList : neighbors.values()) {
            edges.addAll(edgeList);
        }
        return edges;
    }

    public String toString() {
        StringBuilder nodeStringB = new StringBuilder();
        nodeStringB.append(label);
        nodeStringB.append(": {");
        Set<String> adjacencyList = this.getAdjacencyList();
        Iterator<String> alIt = adjacencyList.iterator();
        HashMap<String, List<Edge>> neighbors = this.getNeighbors();
        while (alIt.hasNext()) {
            String neighborLabel = alIt.next();
            nodeStringB.append(neighborLabel.toString());
            nodeStringB.append(": ");
            nodeStringB.append(neighbors.get(neighborLabel));
            if (alIt.hasNext())
                nodeStringB.append(", ");
        }
        nodeStringB.append("}");
        nodeStringB.append("\n");

        return nodeStringB.toString();
    }

    public GeothermalObject getGeothermalObject() {
        return geothermalObject;
    }

    public void setGeothermalObject(GeothermalObject geothermalObject) {
        this.geothermalObject = geothermalObject;
    }

    public boolean isLiteral() {
        return isLiteral;
    }

    public void setLiteral(boolean literal) {
        isLiteral = literal;
    }
}
