package geothermal_db_graph.app.pathfinding.graph.util;

/**
 * Created by brandonsmock on 6/1/15.
 */

import geothermal_db_graph.app.pathfinding.graph.Edge;
import geothermal_db_graph.app.pathfinding.graph.Graph;
import geothermal_db_graph.app.pathfinding.graph.Node;
import geothermal_db_graph.app.pathfinding.graph.util.DijkstraNode;
import geothermal_db_graph.app.pathfinding.graph.util.ShortestPathTree;

import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;

public final class Dijkstra {

    private Dijkstra() {
    }

    public static Path shortestPath(Graph graph, String sourceLabel, String targetLabel) throws Exception {
        //if (!nodes.containsKey(sourceLabel))
        //    throw new Exception("Source node not found in graph.");
        try {
        HashMap<String, Node> nodes = graph.getNodes();
        ShortestPathTree predecessorTree = new ShortestPathTree(sourceLabel);
        PriorityQueue<DijkstraNode> pq = new PriorityQueue<DijkstraNode>();
        for (String nodeLabel : nodes.keySet()) {
            DijkstraNode newNode = new DijkstraNode(nodeLabel);
            newNode.setDist(Double.MAX_VALUE);
            newNode.setDepth(Integer.MAX_VALUE);
            predecessorTree.add(newNode);
        }
        DijkstraNode sourceNode = predecessorTree.getNodes().get(predecessorTree.getRoot());

           sourceNode.setDist(0);
           sourceNode.setDepth(0);
           pq.add(sourceNode);

           int count = 0;
           while (!pq.isEmpty()) {
               DijkstraNode current = pq.poll();
               String currLabel = current.getLabel();
               if (currLabel.equals(targetLabel)) {
                   Path shortestPath = new Path();
                   String currentNodeLabel = targetLabel;
                   String parentNodeLabel = predecessorTree.getParentOf(currentNodeLabel);
                   while (parentNodeLabel != null) {
                       List<Edge> edgeList = nodes.get(parentNodeLabel).getNeighbor(currentNodeLabel);
                       Edge edge = nodes.get(parentNodeLabel).getShortestEdge(currentNodeLabel);
                       shortestPath.addFirst(edge.clone());
//                    shortestPath.addFirst(new Edge(parentNodeLabel, currentNodeLabel, nodes.get(parentNodeLabel).getNeighbors().get(currentNodeLabel)));
                       currentNodeLabel = parentNodeLabel;
                       parentNodeLabel = predecessorTree.getParentOf(currentNodeLabel);
                   }
                   return shortestPath;
               }
               count++;
//            HashMap<String, Double> neighbors = nodes.get(currLabel).getNeighbors();
               HashMap<String, List<Edge>> neighbors = nodes.get(currLabel).getNeighbors();
               for (String currNeighborLabel : neighbors.keySet()) {
                   //TODO Hier wolltest du checken wie sich das mit Country1 und Russia6 verhält warum sie nicht über City1 und City 4 gehen können
                   DijkstraNode neighborNode = predecessorTree.getNodes().get(currNeighborLabel);
                   Double currDistance = neighborNode.getDist();
                   Edge shortestEdge = nodes.get(currLabel).getShortestEdge(currNeighborLabel);
                   Double newDistance = current.getDist() + shortestEdge.getWeight();
//              Double newDistance = current.getDist() + nodes.get(currLabel).getNeighbors().get(currNeighborLabel);
                   if (newDistance < currDistance) {
                       DijkstraNode neighbor = predecessorTree.getNodes().get(currNeighborLabel);
                       pq.remove(neighbor);
                       neighbor.setDist(newDistance);
                       neighbor.setDepth(current.getDepth() + 1);
                       neighbor.setParent(currLabel, shortestEdge);
                       pq.add(neighbor);
                   }
               }
           }
       } catch(NullPointerException e){
            e.printStackTrace(System.out);
            System.out.println(graph.toString());
            System.out.println("SourceLabel = " + sourceLabel);
            System.out.println("TargetLabel = " + targetLabel);

            System.exit(1);
        }
        return null;
    }
}
