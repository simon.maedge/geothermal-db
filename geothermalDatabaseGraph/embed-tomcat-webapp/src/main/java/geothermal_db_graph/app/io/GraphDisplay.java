package geothermal_db_graph.app.io;

import geothermal_db_graph.app.config.CallConfig;
import geothermal_db_graph.app.config.Preferences;
import geothermal_db_graph.app.main.GeothermalOwlGraph;
import geothermal_db_graph.app.things.*;
import geothermal_db_graph.app.pathfinding.graph.util.Path;
import geothermal_db_graph.app.things.*;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.parse.Parser;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.*;


/**
 * Create the display of the results. It is instantiated at the beginning
 * of the main class ({@link GeothermalOwlGraph}) and completed with the
 * geo-objects and successively more information.
 * <p>
 * Once all the main classes
 * are processed, the display is requested, whereupon GraphDisplay generates
 * graphviz dot code from all this information and then renders the graph as
 * an image or generates an HTML display from {@link GraphHtml}.
 * <p>
 * Last, it returns it to the main program (in case of using it as a WebApp)
 * or (in case of using it on the command line) creates files
 */
public class GraphDisplay {

    /**
     * log4j-logger of this class. Preferences defined in ./log4j2.xml
     */
    private final Logger logger = LogManager.getLogger(GraphDisplay.class);
    /**
     * the html maker
     */
    private final GraphHtml graphHtml;
    /**
     * GeothermalObjects that are on THIS Graph
     * key == localName
     * value == corresponding geoOBj
     */
    private final HashMap<String, GeothermalObject> geoObjects = new HashMap<>();
    /**
     * List of all GeothermalSuperobjects
     * that are in THIS Graph
     */
    private final LinkedList<GeothermalSuperObject> superGeoObjects = new LinkedList<>();
    /**
     * full generated strings
     */
    private CallConfig config;
    private String fullGraphVizString, fullGeneratedMapString, fullScaledMapString, fullHTMLString;

    /**
     * generated Image
     */
    private BufferedImage image;

    /**
     * by Yen calculated K shortest paths
     */
    private List<Path> kspPaths;


    public GraphDisplay(CallConfig config) {
        this.config = config;
        graphHtml = new GraphHtml(this, config);
    }


    /**
     * main for show functionality
     *
     * @param args no args expected
     */
    public static void main(String[] args) {
        // logger configuration
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.INFO);

        GraphDisplay graphDisplay = new GraphDisplay(new CallConfig());

        //add edge/method
        GeothermalObject lineal = new GeothermalObject("placeRuler", "Place Ruler", GeothermalThing.METHOD);
        graphDisplay.putGeoObject(lineal);
        GeothermalObject lengthUsingMethod = new GeothermalObject("lengthUsingMethod", "Length Using Method", GeothermalThing.METHOD);
        graphDisplay.putGeoObject(lengthUsingMethod);
        //add node/param
        GeothermalObject length = new GeothermalObject("length", "Length", GeothermalThing.PARAMETER);
        length.addEdge(
                new GeothermalEdge(length, lineal, null), // ! "null" -> ksp not usable
                new GeothermalEdge(lengthUsingMethod, length, null)
        );
        graphDisplay.putGeoObject(length);

        graphDisplay.displayGraph();
    }

    /**
     * forward message to be displayed in html and get the resulting html
     *
     * @param message message to be displayed
     * @return the resulting html
     */
    public String createDisplayMessage(String message) {
        return graphHtml.createDisplayMessage(message);
    }

    /**
     * make GraphVIZ query string with default preferences,
     * get the SVG-Image result
     * and display in an JPanel
     */
    public void displayGraph() {
        // finalize
        removeGeothermalObjectEdgesWithoutEnd();

        fullGraphVizString = makeFullString();
        logger.trace(fullGraphVizString);


        // save as file
        BufferedImage image = generateGraphVizImageAndMap(fullGraphVizString);

        // make frame of the image
        if (Preferences.OPEN_GENERATED_PICTURE_IN_WINDOW) {
            displayInFrame(image);
        }

        logger.trace("\n" +
                "#########################\n" +
                "## FULL GRAPHWIZ-STRING:\n" +
                "#########################\n" +
                fullGraphVizString);
    }


    /**
     * Use the GraphVIZ-Api to generate a SVG-Image
     * and creates HTML-CMAPX-MAP
     * (only place where it is used)
     *
     * @param graphString query-String
     * @return the SVG-Image
     */
    public BufferedImage generateGraphVizImageAndMap(String graphString) {
        MutableGraph g;
        image = null;
        try {

            g = new Parser().read(graphString);
            //image = Graphviz.fromGraph(g).rasterize(Rasterizer.SALAMANDER).toImage();
            image = Graphviz.fromGraph(g).render(Format.PNG).toImage();
            if (!config.isCallerWebserver()) {
                generatePNG(image);
            }

            generateCMAPX(g);


        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    /**
     * makes an image to a png file by
     * using parameters for pathname
     *
     * @param image the image to write
     * @throws IOException by writing file
     */
    private void generatePNG(BufferedImage image) throws IOException {
        File outputfile = new File("out/owlgraphs/owlgraph"
                + config.getPathparameter()
                + ".png");
        ImageIO.write(image, "png", outputfile);
        logger.info("Image written to file \"" + outputfile.getPath() + "\"");
    }

    /**
     * generates the CMAPX-XML-String to include
     * in HTML to make the embedded image clickable.
     * They depend on the same graph.
     * Saves the String in {@link #fullGeneratedMapString}
     *
     * @param graph the generation
     * @throws IOException
     */
    private void generateCMAPX(MutableGraph graph) throws IOException {

        // get original MAP String from Graph
        // (it's not local to get tested by testcases)
        fullGeneratedMapString = Graphviz.fromGraph(graph)
                .scale(0.75)
                .render(Format.CMAPX)
                .toString();
        logger.trace("GENERATED CMAPX STRING:\n" + fullGeneratedMapString);

        // scale the MAP to correct
        fullScaledMapString = scaleCMAPX(fullGeneratedMapString, 0.75);
        logger.trace("SCALED CMAPX STRING:\n" + fullScaledMapString);


        // init HTML file
        /* String.toFile(new File( "out/maps/graphmap" + pathparameter + ".cmapx"));*/

        if (!config.isCallerWebserver()) {

            String uriStr = "out/maps/graphmap" + config.getPathparameter() + ".html";

            FileWriter file = new FileWriter(uriStr);
            BufferedWriter writer = new BufferedWriter(file);

            // write HTML file
            writer.write(
                    fullHTMLString = graphHtml.generateHTMLString(false, null)
            );
            writer.close();
            logger.info("Map-HTML page written to file \"" + uriStr + "\"");

        } else {
            fullHTMLString = graphHtml.generateHTMLString(false, null);
        }


        // optionally: open in Browser
        /*
        if (Properties.OPEN_GENERATED_PICTURE_MAP_IN_BROWSER) {
            openHtmlMapInBrowser(uriStr);
        }
        */
    }

    /**
     * scale the coordinates of the CMAPX
     *
     * @param str   original cmapx xml string
     * @param scale factor to scale the map with.
     *              '0.75' makes the map 75% as large as before
     * @return the scaled xml string
     */
    private String scaleCMAPX(String str, double scale) {
        // convert input
        InputStream stream = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));

        // prepare output
        OutputStream out = new ByteArrayOutputStream();

        // prepare jdom usage
        SAXBuilder saxBuilder = new SAXBuilder();
        Document doc;
        Element rootElt;

        try {
            // prepare xml document
            doc = saxBuilder.build(stream);
            rootElt = doc.getRootElement();

            // get all area elements
            List<Element> areaEltList = rootElt.getChildren("area");

            // scale all numbers in the coordinates attribute
            for (Element areaElt : areaEltList) {
                Attribute coordsAttr = areaElt.getAttribute("coords");
                String coordsStr = coordsAttr.getValue();

                // accumulator for the new attribute string
                String newCoordsStr = "";

                // scale each number in this attribute
                for (String coord : coordsStr.split(",")) {
                    int newCoord = (int) (Integer.parseInt(coord) * scale + 0.5);

                    // accumulate every coordinate to the attribute
                    newCoordsStr += newCoord + ",";
                }
                coordsAttr.setValue(newCoordsStr);

            }

            // finalize
            XMLOutputter xmlOutput = new XMLOutputter();
            xmlOutput.setFormat(org.jdom2.output.Format.getPrettyFormat());
            xmlOutput.output(doc, out);

        } catch (IOException | NumberFormatException | JDOMException e) {
            e.printStackTrace();
        }

        return out.toString();
    }

    /**
     * makes the graphviz string
     * by querying the geo objects
     *
     * @return full string
     */
    private String makeFullString() {
        String fontsizeStr = ", fontsize=" + config.getFontsize();
        String bgULabelColor = config.getTheme() == CallConfig.Theme.DARK ? "grey15" : "white";
        String NodeFillColor = config.getTheme() == CallConfig.Theme.DARK ? "grey80" : "grey15";
        String graphPreferences = "bgcolor=" + bgULabelColor + ", fontcolor=" + NodeFillColor + " ";
        String nodePreferences = "color=" + NodeFillColor + ", style=filled, fontcolor=" + bgULabelColor + fontsizeStr;
        String edgePreferences = "color=" + NodeFillColor + ", minlen = 2, fontcolor=" + NodeFillColor + fontsizeStr;
        String defaults =
                "compound=true;\n" +
                        //"forcelabels=true ;\n" +
                        (config.getGraphDirection() == CallConfig.GraphDirection.LEFT_RIGHT ? "rankdir = LR;\n" : "") +
                        //"graph [fontsize=10 fontname=\"Verdana\" compound=true];" ;
                        "graph [" + graphPreferences + "]; \n" +
                        "node [" + nodePreferences + "]; \n" +
                        "edge [" + edgePreferences +
                        ((config.getEdgewidth() != null) ? ", penwidth=" + config.getEdgewidth() : "") +
                        "]; \n";
        String prefix =
                "digraph g { \n\n" + defaults;
        String suffix = "}";

        // generate Graph

        // @todo es funktioniert aber was genau mache ich hier - issue #38:
        if (config.getClustering() != CallConfig.Clustering.DISABLE) getNodeClusteredEdgeStr(true);

        String fullStr =
                prefix

                        + "\n// geothermal objects nodes and their edge\n"// getNodeEdgeStr of geoObjects
                        + getNodeEdgeStr(geoObjects.values())

                        + "\n// clusters (method categories)\n"// getSuperGeoObjectsString
                        + ((config.getClustering() != CallConfig.Clustering.DISABLE) ? getSuperGeoObjectsString() : "")

                        + "\n// edges from/to clusters\n"// getNodeClusteredEdgeStr 2nd half
                        + (config.getClustering() == CallConfig.Clustering.ENABLE ? getNodeClusteredEdgeStr(false) : "")

                        + suffix;

        return fullStr;
    }


    /**
     * make {@link #geoObjects} to a GraphVIZ-String
     *
     * @return GraphVIZ String
     */
    private String getNodeEdgeStr(Collection<GeothermalObject> geoObjectCollection) {
        String str = "";

        LinkedList<GeothermalObject> geoObjectList = new LinkedList<>(geoObjectCollection);
        Collections.sort(geoObjectList);

        for (GeothermalObject geothermalObject : geoObjectList) {
            str += geothermalObject.toString();
        }

        return str;
    }

    /**
     * make the Objects of {@link #superGeoObjects} to a GraphVIZ-String
     *
     * @return GraphVIZ String
     */
    private String getNodeClusteredEdgeStr(boolean justRemoveDublicates) {
        String str = "";

        Collections.sort(superGeoObjects);
        for (GeothermalSuperObject superGeoObject : superGeoObjects) {
            superGeoObject.finalizeCluster();

            String tmpstr = "";

            // add clusterEdges

            if (justRemoveDublicates)
                tmpstr += getNodeEdgeStr(superGeoObject.getGeoObjects());
            else
                tmpstr += superGeoObject.toString();
            //logger.info(str);
            str += tmpstr;

        }

        return str;

    }


    /**
     * make the Clusters of {@link #superGeoObjects} to a GraphVIZ-String.
     *
     * @return GraphVIZ String
     */
    private String getSuperGeoObjectsString() {
        String str = "\n";
        Collections.sort(superGeoObjects);
        for (GeothermalSuperObject geoSuperObj : superGeoObjects) {
            str += geoSuperObj.toClusterString();
        }
        return str;
    }

    /**
     * optionally show the generated Map in browser
     *
     * @param uriStr the uri of the map
     * @throws IOException
     */
    private void openHtmlMapInBrowser(String uriStr) throws IOException {
        URI uri = null;
        try {
            uri = new URI(System.getProperty("user.dir").replace("\\", "/") + "/" + uriStr);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        Desktop.getDesktop().browse(uri);

    }

    /**
     * make frame of the image
     *
     * @param image put into frame to show
     */
    private void displayInFrame(BufferedImage image) {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.setBackground(new Color(38, 38, 38));
        JFrame frame = new JFrame("Graph of ");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JLabel label = new JLabel();
        label.setIcon(new ImageIcon(image));
        panel.add(label);
        frame.add(panel);

        // add scrolling
        JScrollPane scrollPane = new JScrollPane(panel);
        frame.add(scrollPane);

        frame.pack();
        frame.setVisible(true);
    }


    // contains

    /**
     * @param localname geoObj representation we are looking for
     * @return wheter there is a geoObj in Owlgraph or in one of the Clusters
     */
    public boolean containsGeoObjOrIsInSuperGeoObject(String localname) {
        if (geoObjects.containsKey(localname)) {
            return true;
        }
        for (GeothermalSuperObject geothermalSuperObject : superGeoObjects) {
            if (geothermalSuperObject.containsGeoObject(localname)) {
                return true;
            }
        }

        return false;

    }


    // REMOVEs

    /**
     * removes geoObj from {@link #geoObjects}
     *
     * @param geoObj geoObject to remove
     */
    public void removeGeoObj(GeothermalObject geoObj) {
        logger.trace("removing " + geoObj.getLocalName() + "\ntoString(): " + geoObj);
        logger.trace(geoObjects.containsKey(geoObj.getLocalName()));
        geoObjects.remove(geoObj.getLocalName());
        logger.trace(geoObjects.containsKey(geoObj.getLocalName()));

    }

    /**
     * If GeoObjs got removed, edges at others may not have an existing Source res. Destination
     */
    public void removeGeothermalObjectEdgesWithoutEnd() {

        for (GeothermalObject geothermalObject : geoObjects.values()) {

            // fixme concurrent mod exc
            // by cloning

            // remove incoming edges without end
            LinkedList<GeothermalEdge> listIn = new LinkedList<>(geothermalObject.getIncoming());
            for (GeothermalEdge geothermalEdge : listIn) {
                if (!geoObjects.containsValue(geothermalEdge.getFromNode())) {
                    geothermalObject.getIncoming().remove(geothermalEdge);
                }
            }

            // remove outgoing edges without end
            LinkedList<GeothermalEdge> listOut = new LinkedList<>(geothermalObject.getOutgoing());
            for (GeothermalEdge geothermalEdge : listOut) {
                if (!geoObjects.containsValue(geothermalEdge.getToNode())) {
                    geothermalObject.getOutgoing().remove(geothermalEdge);
                }
            }
        }
    }


    // SETTER AND GETTER

    // set-to-er and foreign GETTER


    public BufferedImage getImage() {
        return image;
    }


    /**
     * add owl2graph.things.GeothermalObject to THIS graph
     *
     * @param geoObject Object to add to THIS Graph
     */
    public void putGeoObject(GeothermalObject geoObject) {
        geoObjects.put(geoObject.getLocalName(), geoObject);
    }

    // specific getter

    /**
     * get all geoObjs
     *
     * @return all geoObjs
     */
    public HashMap<String, GeothermalObject> getGeoObjects() {
        return geoObjects;
    }

    /**
     * get all geoObjects as Name as  Sparql formatted List:
     * `elt_1, elt_2, elt_3, ... elt_n`
     */
    public String getGeoObjectsSparqlFormattedList() {
        StringBuilder str = new StringBuilder();

        for (Iterator<GeothermalObject> iterator = geoObjects.values().iterator();
             iterator.hasNext(); ) {
            GeothermalObject geoObject = iterator.next();
            str.append("\"" + geoObject.getName() + "\"");
            if (iterator.hasNext()) {
                str.append(", ");
            }

        }

        // just for testing:
        //str.append(", \"russian code\", \"agate\", \"aggregate\"");
        return str.toString().toLowerCase();
    }

    /**
     * geth the geoObject for corresponding iri
     *
     * @param iri the iri of the requested geoObject
     * @return the geoObject
     */
    public GeothermalObject getGeoObject(String iri) {
        return geoObjects.get(iri);
    }

    /**
     * get a set of all {@link GeothermalSuperObject}s
     *
     * @return the set
     */
    public LinkedList<GeothermalSuperObject> getSuperGeoObjects() {
        return superGeoObjects;
    }

    /**
     * get the {@link GeothermalSuperObject} with corresponding localName.
     *
     * @param localName the matching name
     * @return the superGeoObj if it exists; null otherwise
     */
    public GeothermalSuperObject getSuperGeoObject(String localName) {

        // can also apply for more
        if (!superGeoObjects.isEmpty()) {
            for (GeothermalSuperObject superGeoObject : superGeoObjects) {
                if (superGeoObject.getLocalName().equals(localName)) {
                    return superGeoObject;
                }
            }
        }

        return null;
    }

    // setter

    /**
     * the focused Object for further Information
     *
     * @param focusedGeoObj
     */
    public void setFocusedGeoObj(GeothermalObject focusedGeoObj) {
        graphHtml.setFocusedGeoObj(focusedGeoObj);
    }

    /**
     * adds a Literature of focused Obj
     *
     * @param literatureList
     */
    public void setLiteratureOfFousedGeoObj(List<Literature> literatureList) {
        graphHtml.setLiteratureOfFousedGeoObj(literatureList);
    }

    /**
     * adds further information of the focused Object
     *
     * @param furtherInformation list of SimpleEntry-Pairs: (Predicate-Str, Object-Str)
     */
    public void setFurtherInformationOfFocusedObj(LinkedList<AbstractMap.SimpleEntry<String, String>> furtherInformation) {
        graphHtml.setFurtherInformationOfFocusedObj(furtherInformation);
    }

    public void setEndpointInformation(boolean endpointInformationRequested, LinkedList<AbstractMap.SimpleEntry<String, String>> endpointInformation) {
        graphHtml.setEndpointInformation(endpointInformationRequested, endpointInformation);
    }

    public void setMethodCategories(List<String> selectedMethodCats, HashMap<String, LinkedList<String>> allMethodCats, HashMap<String, Integer> allMethodCatsOccurrences) {
        graphHtml.setMethodCategories(allMethodCats, allMethodCatsOccurrences);
    }

    /**
     * getter for testing
     *
     * @return the corresponding Strings
     */
    public String getFullGraphVizString() {
        return fullGraphVizString;
    }


    // getter for testing

    public String getFullGeneratedMapString() {
        return fullGeneratedMapString;
    }

    public String getFullScaledMapString() {
        return fullScaledMapString;
    }

    public List<Path> getKspPaths() {
        return kspPaths;
    }

    // getter

    public void setKspPaths(List<Path> kspPaths) {
        this.kspPaths = kspPaths;
    }

    public String getFullHTMLString() {
        return fullHTMLString;
    }

    // "is"er


}
