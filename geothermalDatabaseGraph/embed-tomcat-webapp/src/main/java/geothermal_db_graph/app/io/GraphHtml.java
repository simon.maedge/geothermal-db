package geothermal_db_graph.app.io;

import geothermal_db_graph.app.config.CallConfig;
import geothermal_db_graph.app.config.Preferences;
import geothermal_db_graph.app.pathfinding.graph.util.Path;
import geothermal_db_graph.app.things.GeothermalObject;
import geothermal_db_graph.app.things.Literature;
import j2html.tags.*;

import java.awt.image.BufferedImage;
import java.util.*;

import static j2html.TagCreator.*;

/**
 * Provide a collection of functions to generate the HTML code.
 * For this purpose {@link GraphDisplay} gets all necessary information
 * from this class and forwards HTML to main program.
 */
public class GraphHtml {
    // upper classes
    private final GraphDisplay graphDisplay;
    private final CallConfig config;

    // further object information
    GeothermalObject focusedGeoObj;
    List<Literature> literatureOfFocusedGeoObj = new LinkedList<Literature>();
    List<AbstractMap.SimpleEntry<String, String>> furtherInformationOfFocusedObj = new LinkedList<AbstractMap.SimpleEntry<String, String>>();
    List<AbstractMap.SimpleEntry<String, String>> endpointInformation = new LinkedList<>();
    private boolean endpointInformationRequested = false;

    // method cats
    private HashMap<String, LinkedList<String>> allMethodCats = new HashMap<>();
    private HashMap<String, Integer> allMethodCatsOccurrences = new HashMap<>();

    private String BTN_COLOR, BTN_OUTLINE_COLOR;
    ;

    /**
     * constructor
     *
     * @param graphDisplay the underlying {@link GraphDisplay}
     */
    public GraphHtml(GraphDisplay graphDisplay, CallConfig config) {
        this.graphDisplay = graphDisplay;
        this.config = config;
        BTN_COLOR = config.getTheme() == CallConfig.Theme.DARK ? ".btn-light" : ".btn-primary";
        BTN_OUTLINE_COLOR = config.getTheme() == CallConfig.Theme.DARK ? ".btn-outline-light" : ".btn-outline-primary";
    }

    /**
     * create a html output with a given message
     *
     * @param message given message
     * @return corresponding html output
     */
    public String createDisplayMessage(String message) {
        return generateHTMLString(true, message);
    }

    /**
     * make a full html page with
     * title (with parameters),
     * image,
     * map,
     * comments if necessary,
     * and buttons for interaction with user.
     *
     * @param withoutImage
     * @param message
     * @return the html string
     */
    public String generateHTMLString(boolean withoutImage, String message) {
        // global method constant
        String margin = "20px";

        String includedMessage = (message != null ? message : "");
        if (graphDisplay.getGeoObjects().isEmpty()) {
            includedMessage += "The called map is empty. If you tried to run k-shortest-paths, " +
                    "there is no possible path from source to target.";
        }

        // image
        String imageSrc = config.isCallerWebserver()
                ? "pic"
                : "../owlgraphs/owlgraph" + config.getPathparameter() + ".png";
        int imageWidth = 0;
        int imageHeight = 0;
        BufferedImage image = graphDisplay.getImage();
        if (image != null) {
            imageWidth = image.getWidth();
            imageHeight = image.getHeight();
        }
        ContainerTag includeImage = div(attrs("#imageContent"),
                // include the message
                rawHtml(includedMessage),

                // if empty show error message,
                // otherwitse show or export the image
                img()
                        .withCondSrc(!graphDisplay.getGeoObjects().isEmpty() && !withoutImage, imageSrc)
                        .attr("usemap", "#g")
                        .attr("width", imageWidth)
                        .attr("height", imageHeight)
        );

        // further information
        ContainerTag furtherInformationAndPathTable = div(attrs("#furtherInformationAndPaths"),
                // further information about a focused obj
                (focusedGeoObj != null
                        ? getFurtherInformations()
                        : rawHtml("")),

                (!withoutImage && config.isKspEnabled()
                        ? __htmlComment("ksp-Path-List")
                        : rawHtml("")),
                (!withoutImage && config.isKspEnabled()
                        ? getKspPathList()
                        : rawHtml("")),
                getEndpointInformation()
        );

        // style constants

        // possible node shapes
        String[] possibleNodeShapes = {"rectangle", "square", "polygon", "oval", "circle", "triangle", "plaintext",
                "plain", "diamond", "trapezium", "parallelogram", "house", "pentagon", "hexagon", "octagon", "doublecircle",
                "invtriangle", "invtrapezium", "note", "tab", "folder", "box3d", "component", "cds", "rarrow", "larrow"};

        String[] possibleEdgeWidths = {"0.25", "0.5", "0.75", "1", "1.25", "1.5", "1.75", "2", "2.5", "3", "4", "5", "6", "7", "8", "9", "10"};
        String[] possibleFontSizes = {"5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"};

        ContainerTag styleButtons = div(attrs("#styleButtons"),
                getSwitchButton("Theme", "Light", "Dark", "theme=dark", "theme=light", config.getTheme() == CallConfig.Theme.LIGHT),
                getSwitchButton("Direction of Graph", "Left-Right", "Top-Down", "direction=tb", "direction=lr", config.getGraphDirection() == CallConfig.GraphDirection.LEFT_RIGHT),
                getDropdownString("shapeMethod", getParameterValue("shapeMethod"), possibleNodeShapes, "oval", "Shape of Method nodes"),
                getDropdownString("shapeParameter", getParameterValue("shapeParameter"), possibleNodeShapes, "rectangle", "Shape of Parameter nodes"),
                getDropdownString("edgeWidth", config.getEdgewidth() != null ? config.getEdgewidth() : "", possibleEdgeWidths, "1", "Edge Width"),
                getDropdownString("fontSize", config.getFontsize() != null ? config.getFontsize() : "", possibleFontSizes, "14", "Font Size")
        );

        // go back button (to previous page)
        ContainerTag goBackButton = divButtonGroup(
                a(attrs("#goBackButton.btn" + BTN_OUTLINE_COLOR),
                        "go back")
                        .withType("button")
                        .withHref("javascript:history.back()")
        );

        ContainerTag head = head(
                __htmlComment("Bootstrap required meta tags"),
                meta().withCharset("utf-8"),
                meta().withName("viewport").withContent("width=device-width, initial-scale=1, shrink-to-fit=no"),

                __htmlEmptyLine(),
                __htmlComment("Bootstrap CSS"),
                link().withRel("stylesheet").withHref(config.getTheme() == CallConfig.Theme.DARK ? "assets/bootstrap.css" : "bootstrap_default.css"),
                // link().withRel("stylesheet").withHref("bootstrap-switch.css"),
                link().withRel("stylesheet").withHref("assets/style.css"),

                title("GDG  " + config.getPathparameter().replace("_", " ") + " - Geothermal Database Graph"),

                script().withSrc("assets/jquery.min.js"),

                __htmlEmptyLine(),
                __htmlComment("Latest compiled and minified JavaScript"),
                script().withSrc("assets/bootstrap.bundle.js"),
                //script().withSrc("bootstrap-switch.js"),
                script(rawHtml("function toggle(source) {\n" +
                        "  \t\tcheckboxes = document.getElementsByName('onlyMethodCat');\n" +
                        "  \t\tfor(var i=0, n=checkboxes.length;i<n;i++) {\n" +
                        "    \t\tcheckboxes[i].checked = source.checked;}}"
                )).withType("text/javascript"),
                script(rawHtml("$(document).ready(function($) {\n" +
                        "           $(\".clickable-row\").click(function() {\n" +
                        "              window.document.location = $(this).data(\"href\");\n" +
                        "           });\n" +
                        "        });")).withType("text/javascript")/*,
                script(rawHtml("var myCustomScrollbar = document.querySelector('.my-custom-scrollbar');\n" +
                        "var ps = new PerfectScrollbar(myCustomScrollbar);\n" +
                        "\n" +
                        "var scrollbarY = myCustomScrollbar.querySelector('.ps__rail-y');\n" +
                        "\n" +
                        "myCustomScrollbar.onscroll = function () {\n" +
                        "  scrollbarY.style.cssText = `top: ${this.scrollTop}px!important; height: 400px; right: ${-this.scrollLeft}px`;\n" +
                        "}")).withType("text/javascript")*/
        );

        // map
        List<String> mapLines = null;
        if (!withoutImage) {
            mapLines = Arrays.asList(graphDisplay.getFullScaledMapString()
                    .replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "")
                    .split("\n"));
        }
        DomContent map = !withoutImage
                ? each(mapLines, mapLine -> rawHtml(mapLine))
                : rawHtml("");

        // body
        ContainerTag body = body(
                div(attrs(".container-fluid"),
                        h1("Geothermal Database Graph").withStyle("margin-top:" + margin),
                        getFeatureBadges(),
                        br(),
                        __htmlComment("the main content"),
                        div(attrs("#content" + /* .my-custom-scrollbar.my-custom-scrollbar-primary. */ ".overflow-auto"),
                                // include image
                                // no nodes ==> empty
                                __htmlComment("Image to be connected with #g CMAMX-map"),
                                includeImage,
                                // make it to map by including cmapx
                                __htmlComment("XMAPX-map to connect with image in #imageContent"),
                                map,
                                br()
                        ),
                        div(attrs("#lowerHalf"), // previously ".container"
                                a(attrs("#rawhtml.link-secondary"), "Show Raw Dot Code").withHref("dot?" + config.getThisQueryString()),
                                div(attrs(".row"),

                                        // second column
                                        div(attrs(".col"),

                                                __htmlComment("further information if focused object; paths if k-shortest-paths queried"),
                                                furtherInformationAndPathTable

                                        ),
                                        div(attrs(".col"),
                                                __htmlComment("buttons to interact "),
                                                div(attrs("#buttons"),
                                                        // buttons
                                                        h2("Graph Configuration"),
                                                        h4("Features"),
                                                        getFeaturesButtons(),
                                                        h4("Style"), //br(),
                                                        styleButtons,
                                                        br(),
                                                        goBackButton
                                                )
                                        ),
                                        div(attrs(".col"),
                                                h2("Method Categories"),
                                                getMethodCatSelectionForm()
                                        )

                                ).withStyle("margin-top:" + margin + "; margin-bottom:" + margin + ";")
                        )
                ).attr("align", "center")
        ).withStyle(config.getTheme() == CallConfig.Theme.DARK ? "background-color:#262626" : "");

        String footerColors = config.getTheme() == CallConfig.Theme.DARK ? ".bg-dark.text-white" : "";
        DomContent footer = footer(attrs(".text-center"),
                br(),
                a("GitLab Repository").withHref("https://gitlab.gwdg.de/simon.maedge/geothermal-db"),
                br(),
                //a("USAGE Deutsch, ").withHref("https://pad.gwdg.de/xLIgiAJLTdeQREtcho0P6DE"),
                a("Usage Documentation").withHref("https://pad.gwdg.de/xLIgiAJLTdeQREtcho0P6EN"),
                br(),
                br()
        );

        return document().render() +
                "\n" +
                html(head, body, footer).withCondStyle(config.getTheme() == CallConfig.Theme.DARK, "scrollbar-color: #454a4d #262626;").renderFormatted();
    }

    /**
     * creates a Container Tag
     */
    private ContainerTag getFeatureBadges() {
        // ksp
        int k, kth;
        k = config.getKspK() > 0 ? config.getKspK() : Preferences.KSP_k;
        kth = config.getKspKth() >= 0 ? config.getKspKth() : Preferences.KSP_kth;

        String homeQueryStr = "owl=normal&clustering=enable&direction=lr";

        return div(attrs("#featuresBadges"),
                (config.isCallerWebserver())
                        ? (config.getThisQueryString().equals(homeQueryStr))
                        ? span(attrs(".badge.badge-pill.badge-primary"),
                        text("Initial View / Home")
                )
                        : a(attrs(".badge.badge-pill.badge-secondary"),
                        text("Initial View / Home")
                ).withHref("map?" + homeQueryStr)
                        : rawHtml(""),

                // selected
                (config.getSelectedNode() != null)
                        ? span(attrs(".badge.badge-pill.badge-primary"),
                        text(
                                "node selected"),
                        a(attrs(".badge.badge-pill.badge-primary"),
                                text("X")
                        ).withHref(getLinkWithRemovedParameters(new String[]{"selectNode"})))
                        : rawHtml(""),

                // comp of con
                (config.isCompOfConEnabled())
                        ? span(attrs(".badge.badge-pill.badge-primary"),
                        text(
                                "node focused"),
                        a(attrs(".badge.badge-pill.badge-primary"),
                                text("X")
                        ).withHref(getLinkWithRemovedParameters(new String[]{"compOfCon"})))
                        : rawHtml(""),

                // comp of con
                (config.isKspEnabled())
                        ? span(attrs(".badge.badge-pill.badge-primary"),
                        text(
                                "shortest paths between nodes calculated"
                        ),
                        a(attrs(".badge.badge-pill.badge-primary"),
                                text("X")
                        ).withHref(getLinkWithRemovedParameters(new String[]{"kspSource", "kspTarget"})))
                        : rawHtml("")
        );
    }

    /**
     * Formular to select method categories that only should be shown
     *
     * @return
     */
    private DomContent getMethodCatSelectionForm() {
        String[] thisQueryStrings = null;

        // look for the parametername
        if (config.getThisQueryString() != null) {
            thisQueryStrings = config.getThisQueryString().split("&");
        }

        DomContent[] hiddenContents = new DomContent[]{};
        if (thisQueryStrings != null) {
            hiddenContents = new DomContent[thisQueryStrings.length];
        }

        for (int i = 0; i < hiddenContents.length; i++) {
            if (thisQueryStrings[i].split("=").length > 1) {
                String paramName = thisQueryStrings[i].split("=")[0];
                String paramValue = thisQueryStrings[i].split("=")[1];
                if (!(paramName.equals("onlyMethodCat") || paramName.equals("methodCatSelBy"))) {
                    hiddenContents[i] = input().withType("hidden").withName(paramName).withValue(paramValue);
                }
            }
        }

        DomContent hidden = div(attrs("#hidden"),
                hiddenContents
        );

        DomContent[] catCheckboxes = new DomContent[]{};
        if (allMethodCats != null) {

            catCheckboxes = new DomContent[128];
            for (int i = 0; i < allMethodCats.size(); i++) {
                for (String catGroupStr : allMethodCats.keySet()) {
                    catCheckboxes[i++] = h4(catGroupStr.replace("Category", ""));

                    // <input type="checkbox" id="category10" name="onlyMethodCat" value="geometricMethod" checked="">
                    // <label for="category10"> geometric (4) </label>
                    for (String catStr : allMethodCats.get(catGroupStr)) {
                        DomContent inputcheck;
                        if (config.getSelectedMethodCats().contains(catStr) || config.getSelectedMethodCats().isEmpty()) {
                            inputcheck = input(attrs(".custom-control-input")).withType("checkbox").withId("category" + i).withName("onlyMethodCat").withValue(catStr).attr("checked"); //attrs(".form-check-input")
                        } else {
                            inputcheck = input(attrs(".custom-control-input")).withType("checkbox").withId("category" + i).withName("onlyMethodCat").withValue(catStr); // attrs form-check-input
                        }

                        catCheckboxes[i++] = div(attrs(".custom-control.custom-switch"), //attrs(".form-check.form-switch"),
                                inputcheck,
                                label(attrs(".custom-control-label"), " " + catStr.replace("Method", "")).attr("for", "category" + (i - 1)),
                                badge(allMethodCatsOccurrences.getOrDefault(catStr, 0))
                        );//form-check-label
                        //catCheckboxes[i++] = br();
                    }
                    catCheckboxes[i++] = br();
                }
            }
        }

        DomContent checkboxes = div(attrs("#checkboxes"),
                div(attrs("#toggle.mr-1.custom-control.custom-switch"),
                        input(attrs("#toggleall.custom-control-input")).withType("checkbox").attr("onClick", "toggle(this)").condAttr(config.getSelectedMethodCats() == null || config.getSelectedMethodCats().isEmpty() || allMethodCatsSelected(), "checked", "checked"),
                        label(attrs(".custom-control-label"), b("Toggle all")).attr("for", "toggleall")
                ),
                div(attrs(".custom-control.custom-radio.custom-control-inline"),
                        input(attrs(".custom-control-input")).withType("radio").withId("unite").withName("methodCatSelBy").withValue("unite").condAttr(!getParameterValue("methodCatSelBy").equals("intersect"), "checked", "check"),
                        label(attrs(".custom-control-label"), "unite").attr("for", "unite")
                ),
                div(attrs(".custom-control.custom-radio.custom-control-inline"),
                        input(attrs(".custom-control-input")).withType("radio").withId("intersect").withName("methodCatSelBy").withValue("intersect").condAttr(getParameterValue("methodCatSelBy").equals("intersect"), "checked", "check"),
                        label(attrs(".custom-control-label"), "intersect").attr("for", "intersect")
                ),

                br(), br(),
                div(attrs("#categoryCheckboxes"),
                        catCheckboxes
                ),
                div(attrs(".custom-control.custom-switch"),
                        (config.getSelectedMethodCats() == null || config.getSelectedMethodCats().contains("unclassified") || config.getSelectedMethodCats().isEmpty())
                                ? input(attrs("#unclassified.custom-control-input")).withType("checkbox").withId("unclassified").withName("onlyMethodCat").withValue("unclassified").attr("checked")
                                : input(attrs("#unclassified.custom-control-input")).withType("checkbox").withId("unclassified").withName("onlyMethodCat").withValue("unclassified"),
                        label(attrs(".custom-control-label"), "unclassified").attr("for", "unclassified"),
                        badge(allMethodCatsOccurrences.get("unclassified"))
                ),
                br()
        );


        DomContent buttons =
                config.isCallerWebserver()
                        ? div(attrs(".btn-group.btn-group-toggle"),
                        button(attrs(".btn" + BTN_OUTLINE_COLOR), "Apply").withType("submit").withValue("Apply!"),
                        a(attrs(".btn.btn-outline-danger"), "Reset").withType("button").withHref(getLinkWithRemovedParameters(new String[]{"onlyMethodCat"}))
                )
                        : div(attrs(".btn-group.btn-group-toggle"),
                        button(attrs(".btn" + BTN_OUTLINE_COLOR), "Apply").withType("submit").withValue("Apply!").attr("disabled"),
                        a(attrs(".btn.btn-outline-danger.disabled"), "Reset").withType("button").withHref(getLinkWithRemovedParameters(new String[]{"onlyMethodCat"}))
                );
        return form(
                hidden,
                checkboxes,
                buttons
        ).withAction("map").withMethod("get");
    }

    /**
     * creates a ContainerTag including all feature buttons
     *
     * @return
     */
    ContainerTag getFeaturesButtons() {
        /*
        // ksp
        int k, kth;
        k = config.getKspK() > 0 ? config.getKspK() : Preferences.KSP_k;
        kth = config.getKspKth() >= 0 ? config.getKspKth() : Preferences.KSP_kth;
        */

        return div(attrs("#featuresButtons"),
                // with(out) test ontology
                /*divButtonGroup(
                        config.isOwlTestowl()
                                ? aButton(true, "test ontology") +
                                getOppositeButtonOfPar("owl", "disable")
                                : aButton(false, "test ontology") +
                                getOppositeButtonOfPar("owl", "enable")
                ),*/

                /*
                // with(out) components of connectivity / further object information
                divButtonGroup(
                        config.isCompOfConEnabled()
                                ? aButton(true, "further object information") +
                                getOppositeButtonOfPar("compOfCon", "disable")
                                : aButton(false, "further object information ") +
                                getOppositeButtonOfPar("compOfCon", "double click a node to enable")
                ),

                // with(out) k-shortest-paths
                divButtonGroup(
                        config.isKspEnabled()
                                ? (aButton(true,
                                (kth == Integer.MAX_VALUE
                                        ? "all "
                                        : "the " + kth + ". of ") + k + "-shortest-paths"
                        ) +
                                getOppositeButtonOfPar("ksp", "disable"))
                                : aButton(false, "k-shortest paths ") +
                                getOppositeButtonOfPar("ksp", "click a source and a target node to enable")
                ),
                */

                divButtonGroup(
                        config.getClustering() != CallConfig.Clustering.DISABLE
                                // with(out) clustering
                                ? aButton(true, "clustering") +
                                getOppositeButtonOfPar("clustering", "disable")
                                : aButton(false, "clustering") +
                                getOppositeButtonOfPar("clustering", "enable")
                ),

                // with(out) cluster's edges
                divButtonGroup(
                        config.getClustering() == CallConfig.Clustering.ENABLE
                                ? aButton(true, "the cluster's edges ") +
                                getOppositeButtonOfPar("clustering", "noclusteredges", "disable")
                                : aButton(false, "the cluster's edges ") +
                                getOppositeButtonOfPar("clustering", "noclusteredges", "enable")
                )
        );
    }

    /**
     * A button group to change Direction: Left-Right and Top-Bottom
     *
     * @param xlabel
     * @param label2
     * @param label1
     * @param hrefParameter1
     * @param hrefParameter2
     * @param firstActive
     * @return the Tag
     */
    ContainerTag getSwitchButton(String xlabel, String label2, String label1, String hrefParameter1, String hrefParameter2, boolean firstActive) {
        // button top down and left rigth
        ContainerTag button1;
        ContainerTag button2;

        String disabledIfNotWebserver = !config.isCallerWebserver() ? ".disabled" : "";


        if (firstActive) {
            // top down
            String href1 = null;
            if (config.getThisQueryString() != null)
                href1 = "map?" + (
                        config.getThisQueryString().contains(hrefParameter2)
                                ? config.getThisQueryString().replace(hrefParameter2, hrefParameter1)
                                : config.getThisQueryString() + "&" + hrefParameter1
                );
            //config.getThisQueryString().replace(hrefParameter2, hrefParameter1);

            button1 = a(attrs(".btn" + BTN_OUTLINE_COLOR + disabledIfNotWebserver),
                    label1
            ).withType("button").withCondHref(config.getThisQueryString() != null, href1);

            // left right
            button2 = a(attrs(".btn" + BTN_COLOR),
                    label2
            ).withType("button").attr("disabled");

        } else {
            // top down
            button1 = a(attrs(".btn" + BTN_COLOR),
                    label1
            ).withType("button").attr("disabled");

            // left right
            String href2 = null;
            if (config.getThisQueryString() != null)
                href2 = "map?" + (
                        config.getThisQueryString().contains(hrefParameter1)
                                ? config.getThisQueryString().replace(hrefParameter1, hrefParameter2)
                                : config.getThisQueryString() + "&" + hrefParameter2
                );
            button2 = a(attrs(".btn" + BTN_OUTLINE_COLOR + disabledIfNotWebserver),
                    label2
            ).withType("button").withCondHref(config.getThisQueryString() != null, href2);


        }

        return divButtonGroup(
                rawHtml(xlabel + ": &nbsp;"),
                button1,
                button2
        );
    }

    /**
     * button group
     *
     * @param content the button elements
     * @return the whole group of buttons
     */
    ContainerTag divButtonGroup(DomContent... content) {
        return p(
                div(attrs(".btn-group.btn-group-toggle"),
                        content
                )
        ).withStyle("vertical-align: middle");
    }

    /**
     * as {@link #divButtonGroup(DomContent...)} but with raw html string input
     *
     * @param rawHtml raw html text
     * @return the whole group of buttons
     */
    ContainerTag divButtonGroup(String rawHtml) {
        return divButtonGroup(rawHtml(rawHtml));
    }

    /**
     * a link in buttonstyle
     *
     * @param enabled whether the button should be clickable (enabled) or not.
     * @param content the Name of the property the button cares about
     * @return the button String
     */
    String aButton(boolean enabled, String content) {
        return a(attrs(".btn" + (enabled ? ".btn-success" : ".btn-danger")),
                (enabled ? "with " : "without ") + content
        ).withType("button").condAttr(true, "disabled", null)
                .render();

    }

    /**
     * List the K shortest paths in a interactive table. onclick: show only the path
     *
     * @return the table
     */
    ContainerTag getKspPathList() {

        // iterate
        int i = 0;
        String kspKthString = (config.getKspKth() == Integer.MAX_VALUE) ? "all" : Integer.toString(config.getKspKth());
        // if (kspKthString == null) kspKthString = "25";
        String toReplace = "kspKth=" + kspKthString;

        // the table
        // size: 1 header + no of paths + "all"-row
        ContainerTag[] tableRows = new ContainerTag[graphDisplay.getKspPaths().size() + 2];
        // header
        tableRows[0] = tr(attrs(".thead-dark"),
                th("Path No"),
                th("Total Costs"),
                th("Path")
        );
        for (Path path : graphDisplay.getKspPaths()) {

            boolean trActive;
            String trHref;
            if (config.getThisQueryString() != null) {
                trActive = (i == config.getKspKth() || ((i == graphDisplay.getKspPaths().size() - 1) && (config.getKspKth() >= graphDisplay.getKspPaths().size()) && (config.getKspKth() < Integer.MAX_VALUE)));
                trHref = "map?" + config.getThisQueryString().replace(toReplace, "kspKth=" + i);
            } else {
                trActive = (i == config.getKspKth());
                trHref = "kspKth=" + i;
            }

            //str.append("<tr class='clickable-row' data-href='heise.de'>\n");


            int numEdges = path.getEdges().size();

            // path element list
            // from Path.java#toString()
            StringBuilder pathString = new StringBuilder();
            if (numEdges > 0) {
                for (int j = 0; j < path.getEdges().size(); j++) {
                    pathString.append(path.getEdges().get(j).getFromNode().toString());
                    pathString.append(" - ");
                }

                pathString.append(path.getEdges().getLast().getToNode().toString());
            }
            //
            // j2html version
            tableRows[i + 1] = tr(attrs(trActive ? ".table-active" : ".clickable-row"),
                    td(Integer.toString(i)),
                    td(Double.toString(path.getTotalCost())),
                    td(pathString.toString())

            ).attr("data-href", trHref);
            //
            i++;
        }


        // all-row
        String allRowHref = (config.getThisQueryString() != null
                ? "map?"
                + config.getThisQueryString().replace(toReplace, "kspKth=all")
                : "#kspKth=all");
        tableRows[tableRows.length - 1] = tr(attrs(".clickable-row" + (config.getKspKth() == Integer.MAX_VALUE ? ".table-active " : "")),
                td("all paths"),
                td(),
                td()
        ).attr("data-href", allRowHref);

        ContainerTag table =
                // outer and head
                div(attrs("#kspTable"),
                        h2(text("K shortest paths from "), i(config.getKspSource()), text(" to "), i(config.getKspTarget())),
                        table(attrs(".table.table-hover" + (config.getTheme() == CallConfig.Theme.DARK ? " table-dark" : "")),
                                tableRows
                        )
                );

        return table;
    }

    /**
     * generate a String with further information about this view
     * (for example: in compOfCon-View: about the focused object)
     *
     * @return a html string
     */
    ContainerTag getFurtherInformations() {

        ContainerTag litList[] = new ContainerTag[literatureOfFocusedGeoObj.size()];


        for (int i = 0; i < literatureOfFocusedGeoObj.size(); i++) {
            Literature literature = literatureOfFocusedGeoObj.get(i);
            litList[i] = div(p(
                            literature.toHTML()
                    )/*,
                    div(attrs(".text-left"),
                            pre(code(literature.getBibTex()).render())
                    )*/
            );
        }

        // infos about the liter
        ContainerTag lit = div(attrs("#litInfos"),
                h4("Literature sources"),
                // no source info
                literatureOfFocusedGeoObj.isEmpty() ? i("There is no source to show") : rawHtml(""),

                div(attrs("#litList"),
                        litList
                ),
                br()
        );


        ContainerTag others = table(attrs("#otherInfos.table" + (config.getTheme() == CallConfig.Theme.DARK ? ".table-dark" : "")),
                tbody(
                        each(furtherInformationOfFocusedObj, information -> tr(
                                        rawHtml(td(b(information.getKey())).withStyle("text-align: right; width: 40%").render()),
                                        rawHtml(td(information.getValue()).render())
                                )
                        )
                )
        );


        return div(attrs("#furtherInfos"),
                __htmlComment("Further information about a specific obj"),
                h2(text("Further information about "), i(focusedGeoObj.getName())),
                others,
                lit
        );
    }

    /**
     * show the setted endpoint information ({@link #endpointInformation}) as table
     * if they were requested ({@link #endpointInformationRequested}).
     * If not requested show a button to enable. If information empty just show a message.
     *
     * @return the endpoint info as html content
     */
    ContainerTag getEndpointInformation() {
        String url = getLinkWithParameterValue("withVocprez", "enable");
        ContainerTag activate, table, output;

        if (endpointInformationRequested || !config.isCallerWebserver()) {
            table = (endpointInformation != null && !endpointInformation.isEmpty()) ? table(attrs("#otherInfos.table.table-dark"),
                    tbody(
                            each(endpointInformation, information -> tr(
                                            rawHtml(td(information.getKey()).withStyle("text-align: right; width: 40%").render()),
                                            rawHtml(td(information.getValue()).render())
                                    )
                            )
                    )
            ) : i("No Matching with Endpoint found");

            output = div(attrs("#furtherInfos"),
                    __htmlComment("Show Alternative Names by VocPrez-Endpoint"),
                    h2("Alternative Names from VocPrez-Endpoint")
                            .withTitle("\"VocPrez is a read-only web delivery system - web pages and API - for Simple Knowledge Organization System (SKOS)-formulated RDF vocabularies.\" http://cgi.vocabs.ga.gov.au/about"),
                    table
            );
        } else {
            activate = a(attrs(".btn" + BTN_OUTLINE_COLOR),
                    "Show Alternative Names by VocPrez-Endpoint"
            ).withType("button").withHref(url)
                    .withTitle("\"VocPrez is a read-only web delivery system - web pages and API - for Simple Knowledge Organization System (SKOS)-formulated RDF vocabularies.\" http://cgi.vocabs.ga.gov.au/about \n\nOnly 5 times in total possible!");

            output = div(attrs("#furtherInfos"),
                    __htmlComment("Show Alternative Names by VocPrez-Endpoint"),
                    activate
            );
        }

        return output;

    }


    /**
     * creates a button with the opposite value of the button-corresponding-parameter of the current view
     * more description at {@link #getOppositeButtonOfPar(String, String, String)}
     *
     * @param parName parameter name of the button
     * @param label   label of the button
     * @return a html string
     */
    public String getOppositeButtonOfPar(String parName, String label) {
        return getOppositeButtonOfPar(parName, parName, label);
    }

    /**
     * creates a button with the opposite value of the button-corresponding-parameter of the current view
     * with specific switching parameter
     * <p>
     * owl : normal → test;
     * owl : test → normal;
     * compOfCon : [null] → [disabled, just info of usage];
     * compOfCon : [something] → disable;
     * ksp : [null] → [disabled, just info of usage];
     * ksp : [something] → disable;
     * clustering : enable|noedges → disable;
     * clustering : disable → enable;
     * clusteredges : clustering.enable → clustering.noedges;
     * clusteredges : clustering.disable| clustering.noedges → clustering.enable
     *
     * @param parName            parameter name of the button
     * @param optSwitchParameter optional switch parameter
     * @param label              label of the button
     * @return a html string
     */
    public String getOppositeButtonOfPar(String parName, String optSwitchParameter, String label) {
        String url = null;

        boolean isButtonDisabled = true;

        if (config.getThisQueryString() != null) {
            switch (optSwitchParameter) {
                case "owl":
                    if (getParameterValue("owl").equals("normal")) {
                        url = getLinkWithParameterValue("owl", "test");
                    } else if (getParameterValue("owl").equals("test")) {
                        url = getLinkWithParameterValue("owl", "normal");
                    }
                    break;
                case "compOfCon":
                    url = getLinkWithRemovedParameters(new String[]{"compOfCon"});
                    break;
                case "ksp":
                    url = getLinkWithRemovedParameters(new String[]{"kspSource", "kspTarget"});
                    break;
                case "clustering":
                    if (getParameterValue("clustering").equals("enable") || getParameterValue("clustering").equals("noedges")) {
                        url = getLinkWithParameterValue("clustering", "disable");
                    } else if (getParameterValue("clustering").equals("disable")) {
                        url = getLinkWithParameterValue("clustering", "enable");
                    }
                    break;
                case "noclusteredges":
                    if (getParameterValue("clustering").equals("enable")) {
                        url = getLinkWithParameterValue("clustering", "noedges");
                    } else if (getParameterValue("clustering").equals("noedges") || getParameterValue("clustering").equals("disable")) {
                        url = getLinkWithParameterValue("clustering", "enable");
                    }
                    break;
            }

            // if ksp|compOfCon enabled -> disable its button
            if (!((parName.contains("ksp") && getParameterValue("kspSource").equals(""))
                    || (parName.contains("compOfCon") && getParameterValue("compOfCon").equals("")))) {
                isButtonDisabled = false;
            }
        }

        // make attributes
        StringBuilder attrs = new StringBuilder(".btn");

        if (label.contains("enable")) {
            attrs.append(".btn-outline-success");
        } else if (label.contains("disable")) {
            attrs.append(".btn-outline-danger");
        } else {
            attrs.append(BTN_OUTLINE_COLOR);
        }

        if (isButtonDisabled) {
            attrs.append(".disabled");
        }

        return a(attrs(attrs.toString()),
                label
        ).withType("button").withHref(url).render();

    }

    /**
     * dropdown for a parameter
     *
     * @param parName        the name of the parameter
     * @param currentValue   the current value
     * @param possibleValues a list of possible values
     * @return a html string
     */
    ContainerTag getDropdownString(String parName, String currentValue, String[] possibleValues, String
            defaultValue, String label) {


        String dropdownButtonLabel = currentValue;
        if (currentValue.equals("")) {
            dropdownButtonLabel = "default (" + defaultValue + ")";
        }

        DomContent[] valueLinks = new DomContent[possibleValues.length];

        // make dropdown-item for each possible value
        for (int i = 0; i < possibleValues.length; i++) {

            String value = possibleValues[i];
            valueLinks[i] = rawHtml( // to make links in just one line
                    a(attrs(".dropdown-item"),
                            value + (value.equals(defaultValue) ? " (default)" : "")
                    ).withHref(getLinkWithParameterValue(parName, value)).render() // render to make links in just one line
            );
        }


        String disabledIfNotWebserver = !config.isCallerWebserver() ? ".disabled" : "";

        // begin describing text in front and the button for dropdown
        ContainerTag dropdown = p(
                div(attrs("#dropdownMenuButton.dropdown"),
                        text(label + ": "),
                        button(attrs(".btn.dropdown-toggle" + BTN_COLOR + disabledIfNotWebserver),
                                // button label
                                dropdownButtonLabel
                        ).withType("button")
                                .attr("data-toggle", "dropdown")
                                .attr("aria-haspopup", "dropdown")
                                .attr("aria-expanded", "false"),
                        div(attrs(".dropdown-menu"),
                                valueLinks
                        ).attr("aria-labelledby", "dropdownMenuButton")
                )
        );

        return dropdown;
    }

    /**
     * removes the parameters with parName names to a new newValue value.
     * If paramter not contained add it at the end with the good known {@link #config#thisQueryString}
     *
     * @param parNames
     * @return
     */
    public String getLinkWithRemovedParameters(String[] parNames) {
        String newQueryString = config.getThisQueryString();
        for (String parName : parNames) {
            newQueryString = getLinkWithParameterValue(parName, "", newQueryString).replace("map?", "");
        }
        return "map?" + newQueryString;
    }

    /**
     * removes the parameters with parName names to a new newValue value with a custom queryString
     *
     * @param parNames
     * @return
     */
    public String getLinkWithRemovedParameters(String[] parNames, String queryString) {
        String newQueryString = queryString;
        for (String parName : parNames) {
            newQueryString = getLinkWithParameterValue(parName, "", newQueryString).replace("map?", "");
        }
        return "map?" + newQueryString;
    }

    /**
     * changes a parameter with parName name to a new newValue value.
     * If paramter not contained add it at the end with the good known {@link #config}.thisQueryString
     *
     * @param parName
     * @param newValue
     * @return
     */
    public String getLinkWithParameterValue(String parName, String newValue) {
        return getLinkWithParameterValue(parName, newValue, config.getThisQueryString());
    }

    /**
     * changes a parameter with parName name to a new newValue value.
     * If paramter not contained add it at the end
     *
     * @param parName           the paramter's name
     * @param newValue          the new value for this parameter
     * @param customQueryString replaces function of {@link #config}.thisQueryString
     * @return the full link beginning with "map?"
     */
    public String getLinkWithParameterValue(String parName, String newValue, String customQueryString) {
        StringBuilder link = new StringBuilder("map?");
        String[] thisQueryStrings = {};

        boolean found = false;

        // change parameter
        if (customQueryString != null) {
            thisQueryStrings = customQueryString.split("&");
            for (int i = 0; i < thisQueryStrings.length; i++) {
                String str = thisQueryStrings[i];
                if (str != null) {
                    //   ...&parname=value&...
                    // [..., parname=value, ...]
                    //       parname=
                    if (str.startsWith(parName + "=")) {
                        found = true;
                        if (!newValue.equals("")) {
                            thisQueryStrings[i] = parName + "=" + newValue;
                        } else {
                            thisQueryStrings[i] = "";
                        }
                    }
                }
            }
        }

        // append all parameters
        for (String queryString : thisQueryStrings) {
            if (queryString.length() > 2) {
                link.append(queryString + "&");
            }
        }

        // append new parameter
        if (!found && !newValue.equals("")) {
            link.append(parName + "=" + newValue);
        }

        return cleanUpUrl(link.toString());
    }

    /**
     * get the value of a specific parameter
     *
     * @param parName the parameter's name
     * @return the value, empty string if not found
     */
    public String getParameterValue(String parName) {
        String[] thisQueryStrings;

        // look for the parametername
        if (config.getThisQueryString() != null) {
            thisQueryStrings = config.getThisQueryString().split("&");
            for (int i = 0; i < thisQueryStrings.length; i++) {
                String str = thisQueryStrings[i];
                if (str != null) {
                    //   ...&parname=value&...
                    // [..., parname=value, ...]
                    //       parname=
                    if (str.startsWith(parName + "=")) {

                        // return the value
                        return str.replace(parName + "=", "");
                    }
                }
            }
        }

        // not found: null
        return "";
    }

    /**
     * remove multiple & in a row and other unnecessary stuff
     *
     * @param url
     * @return
     */
    String cleanUpUrl(String url) {
        while (url.contains("&&")) {
            url = url.replace("&&", "");
        }
        url = url.replace("?&", "?");
        url = url.replace("null&", "");
        if (url.charAt(url.length() - 1) == '&') {
            return url.substring(0, url.length() - 1);
        }

        /*
        if (allMethodCatsSelected()) {
            url = getLinkWithRemovedParameters(new String[]{"onlyMethodCat"}, url);
        }*/

        return url;
    }

    private boolean allMethodCatsSelected() {
        boolean allMethodCatsSelected = true;
        for (String catGroupStr : allMethodCats.keySet()) {
            for (String catStr : allMethodCats.get(catGroupStr)) {
                if (!config.getSelectedMethodCats().contains(catStr)) {
                    allMethodCatsSelected = false;
                }
            }
        }
        return allMethodCatsSelected;
    }

    /**
     * returns a bootstrap-badge with the number as input
     *
     * @param number
     * @return
     */
    public DomContent badge(int number) {
        String badgeType = (number > 0) ? ".badge-primary" : ".badge-secondary";
        return span(attrs(".badge" + badgeType), Integer.toString(number));
    }


    /**
     * the focused Object for further Information
     *
     * @param focusedGeoObj
     */
    public void setFocusedGeoObj(GeothermalObject focusedGeoObj) {
        this.focusedGeoObj = focusedGeoObj;
    }

    /**
     * adds a Literature of focused Obj
     *
     * @param literatureList
     */
    public void setLiteratureOfFousedGeoObj(List<Literature> literatureList) {
        literatureOfFocusedGeoObj = literatureList;
    }

    /**
     * adds further information of the focused Object
     *
     * @param furtherInformation list of SimpleEntry-Pairs: (Predicate-Str, Object-Str)
     */
    public void setFurtherInformationOfFocusedObj(LinkedList<AbstractMap.SimpleEntry<String, String>> furtherInformation) {
        furtherInformationOfFocusedObj = furtherInformation;
    }

    public void setEndpointInformation(boolean endpointInformationRequested, LinkedList<AbstractMap.SimpleEntry<String, String>> endpointInformation) {
        this.endpointInformationRequested = endpointInformationRequested;
        this.endpointInformation = endpointInformation;
    }

    public void setMethodCategories(HashMap<String, LinkedList<String>> allMethodCats, HashMap<String, Integer> allMethodCatsOccurrences) {
        this.allMethodCats = allMethodCats;
        this.allMethodCatsOccurrences = allMethodCatsOccurrences;
    }


    /**
     * get a comment in html style
     *
     * @param comment comment string
     * @return the comment as UnexcapedText
     */
    UnescapedText __htmlComment(String comment) {
        return rawHtml("<!-- " + comment + " -->");
    }

    /**
     * get a empty line as UnescapedText
     *
     * @return an empty line
     */
    UnescapedText __htmlEmptyLine() {
        return rawHtml("");
    }
}