package geothermal_db_graph.app.config;

/**
 * Contains constants with configurations about the behavior of the program,
 * as well as various prefixes, IRIs and filenames about the ontologies and
 * enums used for assignments of objects.
 */
public class Preferences {

    // owl iri configs
    // // Prefixes
    public static final String
            PREFIX = "http://www.semwebtech.org/geothermie-ontology#";
    public static final String[]
            ALL_PREFIXES = {
            "http://www.semwebtech.org/geothermie-ontology#",
            "http://www.ontology-of-units-of-measure.org/resource/om-2/"
    };
    public static final String[][]
            PREFIX_REPLACEMENTS = {
            {"http://www.ontology-of-units-of-measure.org/resource/om-2/", PREFIX}
    };
    // // Suffixes
    public static final String
            NAME_IRI = "name",
            TO_METHOD_IRI = "hasMandInput",  // <Parameter> --> (Method)
            FROM_METHOD_IRI = "generates", // (Method) --> <Parameter>
            HAS_MEDIUM_IRI = "hasMedium", // (Method) --> <Parameter>
            IS_MEDIUM_OF_IRI = "isMediumOf", // (Method) --> <Parameter>
            METHOD_CAT_IRI = "hasMethodCat";
    public static final String[]
            FROM_METHOD_IRI_ABCO = {
            "generatesParaLevA",
            "generatesParaLevB",
            "generatesParaLevC",
            "MethodGeneratesObject"
    },
            FROM_METHOD_IRI_ABCO_INVERSE = {
                    "isParaLevAgenFromMethod",
                    "isParaLevBgenFromMethod",
                    "isParaLevCgenFromMethod",
                    "ObjectIsCreatedFromMethod"
            },
            TO_METHOD_IRI_MO = {
                    "hasMandInput",
                    "hasOptInput"
            },
            TO_METHOD_IRI_MO_INVERSE = {
                    "isMandInputOf",
                    "isOptInputOf"
            };
    // // // Literature
    public static final String
            LIT_CLASS_IRI = "LiteratureObject",
            LIT_KEY_IRI = "key",
            BIBTEX_DATA_PROPERTY_GROUP_IRI = "bibTexProperties",
            BIBTEX_OBJ_PROPERTY_GROUP_IRI = "BibTexProperties",
            BIBTEXTYPES_CLASS_IRI = "bibEntryTypes",
            FROM_LITERATURE_IRI = "fromLiterature",
            LIT_FAUTHOR_IRI = "Fauthor",
            LIT_TITLE_IRI = "title",
            LIT_AUTHORS_IRI = "author",
            LIT_YEAR_IRI = "year",
            LIT_BIBTEXTYPE_IRI = "hasBibType";
    // // // Pathfinder
    public static final String
            PATHFINDER_IRI = "pathfinderProperties",
            PATHS_IRI = "inPath",
            PATHS_DEPTH_IRI = "pathsDepth",
            PATHS_DOMAIN_IRI = "GeothermalThing",
            IMPORTANCE_IRI = "hasImportance";
    public static final boolean
            SHOW_ONLY_ANCESTORS = false,    // especially for Parameters
            REMOVE_SIBLINGS = true;    // other Methods etc. resulting to the
    public static final String
            KSP_SOURCENODE = "tapeMeasurement",
            KSP_TARGETNODE = "faultSpacing",
            KSP_SOURCENODE_1 = "thermalConductivity",
            KSP_TARGETNODE_1 = "thermalGradientHole";
    public static final String
            KSP_SOURCENODE_TEST = "dSteady-stateThermalModeling",
            KSP_TARGETNODE_TEST = "fractureOrientation";
    public static final int
            KSP_k = 10,
            KSP_kth = 5;
    // out configs
    public static final String ontology = "ontology.owl", merged = "geothermal_Ontology_copyForWebapp.n3";
    public static final String
            TEST_OWL_SOURCE = "out/testOntology3.owl",
            OWL_SOURCE = merged,
    // // Pathfinder
    PATHFINDERED_SOURCE = "out/pathfindered_ontology.owl",
            PATHFINDERED_TEST_SOURCE = "out/pathfindered_testOntology.owl";
    // same Parameter, only for descendants. Not for ancestors.
    // see issue #47
    // // // // Pathfinder: some sample 'm'ethods and 'p'arameters
    private static final String
            m1 = "tapeMeasurement",
            p1 = "faultAperture",
            p3 = "horizonOrientation",
            m3 = "compassMeasurement",
            m4 = "semi-automaticLineDetection",
            m5 = "thermalGradientHole",
            p5 = "thermalConductivity",
            m6 = "manualPoint-picking",
            p2 = "modeledTemperature",  // only --testowl
            m7 = "gasContentTitration", // only --testowl
            m8 = "injectionTests",
            m9 = "downholeTemperatureProbe";
    // // // // CompOfCon: start DFS at
    public static final String
            COMPOFCON_START = m1;
    public static final String
            COMPOFCON_START_JUNITTEST = m1;
    private static final int
            ksp_all = Integer.MAX_VALUE;
    // activate(true)/deactivate(false) features
    public static boolean
            OPEN_GENERATED_PICTURE_IN_WINDOW = false,
            OPEN_GENERATED_PICTURE_MAP_IN_BROWSER = false;
    public static On1stNodeClick
            BEHAVIOUR_ON_1ST_NODE_CLICK = On1stNodeClick.selelct;
    // // // K-SHORTEST PATHthermalConductivity
    public static boolean
            ENABLE_KSP = false;

    // Pathfinder
    public enum Importance {
        NORMAL,
        MEDIUM,
        HIGH
    }

    // What happens at the first click
    public enum On1stNodeClick {
        selelct,
        compOfCon
    }

}
