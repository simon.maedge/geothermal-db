package geothermal_db_graph.app.config;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.*;

/**
 * Configuration of the current program call
 */
public class CallConfig {

    /**
     * log4j-logger of this class. Preferences defined in ./log4j2.xml
     */
    private final Logger logger = LogManager.getLogger(CallConfig.class);

    //private boolean isJUnitTest = false;

    //private String owlPrefix = Properties.PREFIX;

    // CALLING SRC
    private Caller caller = Caller.DEFAULT;
    private String thisQueryString;


    // FEATURES
    private Owl owl = Owl.GEODB;
    private Clustering clustering = Clustering.ENABLE;
    private CompOfConStatus compOfConStatus = CompOfConStatus.DISABLE;
    private String compOfCon;
    private String selectedNode;
    private Vocprez vocprez = Vocprez.DISABLE;

    // ksp
    private String kspSource, kspTarget;
    private KspItemType kspSourceType = KspItemType.NODE, kspTargetType = KspItemType.NODE;
    private int kspK = -1, kspKth = -1;


    // STYLE
    private Theme theme = Theme.DARK;
    private String shapeMethod, shapeParameter;
    private GraphDirection graphDirection = GraphDirection.TOP_BOTTOM;
    private String edgewidth = "1";
    private String fontsize = "14";
    private List<String> selectedMethodCats = new LinkedList<>();
    private MultipleMethCatOptions multipleMethCatOption = MultipleMethCatOptions.UNITE;

    /**
     * default constructor
     */
    public CallConfig() {
    }

    /**
     * config by parameter map
     *
     * @param parameterMap maps a parameter name to it's values
     */
    public CallConfig(Map<String, String[]> parameterMap) {
        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            String value0 = entry.getValue()[0];
            switch (entry.getKey()) {
                case "owl":
                    if (value0.equals("test")) {
                        owl = Owl.TEST;
                    } else if (entry.getValue()[0].equals("owl")) {
                        owl = Owl.GEODB;
                    }
                    break;
                case "compOfCon":
                    compOfCon = value0;
                    compOfConStatus = CompOfConStatus.ENABLE;

                    break;
                case "clustering":
                    if (value0.equals("disable")) {
                        clustering = Clustering.DISABLE;
                    } else if (value0.equals("noedges")) {
                        clustering = Clustering.WITHOUT_EDGES;
                    } else if (value0.equals("enable")) {
                        clustering = Clustering.ENABLE;
                    }
                    break;
                case "kspSource":
                    kspSource = value0;
                    break;
                case "kspTarget":
                    kspTarget = value0;
                    break;
                case "kspK":
                    kspK = Integer.parseInt(value0);
                    break;
                case "kspKth":
                    kspKth = Integer.parseInt(value0);
                    break;
                case "selectNode":
                    selectedNode = value0;
                    break;
                case "direction":
                    if (value0.equals("lr")) {
                        graphDirection = GraphDirection.LEFT_RIGHT;
                    } else if (value0.equals("tb")) {
                        graphDirection = GraphDirection.TOP_BOTTOM;
                    }
                    break;
                case "shapeMethod":
                    shapeMethod = value0;
                    break;
                case "shapeParameter":
                    shapeParameter = value0;
                    break;
                case "edgeWidth":
                    edgewidth = value0;
                    break;
                case "fontSize":
                    fontsize = value0;
                    break;
                case "onlyMethodCat":
                    selectedMethodCats.addAll(Arrays.asList(entry.getValue()));
                    break;
                case "methodCatSelBy":
                    if (value0.equals("intersect")) {
                        multipleMethCatOption = MultipleMethCatOptions.INTERSECT;
                    } else {
                        multipleMethCatOption = MultipleMethCatOptions.UNITE;
                    }
                    break;
                case "withVocprez":
                    vocprez = Vocprez.ENABLE;
                    break;
                case "theme":
                    if (value0.equals("dark")) {
                        theme = Theme.DARK;
                    } else if (value0.equals("light") || value0.equals("default")) {
                        theme = Theme.LIGHT;
                    }
                    break;
                default:
                    logger.warn("no semantic defined for this parameter");
            }
        }

    }

    /**
     * config with program arguments
     *
     * @param args ordinary program command line arguments
     */
    public CallConfig(String[] args) {
        for (String arg : args) {
            if (arg.equals("--help")) {
                System.out.println("Owl2Graph - Geothermal Ontology Graph\n" +
                        "\n" +
                        "displays the ontology.owl as a html graph in out/maps/... and as a image in out/owlgraphs/...\n" +
                        "\n" +
                        "following parameters are possible:\n" +
                        "\n" +
                        "about functionality:\n" +
                        "--testowl replaces extended_ontology by testOntology;\n" +
                        "--noclustering deactivates clustering of Methods by Superclasses;\n" +
                        "--noclusteredges deactivates turning all same edges of cluster-Objects into one edge for the whole cluster (not working now); \n" +
                        "-compOfCon=name requests the map with special obj in focus. The Graph shows the (restricted) components of connectivity to this object;\n" +
                        "--withVocprez can only be disabled by webserver. if enabled vocprez-endpoint will be queried for alternative obj names\n" +
                        "\n" +
                        "about design:\n" +
                        "-kspSource=name and -kspTarget=name calculates the K shortest paths by Yen-Algorithm;\n" +
                        "-k and -kth specifies the K res. the number of the path that should be shown;\n" +
                        "-selectNode=name highlights a node and allows to run compOfCon (same node) or ksp (different node) with next klick;\n" +
                        "-direction=lr changes flow of graph to Left to Right (instead of default Top to Bottom);\n" +
                        "-shapeMethod=shape, -shapeParameter=shape specify the shape of the nodes (like rectangle, oval, ...);\n" +
                        "-edgeWidth=int, -fontSize=int specify the width of an edge res. the fontsize in numbers, defaults aare 1 res. 14;");
                System.exit(0);
            }
            if (arg.equals("--testowl")) {
                owl = Owl.TEST;
            } else if (arg.equals("--noclustering")) {
                clustering = Clustering.DISABLE;
            } else if (arg.equals("--noclusteredges")) {
                clustering = Clustering.WITHOUT_EDGES;
            } else if (arg.contains("-compOfCon=")) {
                compOfConStatus = CompOfConStatus.ENABLE;
                compOfCon = arg.replace("-compOfCon=", "");
            } else if (arg.contains("-onlyMethodCat=")) {
                selectedMethodCats.add(arg.replace("-onlyMethodCat=", ""));
            } else if (arg.contains("-methodCatSelBy=intersect")) {
                multipleMethCatOption = MultipleMethCatOptions.INTERSECT;
            } else if (arg.contains("-kspSource=")) {
                kspSource = arg.replace("-kspSource=", "");
            } else if (arg.contains("-kspTarget=")) {
                kspTarget = arg.replace("-kspTarget=", "");
            } else if (arg.contains("-kspK=")) {
                kspK = Integer.parseInt(arg.replace("-kspK=", ""));
            } else if (arg.contains("-kspKth=")) {
                if (arg.contains("all")) {
                    kspKth = Integer.MAX_VALUE;
                } else {
                    kspKth = Integer.parseInt(arg.replace("-kspKth=", ""));
                }
            } else if (arg.contains("-selectNode=")) {
                selectedNode = arg.replace("-selectNode=", "");
            } else if (arg.contains("-theme")) {
                if (arg.contains("light")) {
                    theme = Theme.LIGHT;
                } else if (arg.contains("dark") || arg.contains("default")) {
                    theme = Theme.DARK;
                }
            } else if (arg.contains("-direction=lr")) {
                graphDirection = GraphDirection.LEFT_RIGHT;
            } else if (arg.contains("-shapeMethod=")) {
                shapeMethod = arg.replace("-shapeMethod=", "");
            } else if (arg.contains("-shapeParameter=")) {
                shapeParameter = arg.replace("-shapeParameter=", "");
            } else if (arg.contains("-edgeWidth=")) {
                edgewidth = arg.replace("-edgeWidth=", "");
            } else if (arg.contains("-fontSize=")) {
                fontsize = arg.replace("-fontSize=", "");
            } else if (arg.contains("--withVocprez")) {
                vocprez = Vocprez.ENABLE;
            } else if (arg.equals("")) {
                break;
            }
        }
    }

    public boolean isCallerWebserver() {
        return caller == Caller.WEBSERVER;
    }

    // select

    public boolean isOwlTestowl() {
        return owl == Owl.TEST;
    }
    // STYLE

    //
    // shape

    public boolean isCompOfConEnabled() {
        return compOfConStatus == CompOfConStatus.ENABLE;
    }

    public boolean isVocprezEnabled() {
        return vocprez == Vocprez.ENABLE;
    }

    public boolean isKspEnabled() {
        return Preferences.ENABLE_KSP || (kspSource != null && kspTarget != null);
    }

    /**
     * concats configuration/parameter abbreviations
     * (to get included in pathnames)
     */
    public String getPathparameter() {

        // ksp
        int k, kth;
        k = kspK > 0 ? kspK : Preferences.KSP_k;
        kth = kspKth >= 0 ? kspKth : Preferences.KSP_kth;

        String pathparameter = (isCompOfConEnabled() ? "_pf" : "")
                + (isOwlTestowl() ? "_testowl" : "")
                + (clustering == Clustering.DISABLE ? "_noclustering" :
                (clustering == Clustering.WITHOUT_EDGES ? "_noclusteredges" : ""))
                + (isKspEnabled()
                ? "_ksp_k" + k + ((kth != Integer.MAX_VALUE) ? "_" + kth : "")
                : "");
        logger.trace("KSP!=NULL? : " + (kspSource != null) + " " + (kspTarget != null));

        return pathparameter;
    }

    public Caller getCaller() {
        return caller;
    }

    public void setCaller(Caller caller) {
        this.caller = caller;
    }

    public String getThisQueryString() {
        return thisQueryString;
    }

    public void setThisQueryString(String thisQueryString) {
        this.thisQueryString = thisQueryString;
    }

    public Owl getOwl() {
        return owl;
    }

    public void setOwl(Owl owl) {
        this.owl = owl;
    }

    public Clustering getClustering() {
        return clustering;
    }

    public void setClustering(Clustering clustering) {
        this.clustering = clustering;
    }


    // Setter and Getter

    public CompOfConStatus getCompOfConStatus() {
        return compOfConStatus;
    }

    public void setCompOfConStatus(CompOfConStatus compOfConStatus) {
        this.compOfConStatus = compOfConStatus;
    }

    public String getCompOfCon() {
        return compOfCon;
    }

    public void setCompOfCon(String compOfCon) {
        this.compOfCon = compOfCon;
    }

    public Vocprez getVocprez() {
        return vocprez;
    }

    public void setVocprez(Vocprez vocprez) {
        this.vocprez = vocprez;
    }

    public String getKspSource() {
        return kspSource;
    }

    public void setKspSource(String kspSource) {
        this.kspSource = kspSource;
    }

    public String getKspTarget() {
        return kspTarget;
    }

    public void setKspTarget(String kspTarget) {
        this.kspTarget = kspTarget;
    }

    public KspItemType getKspSourceType() {
        return kspSourceType;
    }

    public void setKspSourceType(KspItemType kspSourceType) {
        this.kspSourceType = kspSourceType;
    }

    public KspItemType getKspTargetType() {
        return kspTargetType;
    }

    public void setKspTargetType(KspItemType kspTargetType) {
        this.kspTargetType = kspTargetType;
    }

    public int getKspK() {
        return kspK;
    }

    public void setKspK(int kspK) {
        this.kspK = kspK;
    }

    public int getKspKth() {
        return kspKth;
    }

    public void setKspKth(int kspKth) {
        this.kspKth = kspKth;
    }

    public String getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(String selectedNode) {
        this.selectedNode = selectedNode;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public String getShapeMethod() {
        return shapeMethod;
    }

    public void setShapeMethod(String shapeMethod) {
        this.shapeMethod = shapeMethod;
    }

    public String getShapeParameter() {
        return shapeParameter;
    }

    public void setShapeParameter(String shapeParameter) {
        this.shapeParameter = shapeParameter;
    }

    public GraphDirection getGraphDirection() {
        return graphDirection;
    }

    public void setGraphDirection(GraphDirection graphDirection) {
        this.graphDirection = graphDirection;
    }

    public String getEdgewidth() {
        return edgewidth;
    }

    public void setEdgewidth(String edgewidth) {
        this.edgewidth = edgewidth;
    }

    public String getFontsize() {
        return fontsize;
    }

    public void setFontsize(String fontsize) {
        this.fontsize = fontsize;
    }

    public MultipleMethCatOptions getMultipleMethCatOption() {
        return multipleMethCatOption;
    }

    public void setMultipleMethCatOption(MultipleMethCatOptions multipleMethCatOption) {
        this.multipleMethCatOption = multipleMethCatOption;
    }

    public List<String> getSelectedMethodCats() {
        return selectedMethodCats;
    }

    public void setSelectedMethodCats(List<String> selectedMethodCats) {
        this.selectedMethodCats = selectedMethodCats;
    }

    public enum Caller {
        WEBSERVER,
        DESKTOP,
        DEFAULT
    }

    //
    // ontology
    public enum Owl {
        GEODB,
        TEST
    }

    // clustering
    public enum Clustering {
        ENABLE,
        DISABLE,
        WITHOUT_EDGES
    }

    // compOfcon
    public enum CompOfConStatus {
        ENABLE,
        DISABLE
    }

    // vocprez
    public enum Vocprez {
        ENABLE,
        DISABLE
    }

    // ksp
    public enum KspItemType {
        NODE,
        MEDIUM
    }

    // direction of graph
    public enum GraphDirection {
        TOP_BOTTOM,
        LEFT_RIGHT
    }

    // method categories
    public enum MultipleMethCatOptions {
        UNITE,
        INTERSECT
    }

    // display mode
    public enum Theme {
        DARK,
        LIGHT
    }
}