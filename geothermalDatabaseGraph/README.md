# Geothemal-DB Graph Webapp

This webapp converts the geothermal database (../geothermalOntology.n3) into a graphical representation. 

The aim is to process these data in a user-friendly way, so that existing 
correlations are easily recognizable.

In particular, a simple changeability of the configuration shall be made 
possible by a representation in a web interface. Possible operations are, 
among others, the display of all existing connections in the database, the calculation of 
shortest paths between two specific dates in the set of workflows and the restriction to all 
connections to one date as well as various modifications of the design.

The usability is described in more detail under #Documentations (see below).

Example view of the Geothermal-DB Webapp:
![](https://pad.gwdg.de/uploads/upload_94b3eb5b6382f28cc37fe872534ed67d.png)

## Setup
**note:** This setup and the usage can also be found [here](https://pad.gwdg.de/xLIgiAJLTdeQREtcho0P6EN).

For the webapp to work properly, you should install [Java](https://java.com/en/download/manual.jsp) if you don't have it already installed. Furthermore you need [Graphviz](https://www.graphviz.org/download/) for the graph to be displayed correctly.

The program is developed as a webapp for the [Apache-Tomcat web server](https://tomcat.apache.org/). 

To start the webserver and the  way is to run the `geothermalGraphWebapp.jar` file. It starts a Tomcat Webserver and the webapp is accessable by default under http://localhost:8880/geothermalGraph/. To change the port number just start the program with that number as argument.
