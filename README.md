# Geothermal Database
**Database of relevant spatial datasets for further gis analyses on regional to local scale**

## Description of the deliverable content and purpose
The aim and content of this deliverable is the compilation of parameters, spatial data and methodological workflows dedicated to support future EGS projects, the exploration of unconventional geothermal locations in the Variscan basement and the conversion of non-geothermal wells. This compilation is based on the results of MEET’s  technical work packages 3 to 6 as well as the elaborated deliverables of work package 7 and shall be available as a supporting tool for future analyses with spatial reference (Figure 1).

**Figure 1:** Schematic concept of the data and workflows of WP5 as well as WP3 to 6 and their connection to the deliverables of WP 7
![](https://pad.gwdg.de/uploads/upload_b17f1545da64b4add7b310b4f282c92d.png)

It can be used as a framework for local groups (geological services, administrations, decision makers) to support the selection and evaluation of relevant geodata for investigations on national, regional and local level. Another objective is the transfer of as much technical knowledge and experience as possible, which has been discussed, evaluated, compared and tested in work packages 3 to 6. This is done in the form of linked methodological work steps.

Furthermore, the current version of the elaborated database concept and architecture can be interpreted as a starting point for future optimization and extension. It can be utilized by the Geothermal community for the long-term and open-access documentation, connection and publication of discipline-specific expertise.

## Brief description of the state of the art and the innovation breakthroughs
The original goal of the deliverable was to create a catalogue in which relevant spatial data sets for geothermal projects are summarised. However, in the course of the project and especially through the intensive research and professional exchange with partners from the other work packages, a different set of requirements emerged.

**Figure 2:** First hand-drawn figure of the core concept of a network-like database.
![](https://pad.gwdg.de/uploads/upload_69f5cafcee08d30c7a21269d30bf1ef1.png)

It became clear that the challenges lie in the area of communication between experts from different fields, the development of adequate workflows, the comparability of methodological approaches and the compilation of suitable specialised data (characteristic values, bandwidths). In addition, the more detailed web research has shown that a variety of useful online data sources for obtaining geodata and other relevant data sets are already available. However, these data sources can be classified as very heterogeneous with regard to data formats, functionalities, professional orientation and topicality. In addition, many classic data platforms are currently undergoing a change towards the concept of the Semantic Web and Open Linked Data.

For these reasons, the approach of a simple catalogue was replaced by the idea of a more functional database. Since the core topic of "relevant data sets" was also expanded to include parameters and methods due to the above-mentioned necessities, the originally conceived and developed PostgreSQL database proved to be too inflexible in terms of connecting heterogenous information. As a result, the entire structure and the data already collected were transferred to a newly created graph database (ontology), as shown in Figure 2.

This database, which is now available, has three decisive advantages. Firstly, the data sets relevant to geothermal issues are embedded in an overall concept of parameter and method links. Secondly, the database is already linked to other existing databases via interfaces, so that existing knowledge can be used. Thirdly, in addition to the database, an experimental graphical web application was developed that makes the connection between parameters and methods as well as the linked data sets visible.

# Geothermal Database Graph
In addition, a Webapp was build to process these data in a user-friendly way, so that existing correlations are easily recognizable. 
See [/geothermalDatabaseGraph](./geothermalDatabaseGraph/).